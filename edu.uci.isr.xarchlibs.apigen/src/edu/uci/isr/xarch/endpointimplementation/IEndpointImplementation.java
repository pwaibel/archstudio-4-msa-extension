/*
 * Copyright (c) 2000-2005 Regents of the University of California.
 * All rights reserved.
 *
 * This software was developed at the University of California, Irvine.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that the above copyright notice and this paragraph are
 * duplicated in all such forms and that any documentation,
 * advertising materials, and other materials related to such
 * distribution and use acknowledge that the software was developed
 * by the University of California, Irvine.  The name of the
 * University may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */
package edu.uci.isr.xarch.endpointimplementation;

import java.util.Collection;
import edu.uci.isr.xarch.XArchActionMetadata;
import edu.uci.isr.xarch.XArchTypeMetadata;
import edu.uci.isr.xarch.XArchPropertyMetadata;

/**
 * Interface for accessing objects of the
 * EndpointImplementation <code>xsi:type</code> in the
 * endpointimplementation namespace.  Extends and
 * inherits the properties of the
 * Implementation <code>xsi:type</code>.
 * 
 * @author xArch apigen
 */
public interface IEndpointImplementation extends edu.uci.isr.xarch.implementation.IImplementation, edu.uci.isr.xarch.IXArchElement{

	public final static XArchTypeMetadata TYPE_METADATA = new XArchTypeMetadata(
		XArchTypeMetadata.XARCH_ELEMENT,
		"endpointimplementation", "EndpointImplementation", edu.uci.isr.xarch.implementation.IImplementation.TYPE_METADATA,
		new XArchPropertyMetadata[]{
			XArchPropertyMetadata.createElement("Reply_To_Queue", "instance", "XMLLink", 0, 1),
			XArchPropertyMetadata.createElement("Endpoint_Position_No", "endpointimplementation", "EndpointPositionNoType", 0, 1),
			XArchPropertyMetadata.createElement("Durable_Name", "endpointimplementation", "DurableName", 0, 1),
			XArchPropertyMetadata.createElement("Connection_To_Request_Endpoint", "instance", "XMLLink", 0, 1)},
		new XArchActionMetadata[]{});

	/**
	 * Set the Reply_To_Queue for this EndpointImplementation.
	 * @param value new Reply_To_Queue
	 */
	public void setReply_To_Queue(edu.uci.isr.xarch.instance.IXMLLink value);

	/**
	 * Clear the Reply_To_Queue from this EndpointImplementation.
	 */
	public void clearReply_To_Queue();

	/**
	 * Get the Reply_To_Queue from this EndpointImplementation.
	 * @return Reply_To_Queue
	 */
	public edu.uci.isr.xarch.instance.IXMLLink getReply_To_Queue();

	/**
	 * Determine if this EndpointImplementation has the given Reply_To_Queue
	 * @param Reply_To_QueueToCheck Reply_To_Queue to compare
	 * @return <code>true</code> if the Reply_To_Queues are equivalent,
	 * <code>false</code> otherwise
	 */
	public boolean hasReply_To_Queue(edu.uci.isr.xarch.instance.IXMLLink Reply_To_QueueToCheck);

	/**
	 * Set the Endpoint_Position_No for this EndpointImplementation.
	 * @param value new Endpoint_Position_No
	 */
	public void setEndpoint_Position_No(IEndpointPositionNoType value);

	/**
	 * Clear the Endpoint_Position_No from this EndpointImplementation.
	 */
	public void clearEndpoint_Position_No();

	/**
	 * Get the Endpoint_Position_No from this EndpointImplementation.
	 * @return Endpoint_Position_No
	 */
	public IEndpointPositionNoType getEndpoint_Position_No();

	/**
	 * Determine if this EndpointImplementation has the given Endpoint_Position_No
	 * @param Endpoint_Position_NoToCheck Endpoint_Position_No to compare
	 * @return <code>true</code> if the Endpoint_Position_Nos are equivalent,
	 * <code>false</code> otherwise
	 */
	public boolean hasEndpoint_Position_No(IEndpointPositionNoType Endpoint_Position_NoToCheck);

	/**
	 * Set the Durable_Name for this EndpointImplementation.
	 * @param value new Durable_Name
	 */
	public void setDurable_Name(IDurableName value);

	/**
	 * Clear the Durable_Name from this EndpointImplementation.
	 */
	public void clearDurable_Name();

	/**
	 * Get the Durable_Name from this EndpointImplementation.
	 * @return Durable_Name
	 */
	public IDurableName getDurable_Name();

	/**
	 * Determine if this EndpointImplementation has the given Durable_Name
	 * @param Durable_NameToCheck Durable_Name to compare
	 * @return <code>true</code> if the Durable_Names are equivalent,
	 * <code>false</code> otherwise
	 */
	public boolean hasDurable_Name(IDurableName Durable_NameToCheck);

	/**
	 * Set the Connection_To_Request_Endpoint for this EndpointImplementation.
	 * @param value new Connection_To_Request_Endpoint
	 */
	public void setConnection_To_Request_Endpoint(edu.uci.isr.xarch.instance.IXMLLink value);

	/**
	 * Clear the Connection_To_Request_Endpoint from this EndpointImplementation.
	 */
	public void clearConnection_To_Request_Endpoint();

	/**
	 * Get the Connection_To_Request_Endpoint from this EndpointImplementation.
	 * @return Connection_To_Request_Endpoint
	 */
	public edu.uci.isr.xarch.instance.IXMLLink getConnection_To_Request_Endpoint();

	/**
	 * Determine if this EndpointImplementation has the given Connection_To_Request_Endpoint
	 * @param Connection_To_Request_EndpointToCheck Connection_To_Request_Endpoint to compare
	 * @return <code>true</code> if the Connection_To_Request_Endpoints are equivalent,
	 * <code>false</code> otherwise
	 */
	public boolean hasConnection_To_Request_Endpoint(edu.uci.isr.xarch.instance.IXMLLink Connection_To_Request_EndpointToCheck);
	/**
	 * Determine if another EndpointImplementation is equivalent to this one, ignoring
	 * ID's.
	 * @param EndpointImplementationToCheck EndpointImplementation to compare to this one.
	 * @return <code>true</code> if all the child elements of this
	 * EndpointImplementation are equivalent, <code>false</code> otherwise.
	 */
	public boolean isEquivalent(IEndpointImplementation EndpointImplementationToCheck);

}
