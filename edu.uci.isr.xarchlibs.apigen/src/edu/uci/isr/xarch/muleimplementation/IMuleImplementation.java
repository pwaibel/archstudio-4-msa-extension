/*
 * Copyright (c) 2000-2005 Regents of the University of California.
 * All rights reserved.
 *
 * This software was developed at the University of California, Irvine.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that the above copyright notice and this paragraph are
 * duplicated in all such forms and that any documentation,
 * advertising materials, and other materials related to such
 * distribution and use acknowledge that the software was developed
 * by the University of California, Irvine.  The name of the
 * University may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */
package edu.uci.isr.xarch.muleimplementation;

import java.util.Collection;
import edu.uci.isr.xarch.XArchActionMetadata;
import edu.uci.isr.xarch.XArchTypeMetadata;
import edu.uci.isr.xarch.XArchPropertyMetadata;

/**
 * Interface for accessing objects of the
 * MuleImplementation <code>xsi:type</code> in the
 * muleimplementation namespace.  Extends and
 * inherits the properties of the
 * Implementation <code>xsi:type</code>.
 * 
 * @author xArch apigen
 */
public interface IMuleImplementation extends edu.uci.isr.xarch.implementation.IImplementation, edu.uci.isr.xarch.IXArchElement{

	public final static XArchTypeMetadata TYPE_METADATA = new XArchTypeMetadata(
		XArchTypeMetadata.XARCH_ELEMENT,
		"muleimplementation", "MuleImplementation", edu.uci.isr.xarch.implementation.IImplementation.TYPE_METADATA,
		new XArchPropertyMetadata[]{
			XArchPropertyMetadata.createAttribute("file_id", "http://www.w3.org/2001/XMLSchema", "string", null, null),
			XArchPropertyMetadata.createElement("Additional_Configuration", "muleimplementation", "AdditionalConfig", 0, XArchPropertyMetadata.UNBOUNDED)},
		new XArchActionMetadata[]{});

	/**
	 * Set the file_id attribute on this MuleImplementation.
	 * @param file_id file_id
	 * @exception FixedValueException if the attribute has a fixed value
	 * and the value passed is not the fixed value.
	*/
	public void setFile_id(String file_id);

	/**
	 * Remove the file_id attribute from this MuleImplementation.
	 */
	public void clearFile_id();

	/**
	 * Get the file_id attribute from this MuleImplementation.
	 * if the attribute has a fixed value, this function will
	 * return that fixed value, even if it is not actually present
	 * in the XML document.
	 * @return file_id on this MuleImplementation
	 */
	public String getFile_id();

	/**
	 * Determine if the file_id attribute on this MuleImplementation
	 * has the given value.
	 * @param file_id Attribute value to compare
	 * @return <code>true</code> if they match; <code>false</code>
	 * otherwise.
	 */
	public boolean hasFile_id(String file_id);


	/**
	 * Add a Additional_Configuration to this MuleImplementation.
	 * @param newAdditional_Configuration Additional_Configuration to add.
	 */
	public void addAdditional_Configuration(IAdditionalConfig newAdditional_Configuration);

	/**
	 * Add a collection of Additional_Configurations to this MuleImplementation.
	 * @param Additional_Configurations Additional_Configurations to add.
	 */
	public void addAdditional_Configurations(Collection Additional_Configurations);

	/**
	 * Remove all Additional_Configurations from this MuleImplementation.
	 */
	public void clearAdditional_Configurations();

	/**
	 * Remove the given Additional_Configuration from this MuleImplementation.
	 * Matching is done by the <code>isEquivalent(...)</code> function.
	 * @param Additional_ConfigurationToRemove Additional_Configuration to remove.
	 */
	public void removeAdditional_Configuration(IAdditionalConfig Additional_ConfigurationToRemove);

	/**
	 * Remove all the given Additional_Configurations from this MuleImplementation.
	 * Matching is done by the <code>isEquivalent(...)</code> function.
	 * @param Additional_Configurations Additional_Configuration to remove.
	 */
	public void removeAdditional_Configurations(Collection Additional_Configurations);

	/**
	 * Get all the Additional_Configurations from this MuleImplementation.
	 * @return all Additional_Configurations in this MuleImplementation.
	 */
	public Collection getAllAdditional_Configurations();

	/**
	 * Determine if this MuleImplementation contains a given Additional_Configuration.
	 * @return <code>true</code> if this MuleImplementation contains the given
	 * Additional_ConfigurationToCheck, <code>false</code> otherwise.
	 */
	public boolean hasAdditional_Configuration(IAdditionalConfig Additional_ConfigurationToCheck);

	/**
	 * Determine if this MuleImplementation contains the given set of Additional_Configurations.
	 * @param Additional_ConfigurationsToCheck Additional_Configurations to check for.
	 * @return Collection of <code>java.lang.Boolean</code>.  If the i<sup>th</sup>
	 * element in <code>Additional_Configurations</code> was found, then the i<sup>th</sup>
	 * element of the collection will be set to <code>true</code>, otherwise it
	 * will be set to <code>false</code>.  Matching is done with the
	 * <code>isEquivalent(...)</code> method.
	 */
	public Collection hasAdditional_Configurations(Collection Additional_ConfigurationsToCheck);

	/**
	 * Determine if this MuleImplementation contains each element in the 
	 * given set of Additional_Configurations.
	 * @param Additional_ConfigurationsToCheck Additional_Configurations to check for.
	 * @return <code>true</code> if every element in
	 * <code>Additional_Configurations</code> is found in this MuleImplementation,
	 * <code>false</code> otherwise.
	 */
	public boolean hasAllAdditional_Configurations(Collection Additional_ConfigurationsToCheck);

	/**
	 * Determine if another MuleImplementation is equivalent to this one, ignoring
	 * ID's.
	 * @param MuleImplementationToCheck MuleImplementation to compare to this one.
	 * @return <code>true</code> if all the child elements of this
	 * MuleImplementation are equivalent, <code>false</code> otherwise.
	 */
	public boolean isEquivalent(IMuleImplementation MuleImplementationToCheck);

}
