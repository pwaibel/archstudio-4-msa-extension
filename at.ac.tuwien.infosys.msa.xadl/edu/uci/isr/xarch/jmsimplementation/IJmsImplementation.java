/*
 * Copyright (c) 2000-2005 Regents of the University of California.
 * All rights reserved.
 *
 * This software was developed at the University of California, Irvine.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that the above copyright notice and this paragraph are
 * duplicated in all such forms and that any documentation,
 * advertising materials, and other materials related to such
 * distribution and use acknowledge that the software was developed
 * by the University of California, Irvine.  The name of the
 * University may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */
package edu.uci.isr.xarch.jmsimplementation;

import java.util.Collection;
import edu.uci.isr.xarch.XArchActionMetadata;
import edu.uci.isr.xarch.XArchTypeMetadata;
import edu.uci.isr.xarch.XArchPropertyMetadata;

/**
 * Interface for accessing objects of the
 * JmsImplementation <code>xsi:type</code> in the
 * jmsimplementation namespace.  Extends and
 * inherits the properties of the
 * Implementation <code>xsi:type</code>.
 * 
 * @author xArch apigen
 */
public interface IJmsImplementation extends edu.uci.isr.xarch.implementation.IImplementation, edu.uci.isr.xarch.IXArchElement{

	public final static XArchTypeMetadata TYPE_METADATA = new XArchTypeMetadata(
		XArchTypeMetadata.XARCH_ELEMENT,
		"jmsimplementation", "JmsImplementation", edu.uci.isr.xarch.implementation.IImplementation.TYPE_METADATA,
		new XArchPropertyMetadata[]{
			XArchPropertyMetadata.createAttribute("file_id", "http://www.w3.org/2001/XMLSchema", "string", null, null),
			XArchPropertyMetadata.createElement("Transport_Configuration", "jmsimplementation", "TransportConfig", 1, XArchPropertyMetadata.UNBOUNDED),
			XArchPropertyMetadata.createElement("Persistence_Configuration", "jmsimplementation", "PersistenceConfig", 0, 1),
			XArchPropertyMetadata.createElement("Jms_Specification_Version", "jmsimplementation", "JmsSpecificationType", 0, 1)},
		new XArchActionMetadata[]{});

	/**
	 * Set the file_id attribute on this JmsImplementation.
	 * @param file_id file_id
	 * @exception FixedValueException if the attribute has a fixed value
	 * and the value passed is not the fixed value.
	*/
	public void setFile_id(String file_id);

	/**
	 * Remove the file_id attribute from this JmsImplementation.
	 */
	public void clearFile_id();

	/**
	 * Get the file_id attribute from this JmsImplementation.
	 * if the attribute has a fixed value, this function will
	 * return that fixed value, even if it is not actually present
	 * in the XML document.
	 * @return file_id on this JmsImplementation
	 */
	public String getFile_id();

	/**
	 * Determine if the file_id attribute on this JmsImplementation
	 * has the given value.
	 * @param file_id Attribute value to compare
	 * @return <code>true</code> if they match; <code>false</code>
	 * otherwise.
	 */
	public boolean hasFile_id(String file_id);


	/**
	 * Add a Transport_Configuration to this JmsImplementation.
	 * @param newTransport_Configuration Transport_Configuration to add.
	 */
	public void addTransport_Configuration(ITransportConfig newTransport_Configuration);

	/**
	 * Add a collection of Transport_Configurations to this JmsImplementation.
	 * @param Transport_Configurations Transport_Configurations to add.
	 */
	public void addTransport_Configurations(Collection Transport_Configurations);

	/**
	 * Remove all Transport_Configurations from this JmsImplementation.
	 */
	public void clearTransport_Configurations();

	/**
	 * Remove the given Transport_Configuration from this JmsImplementation.
	 * Matching is done by the <code>isEquivalent(...)</code> function.
	 * @param Transport_ConfigurationToRemove Transport_Configuration to remove.
	 */
	public void removeTransport_Configuration(ITransportConfig Transport_ConfigurationToRemove);

	/**
	 * Remove all the given Transport_Configurations from this JmsImplementation.
	 * Matching is done by the <code>isEquivalent(...)</code> function.
	 * @param Transport_Configurations Transport_Configuration to remove.
	 */
	public void removeTransport_Configurations(Collection Transport_Configurations);

	/**
	 * Get all the Transport_Configurations from this JmsImplementation.
	 * @return all Transport_Configurations in this JmsImplementation.
	 */
	public Collection getAllTransport_Configurations();

	/**
	 * Determine if this JmsImplementation contains a given Transport_Configuration.
	 * @return <code>true</code> if this JmsImplementation contains the given
	 * Transport_ConfigurationToCheck, <code>false</code> otherwise.
	 */
	public boolean hasTransport_Configuration(ITransportConfig Transport_ConfigurationToCheck);

	/**
	 * Determine if this JmsImplementation contains the given set of Transport_Configurations.
	 * @param Transport_ConfigurationsToCheck Transport_Configurations to check for.
	 * @return Collection of <code>java.lang.Boolean</code>.  If the i<sup>th</sup>
	 * element in <code>Transport_Configurations</code> was found, then the i<sup>th</sup>
	 * element of the collection will be set to <code>true</code>, otherwise it
	 * will be set to <code>false</code>.  Matching is done with the
	 * <code>isEquivalent(...)</code> method.
	 */
	public Collection hasTransport_Configurations(Collection Transport_ConfigurationsToCheck);

	/**
	 * Determine if this JmsImplementation contains each element in the 
	 * given set of Transport_Configurations.
	 * @param Transport_ConfigurationsToCheck Transport_Configurations to check for.
	 * @return <code>true</code> if every element in
	 * <code>Transport_Configurations</code> is found in this JmsImplementation,
	 * <code>false</code> otherwise.
	 */
	public boolean hasAllTransport_Configurations(Collection Transport_ConfigurationsToCheck);


	/**
	 * Set the Persistence_Configuration for this JmsImplementation.
	 * @param value new Persistence_Configuration
	 */
	public void setPersistence_Configuration(IPersistenceConfig value);

	/**
	 * Clear the Persistence_Configuration from this JmsImplementation.
	 */
	public void clearPersistence_Configuration();

	/**
	 * Get the Persistence_Configuration from this JmsImplementation.
	 * @return Persistence_Configuration
	 */
	public IPersistenceConfig getPersistence_Configuration();

	/**
	 * Determine if this JmsImplementation has the given Persistence_Configuration
	 * @param Persistence_ConfigurationToCheck Persistence_Configuration to compare
	 * @return <code>true</code> if the Persistence_Configurations are equivalent,
	 * <code>false</code> otherwise
	 */
	public boolean hasPersistence_Configuration(IPersistenceConfig Persistence_ConfigurationToCheck);

	/**
	 * Set the Jms_Specification_Version for this JmsImplementation.
	 * @param value new Jms_Specification_Version
	 */
	public void setJms_Specification_Version(IJmsSpecificationType value);

	/**
	 * Clear the Jms_Specification_Version from this JmsImplementation.
	 */
	public void clearJms_Specification_Version();

	/**
	 * Get the Jms_Specification_Version from this JmsImplementation.
	 * @return Jms_Specification_Version
	 */
	public IJmsSpecificationType getJms_Specification_Version();

	/**
	 * Determine if this JmsImplementation has the given Jms_Specification_Version
	 * @param Jms_Specification_VersionToCheck Jms_Specification_Version to compare
	 * @return <code>true</code> if the Jms_Specification_Versions are equivalent,
	 * <code>false</code> otherwise
	 */
	public boolean hasJms_Specification_Version(IJmsSpecificationType Jms_Specification_VersionToCheck);
	/**
	 * Determine if another JmsImplementation is equivalent to this one, ignoring
	 * ID's.
	 * @param JmsImplementationToCheck JmsImplementation to compare to this one.
	 * @return <code>true</code> if all the child elements of this
	 * JmsImplementation are equivalent, <code>false</code> otherwise.
	 */
	public boolean isEquivalent(IJmsImplementation JmsImplementationToCheck);

}
