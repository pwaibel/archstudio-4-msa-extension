/*
 * Copyright (c) 2000-2005 Regents of the University of California.
 * All rights reserved.
 *
 * This software was developed at the University of California, Irvine.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that the above copyright notice and this paragraph are
 * duplicated in all such forms and that any documentation,
 * advertising materials, and other materials related to such
 * distribution and use acknowledge that the software was developed
 * by the University of California, Irvine.  The name of the
 * University may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */
package edu.uci.isr.xarch.channelimplementation;

import java.util.Collection;
import edu.uci.isr.xarch.XArchActionMetadata;
import edu.uci.isr.xarch.XArchTypeMetadata;
import edu.uci.isr.xarch.XArchPropertyMetadata;

/**
 * Interface for accessing objects of the
 * ChannelImplementation <code>xsi:type</code> in the
 * channelimplementation namespace.  Extends and
 * inherits the properties of the
 * Implementation <code>xsi:type</code>.
 * 
 * @author xArch apigen
 */
public interface IChannelImplementation extends edu.uci.isr.xarch.implementation.IImplementation, edu.uci.isr.xarch.IXArchElement{

	public final static XArchTypeMetadata TYPE_METADATA = new XArchTypeMetadata(
		XArchTypeMetadata.XARCH_ELEMENT,
		"channelimplementation", "ChannelImplementation", edu.uci.isr.xarch.implementation.IImplementation.TYPE_METADATA,
		new XArchPropertyMetadata[]{
			XArchPropertyMetadata.createElement("Queue_Configuration", "channelimplementation", "QueueConfig", 1, 1),
			XArchPropertyMetadata.createElement("Topic_Configuration", "channelimplementation", "TopicConfig", 1, 1)},
		new XArchActionMetadata[]{});

	/**
	 * Set the Queue_Configuration for this ChannelImplementation.
	 * @param value new Queue_Configuration
	 */
	public void setQueue_Configuration(IQueueConfig value);

	/**
	 * Clear the Queue_Configuration from this ChannelImplementation.
	 */
	public void clearQueue_Configuration();

	/**
	 * Get the Queue_Configuration from this ChannelImplementation.
	 * @return Queue_Configuration
	 */
	public IQueueConfig getQueue_Configuration();

	/**
	 * Determine if this ChannelImplementation has the given Queue_Configuration
	 * @param Queue_ConfigurationToCheck Queue_Configuration to compare
	 * @return <code>true</code> if the Queue_Configurations are equivalent,
	 * <code>false</code> otherwise
	 */
	public boolean hasQueue_Configuration(IQueueConfig Queue_ConfigurationToCheck);

	/**
	 * Set the Topic_Configuration for this ChannelImplementation.
	 * @param value new Topic_Configuration
	 */
	public void setTopic_Configuration(ITopicConfig value);

	/**
	 * Clear the Topic_Configuration from this ChannelImplementation.
	 */
	public void clearTopic_Configuration();

	/**
	 * Get the Topic_Configuration from this ChannelImplementation.
	 * @return Topic_Configuration
	 */
	public ITopicConfig getTopic_Configuration();

	/**
	 * Determine if this ChannelImplementation has the given Topic_Configuration
	 * @param Topic_ConfigurationToCheck Topic_Configuration to compare
	 * @return <code>true</code> if the Topic_Configurations are equivalent,
	 * <code>false</code> otherwise
	 */
	public boolean hasTopic_Configuration(ITopicConfig Topic_ConfigurationToCheck);
	/**
	 * Determine if another ChannelImplementation is equivalent to this one, ignoring
	 * ID's.
	 * @param ChannelImplementationToCheck ChannelImplementation to compare to this one.
	 * @return <code>true</code> if all the child elements of this
	 * ChannelImplementation are equivalent, <code>false</code> otherwise.
	 */
	public boolean isEquivalent(IChannelImplementation ChannelImplementationToCheck);

}
