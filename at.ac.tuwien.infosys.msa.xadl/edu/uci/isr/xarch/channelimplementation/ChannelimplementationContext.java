/*
 * Copyright (c) 2000-2005 Regents of the University of California.
 * All rights reserved.
 *
 * This software was developed at the University of California, Irvine.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that the above copyright notice and this paragraph are
 * duplicated in all such forms and that any documentation,
 * advertising materials, and other materials related to such
 * distribution and use acknowledge that the software was developed
 * by the University of California, Irvine.  The name of the
 * University may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */
package edu.uci.isr.xarch.channelimplementation;

import java.util.*;

import edu.uci.isr.xarch.*;

import org.w3c.dom.*;

import edu.uci.isr.xarch.IXArch;
import edu.uci.isr.xarch.IXArchContext;

/**
 * The context object for the channelimplementation package.
 * This object is used to create objects that are used
 * in the channelimplementation namespace.
 *
 * @author Automatically Generated by xArch apigen
 */
public class ChannelimplementationContext implements IChannelimplementationContext {

	protected static final String DEFAULT_ELT_NAME = "anonymousInstanceTag";
	protected Document doc;
	protected IXArch xArch;

	/**
	 * Create a new ChannelimplementationContext for the given
	 * IXArch object.
	 * @param xArch XArch object to contextualize in this namespace.
	 */
	public ChannelimplementationContext(IXArch xArch){
		if(!(xArch instanceof DOMBased)){
			throw new IllegalArgumentException("Cannot process non-DOM based xArch entities.");
		}
		Node docRootNode = ((DOMBased)xArch).getDOMNode();
		synchronized(DOMUtils.getDOMLock(docRootNode)){
			this.doc = docRootNode.getOwnerDocument();
			xArch.addSchemaLocation("at.ac.tuwien.infosys/arch/xArch/channelimplementation.xsd", "at.ac.tuwien.infosys/arch/xArch/channelimplementation.xsd");
			this.xArch = xArch;
		}
	}

	public IXArch getXArch(){
		return xArch;
	}
	
	protected Element createElement(String name){
		synchronized(DOMUtils.getDOMLock(doc)){
			return doc.createElementNS(ChannelimplementationConstants.NS_URI, name);
		}
	}

	public XArchTypeMetadata getTypeMetadata(){
		return IChannelimplementationContext.TYPE_METADATA;
	}
	/**
	 * Create an IConnectorImpl object in this namespace.
	 * @return New IConnectorImpl object.
	 */
	public IConnectorImpl createConnectorImpl(){
		Element elt = createElement(DEFAULT_ELT_NAME);
		DOMUtils.addXSIType(elt, ConnectorImplImpl.XSD_TYPE_NSURI, ConnectorImplImpl.XSD_TYPE_NAME);
		ConnectorImplImpl newElt = new ConnectorImplImpl(elt);
		newElt.setXArch(this.getXArch());
		return newElt;
	}

	/**
	 * Brings an IConnectorImpl object created in another
	 * context into this context.
	 * @param value Object to recontextualize.
	 * @return <code>value</code> object in this namespace.
	 */
	public IConnectorImpl recontextualizeConnectorImpl(IConnectorImpl value){
		if(!(value instanceof DOMBased)){
			throw new IllegalArgumentException("Cannot process non-DOM based xArch entities.");
		}
		Element elt = (Element)((DOMBased)value).getDOMNode();

		elt = DOMUtils.cloneAndRename(elt, doc, ChannelimplementationConstants.NS_URI, elt.getLocalName());
		//elt = DOMUtils.cloneAndRename(elt, ChannelimplementationConstants.NS_URI, elt.getTagName());

		//Removed next line because it causes an illegal character DOM exception
		//elt.setPrefix(null);

		((DOMBased)value).setDOMNode(elt);
		((IXArchElement)value).setXArch(this.getXArch());
		return value;
	}

	/**
	 * Promote an object of type <code>edu.uci.isr.xarch.types.IConnector</code>
	 * to one of type <code>IConnectorImpl</code>.  xArch APIs
	 * are structured in such a way that a regular cast is not possible
	 * to change interface types, so casting must be done through these
	 * promotion functions.  If the <code>edu.uci.isr.xarch.types.IConnector</code>
	 * object wraps a DOM element that is the base type, then the 
	 * <code>xsi:type</code> of the base element is promoted.  Otherwise, 
	 * it is left unchanged.
	 *
	 * This function also emits an <CODE>XArchEvent</CODE> with type
	 * PROMOTE_TYPE.  The source for this events is the pre-promoted
	 * IXArchElement object (should no longer be used), and the
	 * target is the post-promotion object.  The target name is
	 * the name of the interface class that was the target of the
	 * promotion.
	 * 
	 * @param value Object to promote.
	 * @return Promoted object.
	 */
	public IConnectorImpl promoteToConnectorImpl(
	edu.uci.isr.xarch.types.IConnector value){
		if(!(value instanceof DOMBased)){
			throw new IllegalArgumentException("Cannot process non-DOM based xArch entities.");
		}
		Element elt = (Element)((DOMBased)value).getDOMNode();

		if(DOMUtils.hasXSIType(elt, 
			edu.uci.isr.xarch.types.ConnectorImpl.XSD_TYPE_NSURI,
			edu.uci.isr.xarch.types.ConnectorImpl.XSD_TYPE_NAME)){

				DOMUtils.addXSIType(elt, ConnectorImplImpl.XSD_TYPE_NSURI, 
					ConnectorImplImpl.XSD_TYPE_NAME);
		}
		ConnectorImplImpl newElt = new ConnectorImplImpl(elt);
		newElt.setXArch(this.getXArch());

		xArch.fireXArchEvent(
			new XArchEvent(value, 
			XArchEvent.PROMOTE_EVENT,
			XArchEvent.ELEMENT_CHANGED,
			IConnectorImpl.class.getName(), newElt,
			XArchUtils.getDefaultXArchImplementation().isContainedIn(xArch, newElt))
		);

		return newElt;
	}

	/**
	 * Create an edu.uci.isr.xarch.implementation.IImplementation object in this namespace.
	 * @return New edu.uci.isr.xarch.implementation.IImplementation object.
	 */
	public edu.uci.isr.xarch.implementation.IImplementation createImplementation(){
		Element elt = createElement(DEFAULT_ELT_NAME);
		DOMUtils.addXSIType(elt, edu.uci.isr.xarch.implementation.ImplementationImpl.XSD_TYPE_NSURI, edu.uci.isr.xarch.implementation.ImplementationImpl.XSD_TYPE_NAME);
		edu.uci.isr.xarch.implementation.ImplementationImpl newElt = new edu.uci.isr.xarch.implementation.ImplementationImpl(elt);
		newElt.setXArch(this.getXArch());
		return newElt;
	}

	/**
	 * Brings an edu.uci.isr.xarch.implementation.IImplementation object created in another
	 * context into this context.
	 * @param value Object to recontextualize.
	 * @return <code>value</code> object in this namespace.
	 */
	public edu.uci.isr.xarch.implementation.IImplementation recontextualizeImplementation(edu.uci.isr.xarch.implementation.IImplementation value){
		if(!(value instanceof DOMBased)){
			throw new IllegalArgumentException("Cannot process non-DOM based xArch entities.");
		}
		Element elt = (Element)((DOMBased)value).getDOMNode();

		elt = DOMUtils.cloneAndRename(elt, doc, ChannelimplementationConstants.NS_URI, elt.getLocalName());
		//elt = DOMUtils.cloneAndRename(elt, ChannelimplementationConstants.NS_URI, elt.getTagName());

		//Removed next line because it causes an illegal character DOM exception
		//elt.setPrefix(null);

		((DOMBased)value).setDOMNode(elt);
		((IXArchElement)value).setXArch(this.getXArch());
		return value;
	}

	/**
	 * Create an IChannelImplementation object in this namespace.
	 * @return New IChannelImplementation object.
	 */
	public IChannelImplementation createChannelImplementation(){
		Element elt = createElement(DEFAULT_ELT_NAME);
		DOMUtils.addXSIType(elt, ChannelImplementationImpl.XSD_TYPE_NSURI, ChannelImplementationImpl.XSD_TYPE_NAME);
		ChannelImplementationImpl newElt = new ChannelImplementationImpl(elt);
		newElt.setXArch(this.getXArch());
		return newElt;
	}

	/**
	 * Brings an IChannelImplementation object created in another
	 * context into this context.
	 * @param value Object to recontextualize.
	 * @return <code>value</code> object in this namespace.
	 */
	public IChannelImplementation recontextualizeChannelImplementation(IChannelImplementation value){
		if(!(value instanceof DOMBased)){
			throw new IllegalArgumentException("Cannot process non-DOM based xArch entities.");
		}
		Element elt = (Element)((DOMBased)value).getDOMNode();

		elt = DOMUtils.cloneAndRename(elt, doc, ChannelimplementationConstants.NS_URI, elt.getLocalName());
		//elt = DOMUtils.cloneAndRename(elt, ChannelimplementationConstants.NS_URI, elt.getTagName());

		//Removed next line because it causes an illegal character DOM exception
		//elt.setPrefix(null);

		((DOMBased)value).setDOMNode(elt);
		((IXArchElement)value).setXArch(this.getXArch());
		return value;
	}

	/**
	 * Promote an object of type <code>edu.uci.isr.xarch.implementation.IImplementation</code>
	 * to one of type <code>IChannelImplementation</code>.  xArch APIs
	 * are structured in such a way that a regular cast is not possible
	 * to change interface types, so casting must be done through these
	 * promotion functions.  If the <code>edu.uci.isr.xarch.implementation.IImplementation</code>
	 * object wraps a DOM element that is the base type, then the 
	 * <code>xsi:type</code> of the base element is promoted.  Otherwise, 
	 * it is left unchanged.
	 *
	 * This function also emits an <CODE>XArchEvent</CODE> with type
	 * PROMOTE_TYPE.  The source for this events is the pre-promoted
	 * IXArchElement object (should no longer be used), and the
	 * target is the post-promotion object.  The target name is
	 * the name of the interface class that was the target of the
	 * promotion.
	 * 
	 * @param value Object to promote.
	 * @return Promoted object.
	 */
	public IChannelImplementation promoteToChannelImplementation(
	edu.uci.isr.xarch.implementation.IImplementation value){
		if(!(value instanceof DOMBased)){
			throw new IllegalArgumentException("Cannot process non-DOM based xArch entities.");
		}
		Element elt = (Element)((DOMBased)value).getDOMNode();

		if(DOMUtils.hasXSIType(elt, 
			edu.uci.isr.xarch.implementation.ImplementationImpl.XSD_TYPE_NSURI,
			edu.uci.isr.xarch.implementation.ImplementationImpl.XSD_TYPE_NAME)){

				DOMUtils.addXSIType(elt, ChannelImplementationImpl.XSD_TYPE_NSURI, 
					ChannelImplementationImpl.XSD_TYPE_NAME);
		}
		ChannelImplementationImpl newElt = new ChannelImplementationImpl(elt);
		newElt.setXArch(this.getXArch());

		xArch.fireXArchEvent(
			new XArchEvent(value, 
			XArchEvent.PROMOTE_EVENT,
			XArchEvent.ELEMENT_CHANGED,
			IChannelImplementation.class.getName(), newElt,
			XArchUtils.getDefaultXArchImplementation().isContainedIn(xArch, newElt))
		);

		return newElt;
	}

	/**
	 * Create an IQueueConfig object in this namespace.
	 * @return New IQueueConfig object.
	 */
	public IQueueConfig createQueueConfig(){
		Element elt = createElement(DEFAULT_ELT_NAME);
		DOMUtils.addXSIType(elt, QueueConfigImpl.XSD_TYPE_NSURI, QueueConfigImpl.XSD_TYPE_NAME);
		QueueConfigImpl newElt = new QueueConfigImpl(elt);
		newElt.setXArch(this.getXArch());
		return newElt;
	}

	/**
	 * Brings an IQueueConfig object created in another
	 * context into this context.
	 * @param value Object to recontextualize.
	 * @return <code>value</code> object in this namespace.
	 */
	public IQueueConfig recontextualizeQueueConfig(IQueueConfig value){
		if(!(value instanceof DOMBased)){
			throw new IllegalArgumentException("Cannot process non-DOM based xArch entities.");
		}
		Element elt = (Element)((DOMBased)value).getDOMNode();

		elt = DOMUtils.cloneAndRename(elt, doc, ChannelimplementationConstants.NS_URI, elt.getLocalName());
		//elt = DOMUtils.cloneAndRename(elt, ChannelimplementationConstants.NS_URI, elt.getTagName());

		//Removed next line because it causes an illegal character DOM exception
		//elt.setPrefix(null);

		((DOMBased)value).setDOMNode(elt);
		((IXArchElement)value).setXArch(this.getXArch());
		return value;
	}

	/**
	 * Create an ITopicConfig object in this namespace.
	 * @return New ITopicConfig object.
	 */
	public ITopicConfig createTopicConfig(){
		Element elt = createElement(DEFAULT_ELT_NAME);
		DOMUtils.addXSIType(elt, TopicConfigImpl.XSD_TYPE_NSURI, TopicConfigImpl.XSD_TYPE_NAME);
		TopicConfigImpl newElt = new TopicConfigImpl(elt);
		newElt.setXArch(this.getXArch());
		return newElt;
	}

	/**
	 * Brings an ITopicConfig object created in another
	 * context into this context.
	 * @param value Object to recontextualize.
	 * @return <code>value</code> object in this namespace.
	 */
	public ITopicConfig recontextualizeTopicConfig(ITopicConfig value){
		if(!(value instanceof DOMBased)){
			throw new IllegalArgumentException("Cannot process non-DOM based xArch entities.");
		}
		Element elt = (Element)((DOMBased)value).getDOMNode();

		elt = DOMUtils.cloneAndRename(elt, doc, ChannelimplementationConstants.NS_URI, elt.getLocalName());
		//elt = DOMUtils.cloneAndRename(elt, ChannelimplementationConstants.NS_URI, elt.getTagName());

		//Removed next line because it causes an illegal character DOM exception
		//elt.setPrefix(null);

		((DOMBased)value).setDOMNode(elt);
		((IXArchElement)value).setXArch(this.getXArch());
		return value;
	}


}

