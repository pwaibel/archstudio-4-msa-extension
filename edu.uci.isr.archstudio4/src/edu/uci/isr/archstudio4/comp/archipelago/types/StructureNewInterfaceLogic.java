package edu.uci.isr.archstudio4.comp.archipelago.types;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.ui.IWorkbenchActionConstants;

import edu.uci.isr.archstudio4.comp.archipelago.ArchipelagoServices;
import edu.uci.isr.archstudio4.comp.archipelago.ArchipelagoUtils;
import edu.uci.isr.archstudio4.util.ArchstudioResources;
import edu.uci.isr.bna4.AbstractThingLogic;
import edu.uci.isr.bna4.BNAUtils;
import edu.uci.isr.bna4.IBNAMenuListener;
import edu.uci.isr.bna4.IBNAView;
import edu.uci.isr.bna4.IThing;
import edu.uci.isr.bna4.things.glass.BoxGlassThing;
import edu.uci.isr.bna4.things.swt.SWTTextThing;
import edu.uci.isr.bna4.things.utility.EnvironmentPropertiesThing;
import edu.uci.isr.sysutils.UIDGenerator;
import edu.uci.isr.xadlutils.XadlUtils;
import edu.uci.isr.xarchflat.ObjRef;

public class StructureNewInterfaceLogic extends AbstractThingLogic implements IBNAMenuListener{
	protected ArchipelagoServices AS = null;
	protected ObjRef xArchRef = null;
	
	protected List<SWTTextThing> openControls = Collections.synchronizedList(new ArrayList<SWTTextThing>());
	
	public StructureNewInterfaceLogic(ArchipelagoServices services, ObjRef xArchRef){
		this.AS = services;
		this.xArchRef = xArchRef;
	}
	
	public boolean matches(IBNAView view, IThing t){
		if(t instanceof BoxGlassThing){
			IThing pt = view.getWorld().getBNAModel().getParentThing(t);
			if(pt != null){
				return StructureMapper.isBrickAssemblyRootThing(pt);
			}
		}
		return false;
	}
	
	public String getXArchID(IBNAView view, IThing t){
		if(t instanceof BoxGlassThing){
			IThing parentThing = view.getWorld().getBNAModel().getParentThing(t);
			return parentThing.getProperty(ArchipelagoUtils.XARCH_ID_PROPERTY_NAME);
		}
		return null;
	}
	
	public void fillMenu(IBNAView view, IMenuManager m, int localX, int localY, IThing t, int worldX, int worldY){
		IThing[] selectedThings = BNAUtils.getSelectedThings(view.getWorld().getBNAModel());
		if(selectedThings.length > 1) return;

		if(matches(view, t)){
			for(IAction action : getActions(view, t, worldX, worldY)){
				m.add(action);
			}
			m.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
		}
	}
	
	protected IAction[] getActions(IBNAView view, IThing t, int worldX, int worldY){
		final IBNAView fview = view;
		final IThing ft = t;
		final int fworldX = worldX;
		final int fworldY = worldY;
		
		final String eltXArchID = getXArchID(view, t);
		if(eltXArchID == null){
			//Nothing to set description on
			return new IAction[0];
		}
		
		final ObjRef eltRef = AS.xarch.getByID(xArchRef, eltXArchID);
		if(eltRef == null){
			//Nothing to set description on
			return new IAction[0];
		}
		
		//To record changes
		final EnvironmentPropertiesThing ept = BNAUtils.getEnvironmentPropertiesThing(view.getWorld().getBNAModel());		
		
		Action newInterfaceAction = new Action("New Interface", AS.resources.getImageDescriptor(ArchstudioResources.ICON_INTERFACE)){
			public void run(){
				ObjRef typesContextRef = AS.xarch.createContext(xArchRef, "types");
				ObjRef interfaceRef = AS.xarch.create(typesContextRef, "interface");
				AS.xarch.set(interfaceRef, "id", UIDGenerator.generateUID("interface"));
				XadlUtils.setDescription(AS.xarch, interfaceRef, "[New Interface]");
				XadlUtils.setDirection(AS.xarch, interfaceRef, "none");
				AS.xarch.add(eltRef, "interface", interfaceRef);
				
				String changesXArchID = ept.getProperty("ChangesID");
				if(changesXArchID == null){
					//Start a new change session
					ObjRef changesContextRef = AS.xarch.createContext(xArchRef, "changes");
					ObjRef archChangeRef = AS.xarch.getElement(changesContextRef, "archChange", xArchRef);			
					ObjRef changesRef = AS.xarch.create(changesContextRef, "changes");
					AS.xarch.set(changesRef, "id", UIDGenerator.generateUID("changes"));
					AS.xarch.set(changesRef, "status", "unmapped");
					String DATE_FORMAT_NOW = "yyyy-MM-dd HH:mm";
					Calendar cal = Calendar.getInstance();
					SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
					XadlUtils.setDescription(AS.xarch, changesRef, sdf.format(cal.getTime()));
					AS.xarch.add(archChangeRef, "changes", changesRef);
					//Set the environment indicator
					changesXArchID = XadlUtils.getID(AS.xarch, changesRef);
					ept.setProperty("ChangesID", changesXArchID);
				}				
				final ObjRef changesRef = AS.xarch.getByID(xArchRef, changesXArchID);
				if(changesRef == null){
					//Abnormal: no change session is set in the environment
					return;
				}
				//recording changes
				ObjRef changesContextRef = AS.xarch.createContext(xArchRef, "changes");
				ObjRef compChangeRef = AS.xarch.create(changesContextRef, "componentChange");
				AS.xarch.set(compChangeRef, "id", UIDGenerator.generateUID("componentChange"));
				AS.xarch.set(compChangeRef, "type", "update");
				XadlUtils.setDescription(AS.xarch, compChangeRef, "New interface");
				XadlUtils.setXLink(AS.xarch, compChangeRef, "component", eltRef);
				//Add interface change - START
				ObjRef intfChangeRef = AS.xarch.create(changesContextRef, "interfaceChange");
				AS.xarch.set(intfChangeRef, "id", UIDGenerator.generateUID("interfaceChange"));
				AS.xarch.set(intfChangeRef, "type", "add");
				XadlUtils.setXLink(AS.xarch, intfChangeRef, "interface", interfaceRef);
				AS.xarch.set(compChangeRef, "interfaceChange", intfChangeRef);
				//Add interface change - END
				AS.xarch.add(changesRef, "componentChange", compChangeRef);				
			}
		};
		
		return new IAction[]{newInterfaceAction};
	}
	
}
