package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem.xml.mule.update;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem.xml.mule.MuleGenerateXmlHelper;

public abstract class AbstractMuleUpdate {

	protected MuleGenerateXmlHelper generateHelper = new MuleGenerateXmlHelper();
	protected Document doc;
	
	public AbstractMuleUpdate(Document doc) {
		this.doc = doc;
	}
	
	protected boolean checkForArchIdComment(Element element, String archStudioId) {
		
		NodeList nodeList = element.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++) {
	        if (nodeList.item(i).getNodeType() == Element.COMMENT_NODE) {
	            Comment comment=(Comment) nodeList.item(i);
	            if(comment.getTextContent().contains("ArchStudioId=" + archStudioId)) {
	            	return true;
	            }
	        }
	    }
		
		return false;
	}
	
	protected List<Node> findAllArchStudioIdComments(Node startPoint) {

		List<Node> allComments = new ArrayList<Node>();
		
		findAllArchStudioIdCommentsRecursive(startPoint, allComments);
	
		return allComments;
	}
	
	
	protected List<Node> findAllArchStudioIdCommentsRecursive(Node node, List<Node> allComments) {

	    NodeList nodeList = node.getChildNodes();
	    for (int i = 0; i < nodeList.getLength(); i++) {
	        Node currentNode = nodeList.item(i);
	        if (currentNode.getNodeType() == Node.COMMENT_NODE) {
	        	allComments.add(currentNode);
	        }
	        findAllArchStudioIdCommentsRecursive(currentNode, allComments);
	    }
	    
	    return allComments;
	}
	
	protected boolean isFlowComment(Node commentElement) {
		Node element = commentElement.getParentNode();
		return element.getNodeName().equals("flow");
	}
	
	protected boolean isConnectorComment(Node commentElement) {
		Node element = commentElement.getParentNode();
		return element.getNodeName().contains("-connector");
	}
	
	protected boolean isEndpointComment(Node commentElement) {
		Node element = commentElement.getParentNode();
		return element.getNodeName().contains("-endpoint");
	}
	
	
}
