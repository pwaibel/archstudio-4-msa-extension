package at.ac.tuwien.infosys.msa.archstudio4.comp;

public class MessaginSystemGenerationException extends Exception {
	
	private static final long serialVersionUID = 4185671404281211640L;

	public MessaginSystemGenerationException(){
		super();
	}

	public MessaginSystemGenerationException(String message){
		super(message);
	}

	public MessaginSystemGenerationException(Throwable cause){
		super(cause);
	}

	public MessaginSystemGenerationException(String message, Throwable cause){
		super(message, cause);
	}

}
