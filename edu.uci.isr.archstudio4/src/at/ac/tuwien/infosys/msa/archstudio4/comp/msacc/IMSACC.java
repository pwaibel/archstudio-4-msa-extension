package at.ac.tuwien.infosys.msa.archstudio4.comp.msacc;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.checks.ArchitectureConsistencyException;
import edu.uci.isr.xarchflat.ObjRef;

public interface IMSACC {

	public void architectureConsistencyCheck(String name, ObjRef xArchRef, ObjRef structureRef) throws ArchitectureConsistencyException;
	
}
