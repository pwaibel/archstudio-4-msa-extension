package at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.checks;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import at.ac.tuwien.infosys.msa.archstudio4.comp.GeneralHelper;
import edu.uci.isr.xadlutils.XadlUtils;
import edu.uci.isr.xarchflat.ObjRef;
import edu.uci.isr.xarchflat.XArchFlatInterface;

/**
 * Component Consistency Checks: Errors:
 * <ul>
 * <li>Check if a Request-Reply composition has an IN and an OUT interface.</li>
 * <li>If one interface has an order number, all interfaces need one.</li>
 * <li>If more then one INOUT-interface or Request-Reply group exists, the
 * interfaces have to be organized manually.</li>
 * <li>Check if Request-Reply interfaces are set correctly (one interface can only
 * has the attribute Reply_To_Queue or Connection_To_Request_Endpoint).</li>
 * <li>Check if not more than one IN-interface and one OUT-interface are set.
 * </ul>
 * 
 * Warnings:
 * <ul>
 * <li>Check if all components have an implementation</li>
 * </ul>
 * 
 * @author philippwaibel
 * 
 */
public class ComponentConsistencyCheck extends AbstractConsistencyCheck {

	private boolean inInterface = false;
	private boolean outInterface = false;

	private List<ObjRef> inoutInterface = new ArrayList<ObjRef>();
	private int positionInterfacesCounter = 0;

	private List<ObjRef> requestInterface = new ArrayList<ObjRef>();
	private List<ObjRef> responseInterface = new ArrayList<ObjRef>();

	public ComponentConsistencyCheck(XArchFlatInterface xarch) {
		super(xarch);
	}

	@Override
	public boolean check(Set<ObjRef> componentRefs, Set<ObjRef> connectorRefs, ObjRef structureRef) throws ArchitectureConsistencyException {

		for (ObjRef componentRef : componentRefs) {
			ObjRef[] interfaceRefs = xarch.getAll(componentRef, "interface");

			implementationCheck(componentRef);

			inInterface = false;
			outInterface = false;
			inoutInterface = new ArrayList<ObjRef>();
			positionInterfacesCounter = 0;
			requestInterface = new ArrayList<ObjRef>();
			responseInterface = new ArrayList<ObjRef>();

			for (ObjRef interfaceRef : interfaceRefs) {
				ObjRef directionRef = (ObjRef) xarch.get(interfaceRef, "direction");
				String directionString = (String) xarch.get(directionRef, "value");

				if (directionString.equals("in")) {
					endpointProcess(interfaceRef, componentRef, true);
				} else if (directionString.equals("out")) {
					endpointProcess(interfaceRef, componentRef, false);
				} else if (directionString.equals("inout")) {
					inoutInterface.add(interfaceRef);
				}

				if (hasPositionNo(interfaceRef)) {
					positionInterfacesCounter++;
				}

			}

			postCheck(componentRef);

		}
		return true;
	}

	private boolean implementationCheck(ObjRef componentBrickRef) throws ArchitectureConsistencyException {

		ObjRef muleImplementationRef = GeneralHelper.getMuleImplementationRef(xarch, componentBrickRef);
		if (muleImplementationRef != null) {
			return true;
		} else {
			String message = "The component \"" + XadlUtils.getDescription(xarch, componentBrickRef) + "\" doesn't have an implementation.";
			ArchitectureWarning architectureWarning = new ArchitectureWarning();
			boolean shouldCancle = architectureWarning.showArchitectureWarning(message);
			if (shouldCancle) {
				throw new ArchitectureConsistencyException(message);
			}
		}

		return false;

	}

	private boolean postCheck(ObjRef componentRef) throws ArchitectureConsistencyException {

		int numberOfRequestResponseInterfaces = 0;
		Iterator<ObjRef> responseIterator = responseInterface.iterator();

		while (responseIterator.hasNext()) {
			ObjRef responseRef = responseIterator.next();
			ObjRef endpointImplementationRef = GeneralHelper.getEndpointImplementationRef(xarch, responseRef);
			ObjRef requestEndpoint = (ObjRef) XadlUtils.resolveXLink(xarch, endpointImplementationRef, "Connection_To_Request_Endpoint");

			if (requestInterface.contains(requestEndpoint)) {
				requestInterface.remove(requestEndpoint);
				responseIterator.remove();
				numberOfRequestResponseInterfaces++;
			}
		}

		if (positionInterfacesCounter == 0 && (
				(inInterface == true  && outInterface == false && inoutInterface.size() == 0 && numberOfRequestResponseInterfaces == 0) || 
				(inInterface == true  && outInterface == true  && inoutInterface.size() == 0 && numberOfRequestResponseInterfaces == 0) || 
				(inInterface == true  && outInterface == false && inoutInterface.size() == 1 && numberOfRequestResponseInterfaces == 0) || 
				(inInterface == true  && outInterface == false && inoutInterface.size() == 0 && numberOfRequestResponseInterfaces == 1) || 
				(inInterface == true  && outInterface == false && inoutInterface.size() == 1 && numberOfRequestResponseInterfaces == 0) || 
				(inInterface == true  && outInterface == false && inoutInterface.size() == 0 && numberOfRequestResponseInterfaces == 1) || 
				(inInterface == false && outInterface == true  && inoutInterface.size() == 1 && numberOfRequestResponseInterfaces == 0) || 
				(inInterface == false && outInterface == true  && inoutInterface.size() == 0 && numberOfRequestResponseInterfaces == 1) || 
				(inInterface == false && outInterface == false && inoutInterface.size() == 0 && numberOfRequestResponseInterfaces == 1) || 
				(inInterface == false && outInterface == true  && inoutInterface.size() == 0 && numberOfRequestResponseInterfaces == 0))) {
			return true;
		} else {
			int interfaceCount = inoutInterface.size() + numberOfRequestResponseInterfaces * 2;
			if (inInterface) {
				interfaceCount++;
			}
			if (outInterface) {
				interfaceCount++;
			}

			if (positionInterfacesCounter > 0 && interfaceCount != positionInterfacesCounter) {
				throw new ArchitectureConsistencyException("Not all interfaces of the component \"" + XadlUtils.getDescription(xarch, componentRef) + "\" are organized by the attribute Endpoint_Position_No. If at least one interface has the attribute, all interfaces have to be organized manually.");
			}

			if (positionInterfacesCounter == 0 && (inoutInterface.size() + numberOfRequestResponseInterfaces) > 1) {
				throw new ArchitectureConsistencyException("The interfaces of component \"" + XadlUtils.getDescription(xarch, componentRef) + "\" can not be arranged automatically. To get the right order please use Endpoint_Position_No to arrange them manually.");
			}
		}

		return true;
	}

	private boolean hasPositionNo(ObjRef interfaceRef) throws ArchitectureConsistencyException {
		if (xarch.isInstanceOf(interfaceRef, "endpointimplementation#InterfaceImpl")) {
			ObjRef endpointImplementationRef = GeneralHelper.getEndpointImplementationRef(xarch, interfaceRef);
			if (endpointImplementationRef != null) {
				ObjRef endpointRef = (ObjRef) xarch.get(endpointImplementationRef, "Endpoint_Position_No");
				if (endpointRef != null) {
					String positionString = (String) xarch.get(endpointRef, "value");
					if (positionString != null && !positionString.isEmpty()) {
						return true;
					}
				}
			}
		}
		return false;
	}

	private void endpointProcess(ObjRef interfaceRef, ObjRef componentRef, boolean inEndpoint) throws ArchitectureConsistencyException {

		ObjRef requestReplyEndpoint = null;
		boolean replyToQueueSet = false;
		boolean connectionToRequestSet = false;

		if (xarch.isInstanceOf(interfaceRef, "endpointimplementation#InterfaceImpl")) {

			ObjRef endpointImplementationRef = GeneralHelper.getEndpointImplementationRef(xarch, interfaceRef);
			if (endpointImplementationRef != null) {
				if ((ObjRef) xarch.get(endpointImplementationRef, "Reply_To_Queue") != null && (ObjRef) xarch.get(endpointImplementationRef, "Connection_To_Request_Endpoint") != null) {
					throw new ArchitectureConsistencyException("It is not allowed to set Reply_To_Queue and Connection_To_Request_Endpoint at the same interface. Found in component \"" + XadlUtils.getDescription(xarch, componentRef) + "\".");
				} else if ((ObjRef) xarch.get(endpointImplementationRef, "Reply_To_Queue") != null) {
					requestReplyEndpoint = interfaceRef;
					replyToQueueSet = true;
					connectionToRequestSet = false;
					checkRequestReplyInterfaces(endpointImplementationRef, componentRef, "Reply_To_Queue", "Connection_To_Request_Endpoint");
				} else if ((ObjRef) xarch.get(endpointImplementationRef, "Connection_To_Request_Endpoint") != null) {
					requestReplyEndpoint = interfaceRef;
					replyToQueueSet = false;
					connectionToRequestSet = true;
					checkRequestReplyInterfaces(endpointImplementationRef, componentRef, "Connection_To_Request_Endpoint", "Reply_To_Queue");
				}
			}
		}
		if (inEndpoint) {
			inboundEndpoint(requestReplyEndpoint, componentRef, replyToQueueSet, connectionToRequestSet);
		} else {
			outboundEndpoint(requestReplyEndpoint, componentRef, replyToQueueSet, connectionToRequestSet);
		}
	}

	private void checkRequestReplyInterfaces(ObjRef endpointImplementationRef, ObjRef componentRef, String from, String to) throws ArchitectureConsistencyException {
		ObjRef referredInterfaceRef = (ObjRef) XadlUtils.resolveXLink(xarch, endpointImplementationRef, from);
		boolean correctReference = false;

		if (xarch.isInstanceOf(referredInterfaceRef, "endpointimplementation#InterfaceImpl")) {
			ObjRef referredEndpointImplementationRef = GeneralHelper.getEndpointImplementationRef(xarch, referredInterfaceRef);

			if (referredEndpointImplementationRef != null) {
				ObjRef parentBrickRef = xarch.getParent(referredInterfaceRef);
				if ((ObjRef) xarch.get(referredEndpointImplementationRef, to) != null && parentBrickRef == componentRef) {
					correctReference = true;
				}
			}
		}

		if (!correctReference) {
			throw new ArchitectureConsistencyException("A Request-Reply composition of the component \"" + XadlUtils.getDescription(xarch, componentRef) + "\"  has only one interface or the link between the request and the replying interface is wrong. The Response-Endpoint has to be linked to the Request-Endpoint and vice versa.");
		}
	}

	private void inboundEndpoint(ObjRef requestReplyEndpoint, ObjRef componentRef, boolean replyToQueueSet, boolean connectionToRequestSet) throws ArchitectureConsistencyException {

		if (requestReplyEndpoint != null) { // IN interface is a request reply endpoint
			if (replyToQueueSet && !connectionToRequestSet) {
				requestInterface.add(requestReplyEndpoint);
			} else if (!replyToQueueSet && connectionToRequestSet) {
				responseInterface.add(requestReplyEndpoint);
			}
		} else { // IN interface is a normal endpoint
			if (inInterface) { // IN interface already set
				throw new ArchitectureConsistencyException("The component \"" + XadlUtils.getDescription(xarch, componentRef) + "\" has more than one IN-interface. Without Request-Reply only one IN-interface is allowed.");
			} else {
				inInterface = true;
			}
		}
	}

	private void outboundEndpoint(ObjRef requestReplyEndpoint, ObjRef componentRef, boolean replyToQueueSet, boolean connectionToRequestSet) throws ArchitectureConsistencyException {

		if (requestReplyEndpoint != null) { // OUT interface is a request reply
											// endpoint
			if (replyToQueueSet && !connectionToRequestSet) {
				requestInterface.add(requestReplyEndpoint);
			} else if (!replyToQueueSet && connectionToRequestSet) {
				responseInterface.add(requestReplyEndpoint);
			}
		} else { // OUT interface is a normal endpoint
			if (outInterface) { // OUT interface already set
				throw new ArchitectureConsistencyException("The component \"" + XadlUtils.getDescription(xarch, componentRef) + "\" has more than one OUT-interface. Without Request-Reply only one OUT-interface is allowed.");
			} else {
				outInterface = true;
			}

		}

	}
}
