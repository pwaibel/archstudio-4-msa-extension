package at.ac.tuwien.infosys.msa.archstudio4.comp.msglauncher.gui;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerColumn;

public abstract class CellEditLabelProvider extends ColumnLabelProvider {

	protected TableViewer viewer;
	
	@Override
	protected void initialize(ColumnViewer viewer, ViewerColumn column) {
		super.initialize(viewer, column);
        this.viewer = null;
        if (viewer instanceof TableViewer) {
            this.viewer = (TableViewer) viewer;
        }
	}

}
