package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector;

import java.util.ArrayList;
import java.util.List;

import edu.uci.isr.sysutils.UIDGenerator;

/**
 * This class represent the specific JMS connector informations from the XADL file
 * @author philippwaibel
 *
 */
public class JmsConnectorConfig implements IJmsConnectorConfig {
	
	private String name = "";
	private String brokerUrl = "";
	private String specificationVersion = "1.1";
	private boolean durable = false;
	private String clientId = "";					// will be set to an unique string
	private List<PersistenceAdditionalConfig> additionalConfigs = new ArrayList<PersistenceAdditionalConfig>();
	

	public JmsConnectorConfig(String brokerUrl, String specificationVersion) {
		super();		
		this.brokerUrl = brokerUrl;
		this.specificationVersion = specificationVersion;
	}
	
	
	@Override
	public boolean isDurable() {
		return durable;
	}

	@Override
	public void setDurable(boolean durable) {
		this.durable = durable;
	}

	@Override
	public String getClientId() {
		clientId = "Client_Id_" + UIDGenerator.generateUID();
		return clientId;
	}

	@Override
	public List<PersistenceAdditionalConfig> getPersistenceConfigs() {
		return additionalConfigs;
	}

	public void setPersistenceConfigs(List<PersistenceAdditionalConfig> additionalConfigs) {
		this.additionalConfigs = additionalConfigs;
	}

	@Override
	public void addPersistenceConfigs(PersistenceAdditionalConfig additionalConfigs) {
		this.additionalConfigs.add(additionalConfigs);
	}
	
	@Override
	public void setUniqName(String name) {
		this.name = name;
	}
	
	@Override
	public String getUniqName() {
		if(name.isEmpty()) {
			name = generateUniqName();
		}
		return name;
	}
	
	@Override
	public String generateUniqName() {
		name = "JMS_Connector_" + UIDGenerator.generateUID();		
		return name;
	}

	@Override
	public String getBrokerUrl() {
		return this.brokerUrl;
	}

	@Override
	public String getSpecificationVersion() {
		return specificationVersion;
	}

	@Override
	public boolean equals(Object obj) {
		
		if(obj instanceof JmsConnectorConfig) {
			JmsConnectorConfig testObj = (JmsConnectorConfig)obj;
			if(this.brokerUrl.equals(testObj.brokerUrl) &&
			   this.name.equals(testObj.name)) {
				return true;
			}
		}
		
		return false;		
	}
	
}
