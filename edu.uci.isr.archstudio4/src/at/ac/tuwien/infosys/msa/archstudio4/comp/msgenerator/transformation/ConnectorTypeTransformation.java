package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.transformation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import at.ac.tuwien.infosys.msa.archstudio4.comp.GeneralHelper;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.IJmsConnectorConfig;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.JmsConnectorConfig;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.PersistenceAdditionalConfig;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.XadlConnectorConfiguration;
import edu.uci.isr.xadlutils.XadlUtils;
import edu.uci.isr.xarchflat.ObjRef;
import edu.uci.isr.xarchflat.XArchFlatInterface;

public class ConnectorTypeTransformation extends AbstractTransformation {

	private final static Logger LOGGER = Logger.getLogger(ConnectorTypeTransformation.class.getName());

	private Map<ObjRef, XadlConnectorConfiguration> connectorMap = new HashMap<ObjRef, XadlConnectorConfiguration>();

	public ConnectorTypeTransformation(XArchFlatInterface xarch) {
		super(xarch);
	}

	/**
	 * Generates a Map of Connector Brick Reference to Connector Configuration
	 * 
	 * @param structureRef
	 * @return Key: Connector ObjRef; Value: ConnectorConfiguration
	 */
	public void transformConnectors(ObjRef structureRef) {

		List<XadlConnectorConfiguration> connectorConfigList = new ArrayList<XadlConnectorConfiguration>();

		for (ObjRef connectorBrickRef : xarch.getAll(structureRef, "connector")) {
			
			XadlConnectorConfiguration connectorConfig = null;
			String archStudioId = (String) xarch.get(connectorBrickRef, "id");
			ObjRef connectorTypeRef = XadlUtils.resolveXLink(xarch, connectorBrickRef, "type");

			ObjRef jmsImplementationRef = GeneralHelper.getJmsImplementationRef(xarch, connectorTypeRef);
			if (jmsImplementationRef != null) {

				IJmsConnectorConfig jmsConnector = generateJmsConnector(jmsImplementationRef);
				boolean found = false;
				for (XadlConnectorConfiguration conConfig : connectorConfigList) {
					if (conConfig.getJmsConnector().equals(jmsConnector)) {
						conConfig.addConnectorRef(connectorBrickRef);
						connectorConfig = conConfig;
						found = true;
						break;
					}
				}
				if (!found) {
					String fileId = (String) xarch.get(jmsImplementationRef, "file_id");
					connectorConfig = new XadlConnectorConfiguration();
					connectorConfig.setJmsConnector(jmsConnector);
					connectorConfig.setFileId(fileId);
					connectorConfig.setArchStudioId(archStudioId);
					connectorConfig.addConnectorRef(connectorBrickRef);
					connectorConfigList.add(connectorConfig);
				}

				connectorMap.put(connectorBrickRef, connectorConfig);	
				
			}
		}
	}


	private IJmsConnectorConfig generateJmsConnector(ObjRef jmsImplementationRef) {

		IJmsConnectorConfig jmsConnector = null;
		
		ObjRef specificationVersionRef = (ObjRef) xarch.get(jmsImplementationRef, "Jms_Specification_Version");
		String specificationVersion = null;
		if (specificationVersionRef != null) {
			specificationVersion = (String) xarch.get(specificationVersionRef, "value");
		}
		if (specificationVersion == null || specificationVersion.isEmpty()) {
			specificationVersion = "1.1";
		}

		for (ObjRef transportConfig : xarch.getAll(jmsImplementationRef, "Transport_Configuration")) {
			String transportConnector = (String) xarch.get(transportConfig, "transportConnector");
			jmsConnector = new JmsConnectorConfig(transportConnector, specificationVersion);
			LOGGER.fine("Queue Connector: " + transportConnector);
		}

		ObjRef perRef = (ObjRef) xarch.get(jmsImplementationRef, "Persistence_Configuration");
		if (perRef != null) {
			String adapterName = (String) xarch.get(perRef, "adapter");
			String directoryPath = (String) xarch.get(perRef, "directory");
			PersistenceAdditionalConfig persistenConfig = new PersistenceAdditionalConfig(directoryPath, adapterName);

			jmsConnector.addPersistenceConfigs(persistenConfig);
		}
		return jmsConnector;
	}

	public Map<ObjRef, XadlConnectorConfiguration> getConnectorMap() {
		return connectorMap;
	}

}
