package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.transformation;

import java.util.HashMap;
import java.util.Map;

import at.ac.tuwien.infosys.msa.archstudio4.comp.GeneralHelper;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.MSAInterfaceDirection;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.component.AdditionalConfig;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.component.IComponentConfig;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.component.MuleComponentConfig;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.component.MuleInterfaceConfiguration;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.component.XadlComponentConfiguration;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.component.XadlInterfaceConfiguration;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.XadlConnectorConfiguration;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.channel.IJmsChannel;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.workflow.ConnectionEndpoint;
import edu.uci.isr.sysutils.UIDGenerator;
import edu.uci.isr.xadlutils.XadlUtils;
import edu.uci.isr.xarchflat.ObjRef;
import edu.uci.isr.xarchflat.XArchFlatInterface;

public class ComponentTransformation extends AbstractTransformation {

	private Map<ObjRef, ComponentData> componentMap = new HashMap<ObjRef, ComponentData>();
	private Map<ObjRef, String> interface_to_correlationId = new HashMap<ObjRef, String>();

	public ComponentTransformation(XArchFlatInterface xarch) {
		super(xarch);
	}

	/**
	 * 
	 * @param structureRef
	 * @return Key: Component ObjRef; Value: List of Connector ObjRefs with
	 *         Directions
	 */
	public void transformComponents(ObjRef structureRef, Map<ObjRef, XadlConnectorConfiguration> connectorMap, Map<ObjRef, IJmsChannel> connector_to_ChannelMap, Map<ObjRef, IJmsChannel> componentInterface_To_ChannelMap) {

		for (ObjRef linkRef : xarch.getAll(structureRef, "link")) {
			ObjRef[] pointRefs = xarch.getAll(linkRef, "point");

			ObjRef componentBrickRef = null;
			ObjRef connectorBrickRef = null;
			MSAInterfaceDirection direction = null;
			String componentArchStudioId = "";
			IJmsChannel jmsChannel = null;
			boolean componentHasImplementation = false;
			
			InterfaceTransformation interfaceTransformer = new InterfaceTransformation(xarch);

			for (int i = 0; i < pointRefs.length; i++) {
				ObjRef interfaceRef = XadlUtils.resolveXLink(xarch, pointRefs[i], "anchorOnInterface");
				ObjRef parentBrickRef = xarch.getParent(interfaceRef);

				if (xarch.isInstanceOf(parentBrickRef, "types#Component")) {
					componentBrickRef = parentBrickRef;
					
					XadlComponentConfiguration compConfig = getComponentConfiguration(componentBrickRef);
					if(compConfig != null) {
						componentHasImplementation = true;
						
						componentArchStudioId = (String) xarch.get(interfaceRef, "id");
						
						interfaceTransformer.process(interfaceRef, componentInterface_To_ChannelMap);

						direction = interfaceTransformer.getDirection();
						
						setXadlComponentConfig(componentBrickRef, compConfig);
					}
					
				} else if (xarch.isInstanceOf(parentBrickRef, "types#Connector")) {
					connectorBrickRef = parentBrickRef;
					jmsChannel = connector_to_ChannelMap.get(connectorBrickRef);
				}
			}

			if (componentHasImplementation && interfaceTransformer.isGenerateInterface()) {
				ConnectionEndpoint conEndpoint = new ConnectionEndpoint();

				conEndpoint.setConnectorConfig(connectorMap.get(connectorBrickRef));
				conEndpoint.setJmsChannel(jmsChannel);

				XadlInterfaceConfiguration interfaceConfig = new XadlInterfaceConfiguration();
				interfaceConfig.setArchStudioId(componentArchStudioId);
				
				MuleInterfaceConfiguration muleInterfaceConfig = new MuleInterfaceConfiguration();
				muleInterfaceConfig.setDirection(direction);
				
				muleInterfaceConfig.setDurableName(interfaceTransformer.getDurableName());
				muleInterfaceConfig.setGenerateInteface(interfaceTransformer.isGenerateInterface());
				if (!interfaceTransformer.getDurableName().isEmpty()) {
					conEndpoint.getConnectorConfig().getJmsConnector().setDurable(true);
				}
				muleInterfaceConfig.setReplyChannelName(interfaceTransformer.getReplyChannelName());

				if (interfaceTransformer.hasReplyChannelName() || interfaceTransformer.getRequestEndpoint() != null) {

					if (interfaceTransformer.getRequestEndpoint() == null) {
						setInterfaceCorrelationId(interfaceTransformer.getInterfaceRef(), muleInterfaceConfig);
					} else {
						setInterfaceCorrelationId(interfaceTransformer.getRequestEndpoint(), muleInterfaceConfig);						
					}
				}				
				
				interfaceConfig.setMuleInterfaceConfiguration(muleInterfaceConfig);
				
				conEndpoint.setInterfaceConfig(interfaceConfig);

				setConnectionEndpoint(componentBrickRef, conEndpoint);

				if (interfaceTransformer.getOrderNo() != null) {
					setConnectionEndpointOrder(componentBrickRef, conEndpoint, interfaceTransformer.getOrderNo());
				}
			}
		}
	}

	private void setInterfaceCorrelationId(ObjRef reference, MuleInterfaceConfiguration muleInterfaceConfig) {
		if (!interface_to_correlationId.containsKey(reference)) {
			interface_to_correlationId.put(reference, UIDGenerator.generateUID());
		}		
		muleInterfaceConfig.setRequestReplyEndpointId(interface_to_correlationId.get(reference));
	}

	private XadlComponentConfiguration getComponentConfiguration(ObjRef componentBrickRef) {
		ObjRef descriptionRef = (ObjRef) xarch.get(componentBrickRef, "description");
		String name = (String) xarch.get(descriptionRef, "value");

		String archStudioId = (String) xarch.get(componentBrickRef, "id");

		IComponentConfig comp = null;

		ObjRef muleImplementationRef = GeneralHelper.getMuleImplementationRef(xarch, componentBrickRef);
		if (muleImplementationRef != null) {

			comp = new MuleComponentConfig(name);

			for (ObjRef additinalConfigRef : xarch.getAll(muleImplementationRef, "Additional_Configuration")) {
				String additionalConfigName = (String) xarch.get(additinalConfigRef, "name");
				String additionalConfigValue = (String) xarch.get(additinalConfigRef, "value");
				comp.addAdditionalConfigs(new AdditionalConfig(additionalConfigName, additionalConfigValue));
			}
			
			String fileId = (String) xarch.get(muleImplementationRef, "file_id");

			return new XadlComponentConfiguration(componentBrickRef, comp, archStudioId, fileId);

		}
		else {
			return null;
		}
		
	}

	private void setXadlComponentConfig(ObjRef componentBrickRef, XadlComponentConfiguration componentConfig) {
		if (!componentMap.containsKey(componentBrickRef)) {
			ComponentData componentBean = new ComponentData();
			componentMap.put(componentBrickRef, componentBean);
		}
		componentMap.get(componentBrickRef).setXadlComponentConfig(componentConfig);
	}

	private void setConnectionEndpoint(ObjRef componentBrickRef, ConnectionEndpoint connectionEndpoint) {
		if (!componentMap.containsKey(componentBrickRef)) {
			ComponentData componentBean = new ComponentData();
			componentMap.put(componentBrickRef, componentBean);
		}
		componentMap.get(componentBrickRef).getConnectionEndpoints().add(connectionEndpoint);
	}

	private void setConnectionEndpointOrder(ObjRef componentBrickRef, ConnectionEndpoint connectionEndpoint, Integer connectionEndpointOrder) {
		if (!componentMap.containsKey(componentBrickRef)) {
			ComponentData componentBean = new ComponentData();
			componentMap.put(componentBrickRef, componentBean);
		}
		componentMap.get(componentBrickRef).getConnectionEndpointsOrder().put(connectionEndpoint, connectionEndpointOrder);
	}

	public Map<ObjRef, ComponentData> getComponentMap() {
		return componentMap;
	}

}
