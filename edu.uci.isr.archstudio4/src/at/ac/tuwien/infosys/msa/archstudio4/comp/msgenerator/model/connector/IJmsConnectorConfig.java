package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector;

import java.util.List;

public interface IJmsConnectorConfig {

	public String getBrokerUrl();
	public String getUniqName();
	public String getSpecificationVersion();
	public String generateUniqName();
	public String getClientId();
	public boolean isDurable();
	public List<PersistenceAdditionalConfig> getPersistenceConfigs();
	public void addPersistenceConfigs(PersistenceAdditionalConfig additionalConfigs);
	
	public void setUniqName(String name);
	public void setDurable(boolean value);
	
}
