package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem.xml.mule.update;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.MSAInterfaceDirection;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.channel.QueueJmsChannel;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.channel.TopicJmsChannel;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.workflow.ConnectionEndpoint;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.workflow.IEsbWorkflow;

public class MuleUpdateEndpoints extends AbstractMuleUpdate {

	private Map<String, Element> requestReplyMap = new HashMap<String, Element>();

	public MuleUpdateEndpoints(Document doc) {
		super(doc);
	}

	public void updateEndpoints(Element flowElement, IEsbWorkflow workflow, ConnectionEndpoint connectionEndpoint, boolean startEndpoint, Map<String, List<String>> usedFlowArchStudioIds) {
		MSAInterfaceDirection direction = connectionEndpoint.getInterfaceConfig().getMuleInterfaceConfiguration().getDirection();

		Element endpoint = null;
		if (direction.equals(MSAInterfaceDirection.IN)) {
			endpoint = updateInboundEndpoint(flowElement, connectionEndpoint);
		} else if (direction.equals(MSAInterfaceDirection.OUT)) {
			endpoint = updateOutboundEndpoint(flowElement, connectionEndpoint);
		} else if (direction.equals(MSAInterfaceDirection.INOUT)) {
			endpoint = updateInOutboundEndpoint(flowElement, connectionEndpoint, startEndpoint);
		}

		usedFlowArchStudioIds.get(workflow.getComponentConfig().getArchStudioId()).add(connectionEndpoint.getInterfaceConfig().getArchStudioId());

		Element requestReplyElement = null;
		if (connectionEndpoint.getInterfaceConfig().getMuleInterfaceConfiguration().isRequestReplyEndpoint()) {
			requestReplyElement = updateRequestReply(connectionEndpoint, endpoint, flowElement);
			
			if (connectionEndpoint.getInterfaceConfig().getMuleInterfaceConfiguration().getReplyChannelName().isEmpty()) {
				Element addMessagePropertyElement = findAddMessageProperty(requestReplyElement);
	
				if (addMessagePropertyElement == null) {
					Element messageProperties = findMessageProperties(requestReplyElement);
	
					if (messageProperties == null) {
						generateHelper.generateMessagePropertiesElement(doc, requestReplyElement, connectionEndpoint);
					} else {
						generateHelper.generateAddMessagePropertyElement(doc, messageProperties, connectionEndpoint);
					}
				} else {
	
					generateHelper.updateAddMessagePropertyElement(doc, addMessagePropertyElement, connectionEndpoint);
				}
			}
		}

	}

	private Element updateRequestReply(ConnectionEndpoint connectionEndpoint, Element endpoint, Element flowElement) {
		Node parentNode = (Node) endpoint.getParentNode();
		while (parentNode != null) {
			if (parentNode.getNodeName().equals("request-reply")) {
				return (Element) parentNode;
			}
			parentNode = parentNode.getParentNode();
		}

		Element requestReplyElement;
		if (!requestReplyMap.containsKey(connectionEndpoint.getInterfaceConfig().getMuleInterfaceConfiguration().getRequestReplyEndpointId())) {
			requestReplyElement = generateHelper.generateRequestReplyElementForFlowElement(doc, flowElement);
			requestReplyMap.put(connectionEndpoint.getInterfaceConfig().getMuleInterfaceConfiguration().getRequestReplyEndpointId(), requestReplyElement);
		} else {
			requestReplyElement = requestReplyMap.get(connectionEndpoint.getInterfaceConfig().getMuleInterfaceConfiguration().getRequestReplyEndpointId());
		}

		return requestReplyElement;

	}

	private Element updateInOutboundEndpoint(Element flowElement, ConnectionEndpoint connectionEndpoint, boolean startEndpoint) {
		String archStudioId = connectionEndpoint.getInterfaceConfig().getArchStudioId();

		Element endpoint = findEndpoint(flowElement, archStudioId);

		if (endpoint != null) {
			if (connectionEndpoint.getJmsChannel() instanceof TopicJmsChannel) {
				endpoint = generateHelper.updateTopicInOutboundEndpointElement(endpoint, doc, flowElement, connectionEndpoint, startEndpoint);
			} else if (connectionEndpoint.getJmsChannel() instanceof QueueJmsChannel) {
				endpoint = generateHelper.updateQueueInOutboundEndpointElement(endpoint, doc, flowElement, connectionEndpoint, startEndpoint);
			}
		} else {
			if (connectionEndpoint.getJmsChannel() instanceof TopicJmsChannel) {
				endpoint = generateHelper.generateTopicInOutboundEndpointElement(doc, flowElement, connectionEndpoint, startEndpoint);
			} else if (connectionEndpoint.getJmsChannel() instanceof QueueJmsChannel) {
				endpoint = generateHelper.generateQueueInOutboundEndpointElement(doc, flowElement, connectionEndpoint, startEndpoint);
			}
		}

		return endpoint;
	}

	private Element updateOutboundEndpoint(Element flowElement, ConnectionEndpoint connectionEndpoint) {
		String archStudioId = connectionEndpoint.getInterfaceConfig().getArchStudioId();

		Element endpoint = findEndpoint(flowElement, archStudioId);

		if (endpoint != null) {
			if (connectionEndpoint.getJmsChannel() instanceof TopicJmsChannel) {
				endpoint = generateHelper.updateTopicOutboundEndpointElement(endpoint, doc, flowElement, connectionEndpoint);
			} else if (connectionEndpoint.getJmsChannel() instanceof QueueJmsChannel) {
				endpoint = generateHelper.updateQueueOutboundEndpointElement(endpoint, doc, flowElement, connectionEndpoint);
			}
		} else {
			if (connectionEndpoint.getJmsChannel() instanceof TopicJmsChannel) {
				endpoint = generateHelper.generateTopicOutboundEndpointElement(doc, flowElement, connectionEndpoint);
			} else if (connectionEndpoint.getJmsChannel() instanceof QueueJmsChannel) {
				endpoint = generateHelper.generateQueueOutboundEndpointElement(doc, flowElement, connectionEndpoint);
			}
		}

		return endpoint;
	}

	private Element updateInboundEndpoint(Element flowElement, ConnectionEndpoint connectionEndpoint) {
		String archStudioId = connectionEndpoint.getInterfaceConfig().getArchStudioId();

		Element endpoint = findEndpoint(flowElement, archStudioId);

		if (endpoint != null) {
			if (connectionEndpoint.getJmsChannel() instanceof TopicJmsChannel) {
				endpoint = generateHelper.updateTopicInboundEndpointElement(endpoint, doc, flowElement, connectionEndpoint);
			} else if (connectionEndpoint.getJmsChannel() instanceof QueueJmsChannel) {
				endpoint = generateHelper.updateQueueInboundEndpointElement(endpoint, doc, flowElement, connectionEndpoint);
			}
		} else {
			if (connectionEndpoint.getJmsChannel() instanceof TopicJmsChannel) {
				endpoint = generateHelper.generateTopicInboundEndpointElement(doc, flowElement, connectionEndpoint);
			} else if (connectionEndpoint.getJmsChannel() instanceof QueueJmsChannel) {
				endpoint = generateHelper.generateQueueInboundEndpointElement(doc, flowElement, connectionEndpoint);
			}
		}

		return endpoint;
	}

	private Element findAddMessageProperty(Element endpoint) {

		Node previousSibling = (Node) endpoint.getPreviousSibling();
		while (previousSibling != null) {

			if (previousSibling.getNodeName().equals("message-properties-transformer")) {
				NodeList messageProperties = ((Element) previousSibling).getElementsByTagName("add-message-property");
				for (int i = 0; i < messageProperties.getLength(); i++) {
					Element currentProperty = (Element) messageProperties.item(i);
					if (currentProperty.getAttribute("key").equals("MULE_REPLYTO")) {
						return currentProperty;
					}
				}
				break;
			}

			previousSibling = previousSibling.getPreviousSibling();
		}

		return null;
	}

	private Element findMessageProperties(Element endpoint) {
		Node previousSibling = (Node) endpoint.getPreviousSibling();
		while (previousSibling != null) {

			if (previousSibling.getNodeName().equals("message-properties-transformer")) {
				return (Element) previousSibling;
			}

			previousSibling = previousSibling.getPreviousSibling();
		}

		return null;
	}

	private Element findEndpoint(Element flowElement, String archStudioId) {
		NodeList outEndpoints = flowElement.getElementsByTagName("jms:outbound-endpoint");
		Element foundEndpoint = checkEndpoints(outEndpoints, archStudioId);
		if (foundEndpoint != null) {
			return foundEndpoint;
		}

		NodeList inEndpoints = flowElement.getElementsByTagName("jms:inbound-endpoint");
		foundEndpoint = checkEndpoints(inEndpoints, archStudioId);
		if (foundEndpoint != null) {
			return foundEndpoint;
		}

		return null;
	}

	private Element checkEndpoints(NodeList endpoints, String archStudioId) {
		for (int i = 0; i < endpoints.getLength(); i++) {
			Element currentEndpoint = (Element) endpoints.item(i);
			if (checkForArchIdComment(currentEndpoint, archStudioId)) {
				return currentEndpoint;
			}
		}
		return null;
	}

}
