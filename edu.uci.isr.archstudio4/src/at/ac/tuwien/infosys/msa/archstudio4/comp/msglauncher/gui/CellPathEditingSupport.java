package at.ac.tuwien.infosys.msa.archstudio4.comp.msglauncher.gui;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msglauncher.config.model.OutputFileType;

public class CellPathEditingSupport extends CellIdEditingSupport {

	private final ColumnViewer viewer;
	private DialogCellEditor cellEditor;

	public CellPathEditingSupport(ColumnViewer viewer) {
		super(viewer);
		this.viewer = viewer;

		cellEditor = new DialogCellEditor(((TableViewer) viewer).getTable()) {
			
			@Override
			protected Object openDialogBox(Control cellEditorWindow) {
				Shell shell = cellEditorWindow.getShell();
				FileDialog fileDialog = new FileDialog(shell,SWT.SAVE);
				String path = fileDialog.open();
				return path;
			}
		};
		
	}

	@Override
	protected CellEditor getCellEditor(Object element) {
		return cellEditor;
	}
	
	@Override
	protected Object getValue(Object element) {
		return ((OutputFileType) element).getFilepath();
	}

	@Override
	protected void setValue(Object element, Object userInputValue) {
		((OutputFileType) element).setFilepath(String.valueOf(userInputValue));
		viewer.update(element, null);
	}
}