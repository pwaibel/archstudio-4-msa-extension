package at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.checks;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;

public class ArchitectureWarning implements Runnable {

	private boolean cancle;
	private String message;
	
	public boolean showArchitectureWarning(String message) {
		this.message = message;

		Display.getDefault().syncExec(this);

		return shouldCancle();

	}
	
	@Override
	public void run() {
		MessageDialog dialog = new MessageDialog(Display.getDefault().getActiveShell(), "Architecture Warning", null, message, MessageDialog.WARNING, new String[] { "Continue", "Cancel"}, 0);
		int result = dialog.open();
		
		if(result == 1) {
			cancle = true;
		}
		
	}
	
	public boolean shouldCancle() {
		return cancle;
	}

}
