package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.workflow;

import java.util.List;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.component.XadlComponentConfiguration;

public abstract class AbstractEsbWorkflow implements IEsbWorkflow {

	private List<ConnectionEndpoint> orderedInterfaces;
	
	private XadlComponentConfiguration componentConfig;
	
	public AbstractEsbWorkflow(List<ConnectionEndpoint> orderedInterfaces, XadlComponentConfiguration componentConfig) {
		super();
		this.orderedInterfaces = orderedInterfaces;
		this.componentConfig = componentConfig;
	}

	@Override
	public List<ConnectionEndpoint> getOrderedInterfaces() {
		return orderedInterfaces;
	}

	@Override
	public String getFlowName() {
		return componentConfig.getComponentConfiguration().getFlowname();
	}

	@Override
	public XadlComponentConfiguration getComponentConfig() {
		return componentConfig;
	}
	
}
