package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.component;

import java.util.List;


public interface IComponentConfig {

	public List<AdditionalConfig> getAdditionalConfigs();

	public void setAdditionalConfigs(List<AdditionalConfig> additionalConfigs) ;
	public void addAdditionalConfigs(AdditionalConfig additionalConfig) ;

	public String getFlowname();
	
}
