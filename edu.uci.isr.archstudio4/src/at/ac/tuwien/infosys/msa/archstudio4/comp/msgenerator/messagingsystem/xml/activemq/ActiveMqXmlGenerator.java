package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem.xml.activemq;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem.IMessagingSystem;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem.xml.AbstractXmlGenerator;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.XadlConnectorConfiguration;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msglauncher.FilesInMemoryStore;
import edu.uci.isr.xarchflat.ObjRef;

public class ActiveMqXmlGenerator extends AbstractXmlGenerator {

	private Map<String, List<XadlConnectorConfiguration>> fileIdEsbWorkflowMap = new HashMap<String, List<XadlConnectorConfiguration>>();
	
	@Override
	public void generateXmlFile(IMessagingSystem esb) {
		for(Map.Entry<ObjRef, XadlConnectorConfiguration> connector: esb.getConnectorConnectorConfigMap().entrySet()) {
			String currentFileId = connector.getValue().getFileId();
			
			if(!fileIdEsbWorkflowMap.containsKey(currentFileId)) {
				fileIdEsbWorkflowMap.put(currentFileId, new ArrayList<XadlConnectorConfiguration>());
			}
			
			fileIdEsbWorkflowMap.get(currentFileId).add(connector.getValue());			
		}
		
		for(Entry<String, List<XadlConnectorConfiguration>> mapEntrySet : fileIdEsbWorkflowMap.entrySet()) {
			String fileId = mapEntrySet.getKey();
			List<XadlConnectorConfiguration> connectorConfigList = mapEntrySet.getValue();
			
			String filePath = FilesInMemoryStore.getInstance().getJmsFilePath(fileId);
			
			generateConfigFile(filePath, connectorConfigList);
		}
		
		
	}
	
	private void generateConfigFile(String filePath, List<XadlConnectorConfiguration> connectorConfigList) {
		File xmlFile = new File(filePath);
		
		if(xmlFile.exists()) {
			updateNewWorkflow(filePath, connectorConfigList);			
		}
		else {
			generateNewWorkflow(filePath, connectorConfigList);
		}
	}
	
	/**
	 * Update flows
	 */
	private void updateNewWorkflow(String filePath, List<XadlConnectorConfiguration> connectorConfigList) {
		try {
			// root elements
			Document doc = super.openXmlFile(filePath);

			ActiveMqUpdateXml activeMqUpdateXml = new ActiveMqUpdateXml(doc);
			
			Element rootElement = activeMqUpdateXml.findUpdateRootElement();
			
			Element brokerElement = activeMqUpdateXml.findUpdateBrokerElement(rootElement);
			
			activeMqUpdateXml.findUpdatePersistenceAdapterElements(brokerElement, connectorConfigList);
			activeMqUpdateXml.findUpdateTransportConnectorElements(brokerElement, connectorConfigList);
			
			ActiveMqDeleteUnusedElements deleteUnusedElements = new ActiveMqDeleteUnusedElements(doc);
			deleteUnusedElements.deleteUnusedElements(activeMqUpdateXml.getUsedPersistenceArchStudioIds(), activeMqUpdateXml.getUsedTransportArchStudioIds());
			
			super.writeXmlFile(doc,filePath);
			
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private void generateNewWorkflow(String filePath, List<XadlConnectorConfiguration> connectorConfigList) {
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			
			// root elements
			Document doc = docBuilder.newDocument();

			ActiveMqNewXml activeMqNewXml = new ActiveMqNewXml(doc);
			
			Element rootElement = activeMqNewXml.generateBeansElement();
			
			Element brokerElement = activeMqNewXml.generateBrokerElement(rootElement);
			
			activeMqNewXml.generatePersistenceAdapterElements(brokerElement, connectorConfigList);
			activeMqNewXml.generateTransportConnectorElements(brokerElement, connectorConfigList); 
			
			super.writeXmlFile(doc,filePath);
			
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}


}
