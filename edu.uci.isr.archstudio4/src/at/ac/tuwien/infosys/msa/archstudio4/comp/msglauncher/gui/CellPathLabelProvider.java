package at.ac.tuwien.infosys.msa.archstudio4.comp.msglauncher.gui;

import org.eclipse.jface.viewers.ColumnLabelProvider;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msglauncher.config.model.OutputFileType;

public class CellPathLabelProvider extends ColumnLabelProvider {

	@Override
	public String getText(Object element) {
		OutputFileType outputFile = (OutputFileType) element;

		return outputFile.getFilepath();

	}

}
