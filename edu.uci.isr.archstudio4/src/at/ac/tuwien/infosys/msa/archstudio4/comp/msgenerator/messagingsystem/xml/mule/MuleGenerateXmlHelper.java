package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem.xml.mule;

import java.util.List;

import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.component.AdditionalConfig;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.component.XadlComponentConfiguration;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.component.XadlInterfaceConfiguration;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.IJmsConnectorConfig;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.PersistenceAdditionalConfig;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.XadlConnectorConfiguration;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.workflow.ConnectionEndpoint;

public class MuleGenerateXmlHelper {

	public Element generateMuleElement(Document doc, String muleVersion) {
		Element rootElement = doc.createElement("mule");
		rootElement.setAttribute("xmlns:jms", "http://www.mulesoft.org/schema/mule/jms");
		rootElement.setAttribute("xmlns", "http://www.mulesoft.org/schema/mule/core");
		rootElement.setAttribute("xmlns:doc", "http://www.mulesoft.org/schema/mule/documentation");
		rootElement.setAttribute("xmlns:spring", "http://www.springframework.org/schema/beans");
		rootElement.setAttribute("version", muleVersion);
		rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		rootElement.setAttribute("xsi:schemaLocation", "http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-current.xsd http://www.mulesoft.org/schema/mule/core http://www.mulesoft.org/schema/mule/core/current/mule.xsd http://www.mulesoft.org/schema/mule/jms http://www.mulesoft.org/schema/mule/jms/current/mule-jms.xsd");
		doc.appendChild(rootElement);

		return rootElement;
	}

	/**
	 * <jms:activemq-connector name="connectorName"
	 * brokerURL="tcp://localhost:61616"/>
	 * 
	 * @param doc
	 * @param rootElement
	 */
	public void generateConnectorElement(Document doc, Element rootElement, String esbJmsSystem, ConnectionEndpoint connectionEndpoint) {
		
		IJmsConnectorConfig jmsConnector = connectionEndpoint.getConnectorConfig().getJmsConnector();
		String name = connectionEndpoint.getConnectorConfig().getJmsConnector().generateUniqName();
		String archStudioId = connectionEndpoint.getConnectorConfig().getArchStudioId();
		
		Element jmsConnectorElement = doc.createElement("jms:" + esbJmsSystem + "-connector");
		jmsConnectorElement.setAttribute("brokerURL", jmsConnector.getBrokerUrl());
		jmsConnectorElement.setAttribute("name", name);
		jmsConnectorElement.setAttribute("specification", jmsConnector.getSpecificationVersion());

		if(jmsConnector.isDurable()) {
			jmsConnectorElement.setAttribute("durable", "true");
			jmsConnectorElement.setAttribute("clientId", jmsConnector.getClientId());
		}
		
		for(PersistenceAdditionalConfig additionalConfig : jmsConnector.getPersistenceConfigs()) {
			if(additionalConfig instanceof PersistenceAdditionalConfig) {
				jmsConnectorElement.setAttribute("persistentDelivery", "true");
			}
		}
		
		generateDescription(jmsConnectorElement);
		generateArchStudioIdComment(doc, jmsConnectorElement, archStudioId);

		if (rootElement.hasChildNodes()) {
			Node firstChild = rootElement.getFirstChild();
			rootElement.insertBefore(jmsConnectorElement, firstChild);
		} else {
			rootElement.appendChild(jmsConnectorElement);
		}

	}

	/**
	 * <jms:activemq-connector name="connectorName"
	 * brokerURL="tcp://localhost:61616"/>
	 * 
	 * @param doc
	 * @param rootElement
	 */
	public void updateConnectorElement(Element jmsConnectorElement, XadlConnectorConfiguration connectorConfig) {
		jmsConnectorElement.removeAttribute("specification");
		jmsConnectorElement.setAttribute("brokerURL", connectorConfig.getJmsConnector().getBrokerUrl());
		jmsConnectorElement.setAttribute("specification", connectorConfig.getJmsConnector().getSpecificationVersion());
		String name = jmsConnectorElement.getAttribute("name");
		
		if(connectorConfig.getJmsConnector().isDurable()) {
			jmsConnectorElement.setAttribute("durable", "true");
			jmsConnectorElement.setAttribute("clientId", connectorConfig.getJmsConnector().getClientId());
		}
		else {
			if(jmsConnectorElement.hasAttribute("durable")) {
				jmsConnectorElement.removeAttribute("durable");
			}
			if(jmsConnectorElement.hasAttribute("clientId")) {
				jmsConnectorElement.removeAttribute("clientId");
			}
		}
		boolean isPersistence = false;
		for(PersistenceAdditionalConfig additionalConfig : connectorConfig.getJmsConnector().getPersistenceConfigs()) {
			if(additionalConfig instanceof PersistenceAdditionalConfig) {
				isPersistence = true;
				break;
			}
		}
		
		if(isPersistence) {
			jmsConnectorElement.setAttribute("persistentDelivery", "true");
		}
		else {
			if(jmsConnectorElement.hasAttribute("persistentDelivery")) {
				jmsConnectorElement.removeAttribute("persistentDelivery");
			}
		}
		
		
		if(name == null || name.isEmpty()) {
			jmsConnectorElement.setAttribute("name", connectorConfig.getJmsConnector().generateUniqName());
		}
		else {
			connectorConfig.getJmsConnector().setUniqName(name);
		}
		
	}

	/**
	 * <flow name="mule.flow.1" doc:name="mule.flow.1" >
	 * 
	 * @param doc
	 * @param rootElement
	 * @param interfaceConfig.archStudioId
	 */
	public Element generateStartFlow(Document doc, Element rootElement, String name, XadlComponentConfiguration componentConfiguration) {

		String nameWithoutSpace = name.replace(" ", "_");

		Element flowElement = doc.createElement("flow");
		rootElement.appendChild(flowElement);
		flowElement.setAttribute("name", nameWithoutSpace);
		flowElement.setAttribute("doc:name", nameWithoutSpace);
		generateDescription(flowElement);
		generateArchStudioIdComment(doc, flowElement, componentConfiguration.getArchStudioId());
		
		List<AdditionalConfig> additionalConfig = componentConfiguration.getComponentConfiguration().getAdditionalConfigs();
		for(AdditionalConfig addConfig : additionalConfig) {
			flowElement.setAttribute(addConfig.getName(), addConfig.getValue());
		}
		return flowElement;
	}

	/**
	 * <flow name="mule.flow.1" doc:name="mule.flow.1" >
	 * 
	 * @param doc
	 * @param rootElement
	 * @param interfaceConfig.archStudioId
	 */
	public Element updateStartFlow(Element flowElement, Document doc, Element rootElement, String name, XadlComponentConfiguration componentConfiguration) {

		String nameWithoutSpace = name.replace(" ", "_");

		flowElement.setAttribute("name", nameWithoutSpace);
		flowElement.setAttribute("doc:name", nameWithoutSpace);
		
		List<AdditionalConfig> additionalConfig = componentConfiguration.getComponentConfiguration().getAdditionalConfigs();
		for(AdditionalConfig addConfig : additionalConfig) {
			flowElement.setAttribute(addConfig.getName(), addConfig.getValue());
		}
		return flowElement;
	}

	/**
	 * <jms:inbound-endpoint topic="topic.name"
	 * connector-ref="connection_referenc_name" doc:name="JMS"
	 * doc:description="Generated by ArchStudio"/>
	 * 
	 * @param doc
	 * @param jmsConnectorElement
	 */
	public Element generateTopicInboundEndpointElement(Document doc, Element flowElement, ConnectionEndpoint connectionEndpoint) {
		Element jmsConnectorElement = doc.createElement("jms:inbound-endpoint");
		flowElement.appendChild(jmsConnectorElement);
		jmsConnectorElement.setAttribute("topic", connectionEndpoint.getJmsChannel().getName());
		jmsConnectorElement.setAttribute("connector-ref", connectionEndpoint.getConnectorConfig().getJmsConnector().getUniqName());
		jmsConnectorElement.setAttribute("doc:name", "JMS");
		
		XadlInterfaceConfiguration interfaceConfig = connectionEndpoint.getInterfaceConfig();
		if(connectionEndpoint.getConnectorConfig().getJmsConnector().isDurable() && (interfaceConfig.getMuleInterfaceConfiguration().getDurableName() != null && !interfaceConfig.getMuleInterfaceConfiguration().getDurableName().isEmpty())) {
			jmsConnectorElement.setAttribute("durableName", interfaceConfig.getMuleInterfaceConfiguration().getDurableName());
		}
		
		generateDescription(jmsConnectorElement);
		generateArchStudioIdComment(doc, jmsConnectorElement, interfaceConfig.getArchStudioId());
		return jmsConnectorElement;
	}

	/**
	 * <jms:inbound-endpoint topic="topic.name"
	 * connector-ref="connection_referenc_name" doc:name="JMS"
	 * doc:description="Generated by ArchStudio"/>
	 * 
	 * @param doc
	 * @param jmsConnectorElement
	 */
	public Element updateTopicInboundEndpointElement(Element jmsConnectorElement, Document doc, Element flowElement, ConnectionEndpoint connectionEndpoint) {
		jmsConnectorElement.removeAttribute("queue");
		jmsConnectorElement.setAttribute("topic", connectionEndpoint.getJmsChannel().getName());
		jmsConnectorElement.setAttribute("connector-ref", connectionEndpoint.getConnectorConfig().getJmsConnector().getUniqName());
		jmsConnectorElement.setAttribute("doc:name", "JMS");
		
		XadlInterfaceConfiguration interfaceConfig = connectionEndpoint.getInterfaceConfig();
		if(connectionEndpoint.getConnectorConfig().getJmsConnector().isDurable() && (interfaceConfig.getMuleInterfaceConfiguration().getDurableName() != null && !interfaceConfig.getMuleInterfaceConfiguration().getDurableName().isEmpty())) {
			jmsConnectorElement.setAttribute("durableName", interfaceConfig.getMuleInterfaceConfiguration().getDurableName());
		}
		else {
			if(jmsConnectorElement.hasAttribute("durableName")) {
				jmsConnectorElement.removeAttribute("durableNam");
			}
		}
		
		return jmsConnectorElement;
	}

	/**
	 * <jms:outbound-endpoint topic="topic.name"
	 * connector-ref="connection_referenc_name" doc:name="JMS"
	 * doc:description="Generated by ArchStudio"/>
	 * 
	 * @param doc
	 * @param jmsConnectorElement
	 */
	public Element generateTopicOutboundEndpointElement(Document doc, Element flowElement, ConnectionEndpoint connectionEndpoint) {
		Element jmsConnectorElement = doc.createElement("jms:outbound-endpoint");
		flowElement.appendChild(jmsConnectorElement);
		jmsConnectorElement.setAttribute("topic", connectionEndpoint.getJmsChannel().getName());
		jmsConnectorElement.setAttribute("connector-ref", connectionEndpoint.getConnectorConfig().getJmsConnector().getUniqName());
		jmsConnectorElement.setAttribute("doc:name", "JMS");
		
		XadlInterfaceConfiguration interfaceConfig = connectionEndpoint.getInterfaceConfig();
		if(connectionEndpoint.getConnectorConfig().getJmsConnector().isDurable() && (interfaceConfig.getMuleInterfaceConfiguration().getDurableName() != null && !interfaceConfig.getMuleInterfaceConfiguration().getDurableName().isEmpty())) {
			jmsConnectorElement.setAttribute("durableName", interfaceConfig.getMuleInterfaceConfiguration().getDurableName());
		}
		
		generateDescription(jmsConnectorElement);
		generateArchStudioIdComment(doc, jmsConnectorElement, interfaceConfig.getArchStudioId());
		return jmsConnectorElement;
	}

	/**
	 * <jms:outbound-endpoint topic="topic.name"
	 * connector-ref="connection_referenc_name" doc:name="JMS"
	 * doc:description="Generated by ArchStudio"/>
	 * 
	 * @param doc
	 * @param jmsConnectorElement
	 */
	public Element updateTopicOutboundEndpointElement(Element jmsConnectorElement, Document doc, Element flowElement, ConnectionEndpoint connectionEndpoint) {
		jmsConnectorElement.removeAttribute("queue");
		jmsConnectorElement.setAttribute("topic", connectionEndpoint.getJmsChannel().getName());
		jmsConnectorElement.setAttribute("connector-ref", connectionEndpoint.getConnectorConfig().getJmsConnector().getUniqName());
		jmsConnectorElement.setAttribute("doc:name", "JMS");
		
		XadlInterfaceConfiguration interfaceConfig = connectionEndpoint.getInterfaceConfig();
		if(connectionEndpoint.getConnectorConfig().getJmsConnector().isDurable() && (interfaceConfig.getMuleInterfaceConfiguration().getDurableName() != null && !interfaceConfig.getMuleInterfaceConfiguration().getDurableName().isEmpty())) {
			jmsConnectorElement.setAttribute("durableName", interfaceConfig.getMuleInterfaceConfiguration().getDurableName());
		}
		else {
			if(jmsConnectorElement.hasAttribute("durableName")) {
				jmsConnectorElement.removeAttribute("durableNam");
			}
		}
		
		return jmsConnectorElement;
	}

	/**
	 * <jms:inbound-endpoint queue="queue.name"
	 * connector-ref="connection_referenc_name" doc:name="JMS"
	 * doc:description="Generated by ArchStudio"/>
	 * 
	 * @param doc
	 * @param jmsConnectorElement
	 */
	public Element generateQueueInboundEndpointElement(Document doc, Element flowElement, ConnectionEndpoint connectionEndpoint) {
		Element jmsConnectorElement = doc.createElement("jms:inbound-endpoint");
		flowElement.appendChild(jmsConnectorElement);
		jmsConnectorElement.setAttribute("queue", connectionEndpoint.getJmsChannel().getName());
		jmsConnectorElement.setAttribute("connector-ref", connectionEndpoint.getConnectorConfig().getJmsConnector().getUniqName());
		jmsConnectorElement.setAttribute("doc:name", "JMS");
		
		XadlInterfaceConfiguration interfaceConfig = connectionEndpoint.getInterfaceConfig();
		if(connectionEndpoint.getConnectorConfig().getJmsConnector().isDurable() && (interfaceConfig.getMuleInterfaceConfiguration().getDurableName() != null && !interfaceConfig.getMuleInterfaceConfiguration().getDurableName().isEmpty())) {
			jmsConnectorElement.setAttribute("durableName", interfaceConfig.getMuleInterfaceConfiguration().getDurableName());
		}
		
		generateDescription(jmsConnectorElement);
		generateArchStudioIdComment(doc, jmsConnectorElement, interfaceConfig.getArchStudioId());
		return jmsConnectorElement;
	}

	/**
	 * <jms:inbound-endpoint queue="queue.name"
	 * connector-ref="connection_referenc_name" doc:name="JMS"
	 * doc:description="Generated by ArchStudio"/>
	 * 
	 * @param doc
	 * @param jmsConnectorElement
	 */
	public Element updateQueueInboundEndpointElement(Element jmsConnectorElement, Document doc, Element flowElement, ConnectionEndpoint connectionEndpoint) {
		jmsConnectorElement.removeAttribute("topic");
		jmsConnectorElement.setAttribute("queue", connectionEndpoint.getJmsChannel().getName());
		jmsConnectorElement.setAttribute("connector-ref", connectionEndpoint.getConnectorConfig().getJmsConnector().getUniqName());
		jmsConnectorElement.setAttribute("doc:name", "JMS");
		
		XadlInterfaceConfiguration interfaceConfig = connectionEndpoint.getInterfaceConfig();
		if(connectionEndpoint.getConnectorConfig().getJmsConnector().isDurable() && (interfaceConfig.getMuleInterfaceConfiguration().getDurableName() != null && !interfaceConfig.getMuleInterfaceConfiguration().getDurableName().isEmpty())) {
			jmsConnectorElement.setAttribute("durableName", interfaceConfig.getMuleInterfaceConfiguration().getDurableName());
		}
		else {
			if(jmsConnectorElement.hasAttribute("durableName")) {
				jmsConnectorElement.removeAttribute("durableNam");
			}
		}
		
		return jmsConnectorElement;
	}

	/**
	 * <jms:outbound-endpoint queue="queue.name"
	 * connector-ref="connection_referenc_name" doc:name="JMS"
	 * doc:description="Generated by ArchStudio"/>
	 * 
	 * @param doc
	 * @param jmsConnectorElement
	 */
	public Element generateQueueOutboundEndpointElement(Document doc, Element flowElement, ConnectionEndpoint connectionEndpoint) {
		Element jmsConnectorElement = doc.createElement("jms:outbound-endpoint");
		flowElement.appendChild(jmsConnectorElement);
		jmsConnectorElement.setAttribute("queue", connectionEndpoint.getJmsChannel().getName());
		jmsConnectorElement.setAttribute("connector-ref", connectionEndpoint.getConnectorConfig().getJmsConnector().getUniqName());
		jmsConnectorElement.setAttribute("doc:name", "JMS");
		
		XadlInterfaceConfiguration interfaceConfig = connectionEndpoint.getInterfaceConfig();
		if(connectionEndpoint.getConnectorConfig().getJmsConnector().isDurable() && (interfaceConfig.getMuleInterfaceConfiguration().getDurableName() != null && !interfaceConfig.getMuleInterfaceConfiguration().getDurableName().isEmpty())) {
			jmsConnectorElement.setAttribute("durableName", interfaceConfig.getMuleInterfaceConfiguration().getDurableName());
		}
		
		generateDescription(jmsConnectorElement);
		generateArchStudioIdComment(doc, jmsConnectorElement, interfaceConfig.getArchStudioId());
		return jmsConnectorElement;
	}

	/**
	 * <jms:outbound-endpoint queue="queue.name"
	 * connector-ref="connection_referenc_name" doc:name="JMS"
	 * doc:description="Generated by ArchStudio"/>
	 * 
	 * @param doc
	 * @param jmsConnectorElement
	 */
	public Element updateQueueOutboundEndpointElement(Element jmsConnectorElement, Document doc, Element flowElement, ConnectionEndpoint connectionEndpoint) {
		jmsConnectorElement.removeAttribute("topic");
		jmsConnectorElement.setAttribute("queue", connectionEndpoint.getJmsChannel().getName());
		jmsConnectorElement.setAttribute("connector-ref", connectionEndpoint.getConnectorConfig().getJmsConnector().getUniqName());
		jmsConnectorElement.setAttribute("doc:name", "JMS");
		
		XadlInterfaceConfiguration interfaceConfig = connectionEndpoint.getInterfaceConfig();
		if(connectionEndpoint.getConnectorConfig().getJmsConnector().isDurable() && (interfaceConfig.getMuleInterfaceConfiguration().getDurableName() != null && !interfaceConfig.getMuleInterfaceConfiguration().getDurableName().isEmpty())) {
			jmsConnectorElement.setAttribute("durableName", interfaceConfig.getMuleInterfaceConfiguration().getDurableName());
		}
		else {
			if(jmsConnectorElement.hasAttribute("durableName")) {
				jmsConnectorElement.removeAttribute("durableNam");
			}
		}
		
		return jmsConnectorElement;
	}

	/**
	 * <jms:inbound-endpoint queue="queue.name"
	 * connector-ref="connection_referenc_name"
	 * exchange-pattern="request-response"
	 * doc:description="Generated by ArchStudio"/>
	 * 
	 * @param doc
	 * @param flowElement
	 */
	public Element generateQueueInOutboundEndpointElement(Document doc, Element flowElement, ConnectionEndpoint connectionEndpoint, boolean startEndpoint) {
		Element jmsConnectorElement;
		if (startEndpoint) {
			jmsConnectorElement = generateQueueInboundEndpointElement(doc, flowElement, connectionEndpoint);
		} else {
			jmsConnectorElement = generateQueueOutboundEndpointElement(doc, flowElement, connectionEndpoint);
		}

		jmsConnectorElement.setAttribute("exchange-pattern", "request-response");
		return jmsConnectorElement;
	}

	/**
	 * <jms:inbound-endpoint queue="queue.name"
	 * connector-ref="connection_referenc_name"
	 * exchange-pattern="request-response"
	 * doc:description="Generated by ArchStudio"/>
	 * 
	 * @param doc
	 * @param flowElement
	 */
	public Element updateQueueInOutboundEndpointElement(Element jmsConnectorElement, Document doc, Element flowElement, ConnectionEndpoint connectionEndpoint, boolean startEndpoint) {
		if (startEndpoint) {
			jmsConnectorElement = updateQueueInboundEndpointElement(jmsConnectorElement, doc, flowElement, connectionEndpoint);
		} else {
			jmsConnectorElement = updateQueueOutboundEndpointElement(jmsConnectorElement, doc, flowElement, connectionEndpoint);
		}

		jmsConnectorElement.setAttribute("exchange-pattern", "request-response");
		return jmsConnectorElement;
	}

	/**
	 * <jms:inbound-endpoint topic="topic.name"
	 * connector-ref="connection_referenc_name"
	 * exchange-pattern="request-response"
	 * doc:description="Generated by ArchStudio"/>
	 * 
	 * @param doc
	 * @param flowElement
	 * @param startEndpoint
	 */
	public Element generateTopicInOutboundEndpointElement(Document doc, Element flowElement, ConnectionEndpoint connectionEndpoint, boolean startEndpoint) {
		Element jmsConnectorElement;
		if (startEndpoint) {
			jmsConnectorElement = generateTopicInboundEndpointElement(doc, flowElement, connectionEndpoint);
		} else {
			jmsConnectorElement = generateTopicOutboundEndpointElement(doc, flowElement, connectionEndpoint);
		}

		jmsConnectorElement.setAttribute("exchange-pattern", "request-response");
		return jmsConnectorElement;
	}

	/**
	 * <jms:inbound-endpoint topic="topic.name"
	 * connector-ref="connection_referenc_name"
	 * exchange-pattern="request-response"
	 * doc:description="Generated by ArchStudio"/>
	 * 
	 * @param doc
	 * @param flowElement
	 * @param startEndpoint
	 */
	public Element updateTopicInOutboundEndpointElement(Element jmsConnectorElement, Document doc, Element flowElement, ConnectionEndpoint connectionEndpoint, boolean startEndpoint) {
		if (startEndpoint) {
			jmsConnectorElement = updateTopicInboundEndpointElement(jmsConnectorElement, doc, flowElement, connectionEndpoint);
		} else {
			jmsConnectorElement = updateTopicOutboundEndpointElement(jmsConnectorElement, doc, flowElement, connectionEndpoint);
		}

		jmsConnectorElement.setAttribute("exchange-pattern", "request-response");
		return jmsConnectorElement;
	}
	
	/**
	 * <message-properties-transformer overwrite="true" scope="session" doc:name="Message Properties">
	 * 	  <add-message-property key="MULE_REPLYTO" value="jms://queue"/>
	 * </message-properties-transformer>
	 * @param element
	 * @return
	 */
	public Element generateMessagePropertiesElement(Document doc, Element requestReplyElement, ConnectionEndpoint connectionEndpoint) {
		Element messageProperties = doc.createElement("message-properties-transformer");
		messageProperties.setAttribute("doc:name", "Message Properties");
		generateDescription(messageProperties);
		
		generateAddMessagePropertyElement(doc, messageProperties, connectionEndpoint);	
		
		requestReplyElement.getParentNode().insertBefore(messageProperties, requestReplyElement);
		
		return requestReplyElement;
	}
	
	/**
	 * 	  <add-message-property key="MULE_REPLYTO" value="jms://queue"/>
	 * @param element
	 * @return
	 */
	public Element generateAddMessagePropertyElement(Document doc, Element messageProperties, ConnectionEndpoint connectionEndpoint) {
		Element property = doc.createElement("add-message-property");
		property.setAttribute("key", "MULE_REPLYTO");
		property.setAttribute("value", "jms://"+connectionEndpoint.getInterfaceConfig().getMuleInterfaceConfiguration().getReplyChannelName());
		
		messageProperties.appendChild(property);
		
		return messageProperties;
	}
	
	/**
	 * 	  <add-message-property key="MULE_REPLYTO" value="jms://queue"/>
	 * @param element
	 * @return
	 */
	public Element updateAddMessagePropertyElement(Document doc, Element addMessageProperty, ConnectionEndpoint connectionEndpoint) {
		addMessageProperty.setAttribute("key", "MULE_REPLYTO");
		addMessageProperty.setAttribute("value", "jms://"+connectionEndpoint.getInterfaceConfig().getMuleInterfaceConfiguration().getReplyChannelName());
		
		return addMessageProperty;
	}
  

	protected Element generateDescription(Element element) {
		element.setAttribute("doc:description", "Generated by ArchStudio");
		return element;
	}

	protected Element generateDescription(Element element, String archStudioId) {
		element.setAttribute("doc:description", "ArchStudioId=" + archStudioId + ";Generated by ArchStudio");
		return element;
	}


	protected Element generateArchStudioIdComment(Document doc, Element element, String archStudioId) {
		Comment comment = doc.createComment(" Do not delete! Used by ArchStudio. ArchStudioId=" + archStudioId + " ");
		element.appendChild(comment);
		return element;
	}
	
	/**
	 * <!-- Insert flow components here -->
	 * 
	 * @param doc
	 * @param parentElement
	 */
	protected Comment generateComment(Document doc, Element parentElement) {

		String commentText = " Insert flow components here ";

		Comment comment = doc.createComment(commentText);		
		NodeList childNodes = parentElement.getChildNodes();
		
		for(int i = 0; i < childNodes.getLength(); i++) {
			Node tempElement = (Node) childNodes.item(i);
			
			if(tempElement.isEqualNode(comment)) {
				return null;
			}
		}
		parentElement.appendChild(comment);
		return comment;

	}

	/**
	 * <request-reply>
	 * </request-reply>
	 * @param doc
	 * @param flowElement
	 * @return
	 */
	public Element generateRequestReplyElementForFlowElement(Document doc, Element flowElement) {
		Element requestReplyElement = doc.createElement("request-reply");
		flowElement.appendChild(requestReplyElement);
		return requestReplyElement;
	}
	
	/**
	 * <request-reply>
	 * </request-reply>
	 * @param doc
	 * @param flowElement
	 * @return
	 */
	public Element generateRequestReplyElementAsParent(Document doc, Element endpoint, Element flowElement) {
		Element requestReplyElement = doc.createElement("request-reply");
		endpoint.insertBefore(requestReplyElement, endpoint);
		requestReplyElement.appendChild(endpoint);
		return requestReplyElement;
	}

}
