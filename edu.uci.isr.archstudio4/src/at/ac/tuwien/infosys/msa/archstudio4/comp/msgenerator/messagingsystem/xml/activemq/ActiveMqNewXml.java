package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem.xml.activemq;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.PersistenceAdditionalConfig;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.XadlConnectorConfiguration;

public class ActiveMqNewXml {

	private Document doc;
	
	public ActiveMqNewXml(Document doc) {
		this.doc = doc;
	}

	/**
	 * <persistenceAdapter>
     *    <kahaDB directory="${activemq.data}/kahadb"/>
     * </persistenceAdapter>
	 * 
	 * @param doc
	 * @param brokerElement
	 * @param connectorConfigList
	 * @return
	 */
	protected Element generatePersistenceAdapterElements(Element brokerElement, List<XadlConnectorConfiguration> connectorConfigList) {
		
		for(XadlConnectorConfiguration connectorConfig : connectorConfigList) {
			List<PersistenceAdditionalConfig> additionalConfigs = connectorConfig.getJmsConnector().getPersistenceConfigs();
			
			for(PersistenceAdditionalConfig additionalConfig : additionalConfigs) {
				if(additionalConfig instanceof PersistenceAdditionalConfig) {
				
					Element transportConnectors = doc.createElement("persistenceAdapter");
					brokerElement.appendChild(transportConnectors);
				
					PersistenceAdditionalConfig addConfig = (PersistenceAdditionalConfig) additionalConfig;
					Element transportConnector = doc.createElement(addConfig.getAdapterName());
					transportConnector.setAttribute("directory", addConfig.getDirectory());
					generateArchStudioIdComment(transportConnector, connectorConfig.getArchStudioId());
					transportConnectors.appendChild(transportConnector);
					
					return transportConnectors; 
				}
			}
		}

		return brokerElement;
		
	}
	
	/**
	 * <transportConnectors>
     *    <transportConnector uri="tcp://localhost:61616"/>
     * </transportConnectors>
     * 
	 * @param doc
	 * @param brokerElement
	 * @param connectorConfigList
	 * @return
	 */
	protected Element generateTransportConnectorElements(Element brokerElement, List<XadlConnectorConfiguration> connectorConfigList) {
		Element transportConnectors = doc.createElement("transportConnectors");
		brokerElement.appendChild(transportConnectors);
		
		List<XadlConnectorConfiguration> alreadyWritten = new ArrayList<XadlConnectorConfiguration>();
		for(XadlConnectorConfiguration connectorConfig : connectorConfigList) {
			if(!alreadyWritten.contains(connectorConfig)) {
				Element transportConnector = doc.createElement("transportConnector");
				transportConnector.setAttribute("uri", connectorConfig.getJmsConnector().getBrokerUrl());
				generateArchStudioIdComment(transportConnector, connectorConfig.getArchStudioId());
				transportConnectors.appendChild(transportConnector);	
				alreadyWritten.add(connectorConfig);
			}
		}

		return transportConnectors;
	}

	/**
	 * <broker xmlns="http://activemq.apache.org/schema/core">
	 * @param doc
	 * @param rootElement
	 * @return
	 */
	protected Element generateBrokerElement(Element rootElement) {
		Element brokerElement = doc.createElement("broker");
		rootElement.appendChild(brokerElement);
		brokerElement.setAttribute("xmlns","http://activemq.apache.org/schema/core");
		return brokerElement;
	}

	/**
	 * <beans
  	 * xmlns="http://www.springframework.org/schema/beans"
  	 * xmlns:amq="http://activemq.apache.org/schema/core"
  	 * xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  	 * xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.0.xsd
  	 *       http://activemq.apache.org/schema/core http://activemq.apache.org/schema/core/activemq-core.xsd
  	 *       http://camel.apache.org/schema/spring http://camel.apache.org/schema/spring/camel-spring.xsd">
	 * @param doc
	 * @return
	 */
	protected Element generateBeansElement() {
		Element rootElement = doc.createElement("beans");
		rootElement.setAttribute("xmlns","http://www.springframework.org/schema/beans");
		rootElement.setAttribute("xmlns:amq","http://activemq.apache.org/schema/core");
		rootElement.setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
		rootElement.setAttribute("xsi:schemaLocation","http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.0.xsd http://activemq.apache.org/schema/core http://activemq.apache.org/schema/core/activemq-core.xsd http://camel.apache.org/schema/spring http://camel.apache.org/schema/spring/camel-spring.xsd");
		doc.appendChild(rootElement);
		return rootElement;
	}

	protected Element generateArchStudioIdComment(Element element, String archStudioId) {
		Comment comment = doc.createComment(" Do not delete! Used by ArchStudio. ArchStudioId=" + archStudioId + " ");
		element.appendChild(comment); 
		return element;
	}
	
}
