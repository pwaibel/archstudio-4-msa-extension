package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem.xml.IXmlGenerator;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem.xml.activemq.ActiveMqXmlGenerator;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem.xml.mule.MuleXmlGenerator;

public class MuleActiveMqSystem extends AbstractMessagingSystem {

	private final String ESB_JMS_SYSTEM = "activemq";
	private final String MULE_VERSION = "EE-3.4.0";
	
	@Override
	public String getEsbJmsSystem() {
		return ESB_JMS_SYSTEM;
	}
	
	@Override
	public String getEsbVersion() {
		return MULE_VERSION;
	}
	
	public boolean generateXmlFile() {
		
		IXmlGenerator muleXmlGenerator = new MuleXmlGenerator();		
		muleXmlGenerator.generateXmlFile(this);
		
		IXmlGenerator jmsXmlGenerator = new ActiveMqXmlGenerator();		
		jmsXmlGenerator.generateXmlFile(this);
		
		return true;
	}
	
}
