package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.workflow;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.component.XadlInterfaceConfiguration;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.XadlConnectorConfiguration;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.channel.IJmsChannel;

/**
 * This class holds all the informations for an endpoint
 * 
 * @author philippwaibel
 *
 */
public class ConnectionEndpoint {

	private IJmsChannel jmsChannel;
	private XadlConnectorConfiguration connectorConfig;
	private XadlInterfaceConfiguration interfaceConfig;
	
	public ConnectionEndpoint() {
		super();		
	}
	
	public XadlConnectorConfiguration getConnectorConfig() {
		return connectorConfig;
	}
	public void setConnectorConfig(XadlConnectorConfiguration connectorConfig) {
		this.connectorConfig = connectorConfig;
	}
	
	public IJmsChannel getJmsChannel() {
		return jmsChannel;
	}
	public void setJmsChannel(IJmsChannel jmsChannel) {
		this.jmsChannel = jmsChannel;
	}

	public XadlInterfaceConfiguration getInterfaceConfig() {
		return interfaceConfig;
	}
	public void setInterfaceConfig(XadlInterfaceConfiguration interfaceConfig) {
		this.interfaceConfig = interfaceConfig;
	}
}
