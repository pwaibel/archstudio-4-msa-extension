package at.ac.tuwien.infosys.msa.archstudio4.comp.msglauncher;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msglauncher.config.model.OutputFileType;

public class FilesInMemoryStore {

	private static final FilesInMemoryStore instance = new FilesInMemoryStore();

	private List<OutputFileType> muleFileList = new ArrayList<OutputFileType>();
	private List<OutputFileType> jmsFileList = new ArrayList<OutputFileType>();
	
	private FilesInMemoryStore() {

	}
	
	public void resetFileLists() {
		muleFileList = new ArrayList<OutputFileType>();
		jmsFileList = new ArrayList<OutputFileType>();
	}
	
	public String getJmsFilePath(String fileId) {
		return getFilePath(fileId, jmsFileList);
	}
	
	public String getMuleFilePath(String fileId) {
		return getFilePath(fileId, muleFileList);
	}
	
	private String getFilePath(String fileId, List<OutputFileType> fileList) {
		for(OutputFileType outputFile : fileList) {
			if(outputFile.getId().equals(fileId)) {
				return outputFile.getFilepath();
			}
		}
		return null;
	}

	public static FilesInMemoryStore getInstance() {
		return instance;
	}

	public List<OutputFileType> getMuleFileList() {
		return muleFileList;
	}

	public List<OutputFileType> getJmsFileList() {
		return jmsFileList;
	}

	public OutputFileType newMuleFile() {

		String id_prefix = "Mule_";
		OutputFileType outputFile = newFile(id_prefix, muleFileList);
		return outputFile;
	}

	public OutputFileType newJmsFile() {
		String id_prefix = "Jms_";
		OutputFileType outputFile = newFile(id_prefix, jmsFileList);
		return outputFile;
	}

	private OutputFileType newFile(String id_prefix, List<OutputFileType> fileList) {
		for (int i = 0; i < 100; i++) {
			String tmpId = id_prefix + String.valueOf(i);
			if (isIdUniq(tmpId, fileList)) {
				OutputFileType outputFile = new OutputFileType(tmpId, "");
				fileList.add(new OutputFileType(tmpId, ""));
				return outputFile;
			}
		}

		String tmpId = id_prefix + UUID.randomUUID();
		OutputFileType outputFile = new OutputFileType(tmpId, "");
		fileList.add(outputFile);

		return outputFile;
	}

	public boolean isIdUniq(String id, List<OutputFileType> fileList) {
		for(OutputFileType file : fileList) {
			if(id.equals(file.getId())) {
				return false;
			}
		}
		return true;
	}

}
