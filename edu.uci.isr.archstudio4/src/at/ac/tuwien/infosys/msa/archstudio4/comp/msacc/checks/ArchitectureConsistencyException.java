package at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.checks;

public class ArchitectureConsistencyException extends Exception {
	
	private static final long serialVersionUID = -4886643480247810165L;

	private boolean showException = true;
	
	public ArchitectureConsistencyException(){
		super();
	}

	public ArchitectureConsistencyException(String message, boolean showException){
		super(message);
		this.showException = showException;
	}
	
	public ArchitectureConsistencyException(String message){
		super(message);
	}

	public ArchitectureConsistencyException(Throwable cause){
		super(cause);
	}

	public ArchitectureConsistencyException(String message, Throwable cause){
		super(message, cause);
	}

	public boolean isShowException() {
		return showException;
	}

	
}
