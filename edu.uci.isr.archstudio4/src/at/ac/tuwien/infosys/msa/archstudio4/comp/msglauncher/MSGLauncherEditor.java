package at.ac.tuwien.infosys.msa.archstudio4.comp.msglauncher;

import java.net.URI;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.IMSACC;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.checks.ArchitectureConsistencyException;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msglauncher.gui.MSGLauncherEditorComposite;
import edu.uci.isr.archstudio4.comp.resources.IResources;
import edu.uci.isr.archstudio4.editors.AbstractArchstudioEditor;
import edu.uci.isr.archstudio4.editors.AbstractArchstudioOutlinePage;
import edu.uci.isr.widgets.swt.SWTWidgetUtils;
import edu.uci.isr.xadlutils.XadlUtils;
import edu.uci.isr.xarchflat.ObjRef;

public class MSGLauncherEditor extends AbstractArchstudioEditor {

	private IMSACC msacc = null;

	public MSGLauncherEditor() {
		super(MSGLauncherMyxComponent.class, MSGLauncherMyxComponent.EDITOR_NAME);
	}

	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		super.init(site, input);

		setBannerInfo(((MSGLauncherMyxComponent) comp).getIcon(), "Messaging System Generator");
		setHasBanner(true);

		msacc = ((MSGLauncherMyxComponent) comp).getMSACC();

		FilesInMemoryStore.getInstance().resetFileLists();
		
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	protected AbstractArchstudioOutlinePage createOutlinePage() {
		return new MSGLauncherOutlinePage(xarch, xArchRef, resources);
	}

	public void createEditorContents(final Composite parent) {

		parent.setLayout(new GridLayout(1, false));
		parent.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		ObjRef[] selectedRefs = null;
		if (outlinePage != null) {
			selectedRefs = ((MSGLauncherOutlinePage) outlinePage).getSelectedRefs();
		}

		if ((selectedRefs == null) || (selectedRefs.length == 0)) {
			Label lNothingSelected = new Label(parent, SWT.LEFT);
			lNothingSelected.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
			lNothingSelected.setFont(resources.getPlatformFont(IResources.PLATFORM_DEFAULT_FONT_ID));
			lNothingSelected.setText("Select one or more elements in the outline view.");
		} else {
			ObjRef selectedRef = selectedRefs[0];
			if ((selectedRefs.length == 1) && (xarch.getParent(selectedRef) == null)) {
				// It's an xArch element
				Label lNothingSelected = new Label(parent, SWT.LEFT);
				lNothingSelected.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
				lNothingSelected.setFont(resources.getPlatformFont(IResources.PLATFORM_DEFAULT_FONT_ID));
				lNothingSelected.setText("Select one or more structures in the outline view.");
			} else if (selectedRefs.length == 1){
				final ObjRef structureRef = selectedRef;
				
				Label lElement = new Label(parent, SWT.LEFT);
				lElement.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_WHITE));
				lElement.setFont(resources.getPlatformFont(IResources.PLATFORM_HEADER_FONT_ID));
				lElement.setText(XadlUtils.getDescription(xarch, structureRef));

				ScrolledComposite scrollBox = new ScrolledComposite(parent, SWT.V_SCROLL | SWT.V_SCROLL);
				GridData layoutData = new GridData(GridData.FILL_BOTH);
				scrollBox.setLayoutData(layoutData);
				scrollBox.setBackground(parent.getBackground());

				Composite editorComp = new MSGLauncherEditorComposite(scrollBox, SWT.NONE);

				final Button bInstantiate = new Button(editorComp, SWT.NONE);
				bInstantiate.setImage(resources.getImage(MSGLauncherMyxComponent.IMAGE_MSAGENERATOR_GO_ICON));
				bInstantiate.setBounds(10, 386, 110, 28);
				bInstantiate.setText("Generate");
				bInstantiate.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						bInstantiate.setEnabled(false);
						bInstantiate.setText("Generating...");
						parent.layout(true, true);
						Thread t = new Thread(new Runnable() {
							public void run() {
								try {
									launchArchitecture(structureRef);
								} finally {
									SWTWidgetUtils.async(parent, new Runnable() {
										public void run() {
											bInstantiate.setEnabled(true);
											bInstantiate.setText("Generate");
											parent.layout(true, true);
										}
									});
								}
							}
						});
						t.start();
					}
				});

				editorComp.pack();
				editorComp.layout();

				scrollBox.setContent(editorComp);

			}
		}
	}


	protected void launchArchitecture(final ObjRef structureRef) {

		try {

			URI xArchURI = URI.create(xarch.getXArchURI(xarch.getXArch(structureRef)));
			IProject xArchProject = ResourcesPlugin.getWorkspace().getRoot().findMember(xArchURI.getPath()).getProject();

			msacc.architectureConsistencyCheck(xArchProject.getName(), xarch.getXArch(structureRef), structureRef);

		} catch (ArchitectureConsistencyException ex) {
			ex.printStackTrace();
		}
	}
	
}
