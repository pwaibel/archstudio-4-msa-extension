package at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.checks;

import java.util.Set;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.MSAInterfaceDirection;

import edu.uci.isr.xadlutils.XadlUtils;
import edu.uci.isr.xarchflat.ObjRef;
import edu.uci.isr.xarchflat.XArchFlatInterface;

/**
 * Link Consistency Checks:
 * Errors:
 * <ul>
 * 	<li>Check if the link has exactly two points</li>
 *  <li>Check if the link has a valid endpoint</li>
 *  <li>Check if the interface has a direction</li>
 *  <li>Check if the interface has a valid direction</li>
 *  <li>Check if the link is connected to a brick in the structure</li>
 *  <li>Check if each signature has an interface</li>
 *  <li>Check if the interfaces, that are connected by a link, has different directions</li>
 * </ul>
 * 
 * Warnings: 
 * @author philippwaibel
 *
 */
public class LinkConsistencyCheck extends AbstractConsistencyCheck {

	public LinkConsistencyCheck(XArchFlatInterface xarch) {
		super(xarch);
	}

	@Override
	public boolean check(Set<ObjRef> componentRefs, Set<ObjRef> connectorRefs, ObjRef structureRef) throws ArchitectureConsistencyException {

		for (ObjRef linkRef : xarch.getAll(structureRef, "link")) {
			ObjRef[] pointRefs = xarch.getAll(linkRef, "point");
			if (pointRefs.length != 2) {
				throw new ArchitectureConsistencyException("The link \"" + XadlUtils.getDescription(xarch, linkRef) + "\" must have exactly two points!");
			}

			MSAInterfaceDirection[] parentDirections = new MSAInterfaceDirection[2];

			for (int i = 0; i < pointRefs.length; i++) {
				ObjRef interfaceRef = XadlUtils.resolveXLink(xarch, pointRefs[i], "anchorOnInterface");
				if (interfaceRef == null) {
					throw new ArchitectureConsistencyException("The link \"" + XadlUtils.getDescription(xarch, linkRef) + "\" has an invalid endpoint!");
				}
				
				ObjRef directionRef = (ObjRef) xarch.get(interfaceRef, "direction");
				if (directionRef == null) {
					throw new ArchitectureConsistencyException("The interface \"" + XadlUtils.getDescription(xarch, interfaceRef) + "\" has no direction!");
				}
				String direction = (String) xarch.get(directionRef, "value");
				if (direction.equals("in")) {
					parentDirections[i] = MSAInterfaceDirection.IN;
				} else if (direction.equals("out")) {
					parentDirections[i] = MSAInterfaceDirection.OUT;
				} else if (direction.equals("inout")) {
					parentDirections[i] = MSAInterfaceDirection.INOUT;
				} else {
					throw new ArchitectureConsistencyException("The interface \"" + XadlUtils.getDescription(xarch, interfaceRef) + "\" has an invalid (not in/out/inout) direction!");
				}

				ObjRef parentBrickRef = xarch.getParent(interfaceRef);
				if (!componentRefs.contains(parentBrickRef) && !connectorRefs.contains(parentBrickRef)) {
					throw new ArchitectureConsistencyException("The link \"" + XadlUtils.getDescription(xarch, linkRef) + "\" points to a brick that is not in the structure \"" + XadlUtils.getDescription(xarch, structureRef) + "\"!");
				}

				ObjRef signatureType = XadlUtils.resolveXLink(xarch, interfaceRef, "signature");
				if (signatureType == null) {
					throw new ArchitectureConsistencyException("The interface \"" + XadlUtils.getDescription(xarch, pointRefs[i]) + "\" has no signature.");
				}
			}

			if (((!parentDirections[0].equals(MSAInterfaceDirection.INOUT) && !parentDirections[1].equals(MSAInterfaceDirection.INOUT)) &&  parentDirections[0].equals(parentDirections[1])) ||
				(( parentDirections[0].equals(MSAInterfaceDirection.INOUT) ||  parentDirections[1].equals(MSAInterfaceDirection.INOUT)) && !parentDirections[0].equals(parentDirections[1]))) 
			{
				throw new ArchitectureConsistencyException("The interface directions of the link \"" + XadlUtils.getDescription(xarch, linkRef) + "\" has to be different or INOUT -> INOUT");
			}
		
		}
		return true;
	}

}
