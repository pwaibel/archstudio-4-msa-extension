package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.workflow;

import java.util.List;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.component.XadlComponentConfiguration;

public interface IEsbWorkflow {
	
	public List<ConnectionEndpoint> getOrderedInterfaces();
	
	public String getFlowName();
	public XadlComponentConfiguration getComponentConfig();

}