package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem.xml;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem.IMessagingSystem;

public interface IXmlGenerator {

	public abstract void generateXmlFile(IMessagingSystem esb);

}