package at.ac.tuwien.infosys.msa.archstudio4.comp.msacc;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import at.ac.tuwien.infosys.msa.archstudio4.comp.MessaginSystemGenerationException;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.checks.ArchitectureConsistencyCheck;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.checks.ArchitectureConsistencyException;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.checks.ArchitectureExceptionDialog;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.checks.ComponentConsistencyCheck;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.checks.ConnectorConsistencyCheck;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.checks.ConnectorTypeConsistencyCheck;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.checks.FileIdConsistencyCheck;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.checks.IConsistencyCeck;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.checks.LinkConsistencyCheck;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.IMSGenerator;
import edu.uci.isr.xarchflat.ObjRef;
import edu.uci.isr.xarchflat.XArchFlatInterface;

public class MSACCImpl implements IMSACC {

	private final static Logger LOGGER = Logger.getLogger(MSACCImpl.class.getName());

	private XArchFlatInterface xarch;
	private IMSGenerator msgenerator;

	public MSACCImpl(XArchFlatInterface xarch, IMSGenerator msgenerator) {
		this.xarch = xarch;
		this.msgenerator = msgenerator;

	}

	@Override
	public void architectureConsistencyCheck(String name, ObjRef xArchRef, ObjRef structureRef) throws ArchitectureConsistencyException {

		Set<ObjRef> componentRefs = new HashSet<ObjRef>();
		Set<ObjRef> connectorRefs = new HashSet<ObjRef>();

		componentRefs.addAll(Arrays.asList(xarch.getAll(structureRef, "component")));
		connectorRefs.addAll(Arrays.asList(xarch.getAll(structureRef, "connector")));

		IConsistencyCeck fileIdCheck = new FileIdConsistencyCheck(xarch);
		IConsistencyCeck connectorTypeCheck = new ConnectorTypeConsistencyCheck(xarch);
		IConsistencyCeck componentCheck = new ComponentConsistencyCheck(xarch);
		IConsistencyCeck linkCheck = new LinkConsistencyCheck(xarch);
		IConsistencyCeck archCheck = new ArchitectureConsistencyCheck(xarch);
		IConsistencyCeck connectorCheck = new ConnectorConsistencyCheck(xarch);

		try {
			fileIdCheck.check(componentRefs, connectorRefs, structureRef);
			linkCheck.check(componentRefs, connectorRefs, structureRef);
			componentCheck.check(componentRefs, connectorRefs, structureRef);
			connectorTypeCheck.check(componentRefs, connectorRefs, structureRef);
			connectorCheck.check(componentRefs, connectorRefs, structureRef);
			archCheck.check(componentRefs, connectorRefs, structureRef);
		} catch (ArchitectureConsistencyException e) {
			String message = e.getMessage();
			ArchitectureExceptionDialog architectureException = new ArchitectureExceptionDialog();
			if(e.isShowException()) {
				architectureException.showArchitectureException(message);
			}
			throw new ArchitectureConsistencyException(message);
		}

		try {
			msgenerator.generateMessagingSystem("", xArchRef, structureRef);
		} catch (MessaginSystemGenerationException e) {
			e.printStackTrace();
		}

		LOGGER.info("Finished creating/updating");
	}

}
