package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector;

import java.util.ArrayList;
import java.util.List;

import edu.uci.isr.xarchflat.ObjRef;

/**
 * This class represent the common connector informations from the XADL file
 * @author philippwaibel
 *
 */
public class XadlConnectorConfiguration {
	
	private IJmsConnectorConfig jmsConnector;
	private List<ObjRef> connectorRefList;
	private String archStudioId;
	private String fileId = "";
	
	public XadlConnectorConfiguration() {
		super();
	}
	public XadlConnectorConfiguration(IJmsConnectorConfig jmsConnector, List<ObjRef> connectorRefList, String archStudioId, String fileId) {
		super();
		this.jmsConnector = jmsConnector;
		this.connectorRefList = connectorRefList;
		this.archStudioId = archStudioId;
		this.fileId = fileId;
	}
	
	
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	public IJmsConnectorConfig getJmsConnector() {
		return jmsConnector;
	}
	public void setJmsConnector(IJmsConnectorConfig jmsConnector) {
		this.jmsConnector = jmsConnector;
	}
	public List<ObjRef> getConnectorRefList() {
		return connectorRefList;
	}
	public void setConnectorRefList(List<ObjRef> connectorRef) {
		this.connectorRefList = connectorRef;
	}
	public void addConnectorRef(ObjRef connectorRef) {
		if(this.connectorRefList == null) {
			this.connectorRefList = new ArrayList<ObjRef>();
		}
		this.connectorRefList.add(connectorRef);
	}
	public String getArchStudioId() {
		return archStudioId;
	}
	public void setArchStudioId(String archStudioId) {
		this.archStudioId = archStudioId;
	}
}
