package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.uci.isr.xarchflat.ObjRef;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.XadlConnectorConfiguration;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.workflow.IEsbWorkflow;

public abstract class AbstractMessagingSystem implements IMessagingSystem  {

	protected List<IEsbWorkflow> esbWorkflows = new ArrayList<IEsbWorkflow>();
	protected Map<ObjRef, XadlConnectorConfiguration> connectorConnectorConfigMap =  new HashMap<ObjRef, XadlConnectorConfiguration>();

	public List<IEsbWorkflow> getEsbWorkflows() {
		return esbWorkflows;
	}

	public void setEsbWorkflows(List<IEsbWorkflow> esbWorkflows) {
		this.esbWorkflows = esbWorkflows;
	}
	
	public void addEsbWorkflows(IEsbWorkflow esbWorkflow) {
		this.esbWorkflows.add(esbWorkflow);
	}

	public Map<ObjRef, XadlConnectorConfiguration> getConnectorConnectorConfigMap() {
		return connectorConnectorConfigMap;
	}

	public void setConnectorConnectorConfigMap(Map<ObjRef, XadlConnectorConfiguration> connectorConnnectorConfigMap) {
		this.connectorConnectorConfigMap = connectorConnnectorConfigMap;
	}
	
}