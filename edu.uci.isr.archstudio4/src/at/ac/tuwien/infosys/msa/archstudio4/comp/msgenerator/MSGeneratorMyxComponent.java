package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator;

import edu.uci.isr.myx.fw.AbstractMyxSimpleBrick;
import edu.uci.isr.myx.fw.IMyxName;
import edu.uci.isr.myx.fw.MyxRegistry;
import edu.uci.isr.myx.fw.MyxUtils;
import edu.uci.isr.xarchflat.XArchFlatInterface;

public class MSGeneratorMyxComponent extends AbstractMyxSimpleBrick{

	public static final IMyxName INTERFACE_NAME_IN_MSGENERATOR = MyxUtils.createName("msgenerator");
	public static final IMyxName INTERFACE_NAME_OUT_XARCH = MyxUtils.createName("xarch");

	protected MSGenerator msgenerator;
	protected XArchFlatInterface xarch = null;
	
	private MyxRegistry er = MyxRegistry.getSharedInstance();

	public MSGeneratorMyxComponent(){
	}

	public void init(){
		xarch = (XArchFlatInterface)MyxUtils.getFirstRequiredServiceObject(this, INTERFACE_NAME_OUT_XARCH);
		msgenerator = new MSGenerator(xarch);
		
	}

	public void end(){
		er.unregister(this);
	}
	
	public Object getServiceObject(IMyxName interfaceName){
		if(interfaceName.equals(INTERFACE_NAME_IN_MSGENERATOR)){
			return msgenerator;
		}
		return null;
	}

}
