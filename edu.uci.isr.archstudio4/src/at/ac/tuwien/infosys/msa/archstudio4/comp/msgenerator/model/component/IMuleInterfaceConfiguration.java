package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.component;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.MSAInterfaceDirection;

public interface IMuleInterfaceConfiguration {

	public abstract boolean isRequestReplyEndpoint();

	public abstract String getRequestReplyEndpointId();

	public abstract boolean isGenerateInteface();

	public abstract String getDurableName();

	public String getReplyChannelName();

	public abstract MSAInterfaceDirection getDirection();

}
