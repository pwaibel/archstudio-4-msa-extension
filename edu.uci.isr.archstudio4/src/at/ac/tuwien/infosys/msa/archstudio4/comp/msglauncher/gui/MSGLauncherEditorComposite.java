package at.ac.tuwien.infosys.msa.archstudio4.comp.msglauncher.gui;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msglauncher.FilesInMemoryStore;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msglauncher.config.model.JmsListType;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msglauncher.config.model.MuleListType;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msglauncher.config.model.OutputFileConfigType;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msglauncher.config.model.OutputFileType;

public class MSGLauncherEditorComposite extends Composite {
	
	private TableViewer tableViewer_Mule;
	private TableViewer tableViewer_Jms;
	private FilesInMemoryStore filesInMemoryStore;
	private Text configFilePath;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public MSGLauncherEditorComposite(Composite parent, int style) {
		super(parent, SWT.NO_BACKGROUND);
		
		filesInMemoryStore = FilesInMemoryStore.getInstance();
		
		Group group = new Group(this, SWT.NONE);
		group.setBounds(10, 10, 804, 364);
		
		Button btnAddFile_Jms = new Button(group, SWT.NONE);
		btnAddFile_Jms.setBounds(663, 223, 117, 28);
		btnAddFile_Jms.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				addJmsFile();
			}
		});
		btnAddFile_Jms.setText("Add Entry");
		
		Button btnAddFile_Mule = new Button(group, SWT.NONE);
		btnAddFile_Mule.setBounds(663, 59, 117, 28);
		btnAddFile_Mule.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				addMuleFile();
			}
		});
		btnAddFile_Mule.setText("Add Entry");
		
		Label lblMuleFiles = new Label(group, SWT.NONE);
		lblMuleFiles.setBounds(10, 40, 59, 14);
		lblMuleFiles.setFont(SWTResourceManager.getFont("Lucida Grande", 11, SWT.BOLD));
		lblMuleFiles.setText("Mule Files");
		
		Label lblJmsConfigurationFiles = new Label(group, SWT.NONE);
		lblJmsConfigurationFiles.setBounds(10, 204, 157, 14);
		lblJmsConfigurationFiles.setFont(SWTResourceManager.getFont("Lucida Grande", 11, SWT.BOLD));
		lblJmsConfigurationFiles.setText("JMS Files");
		
		tableViewer_Jms = new TableViewer(group, SWT.BORDER | SWT.FULL_SELECTION);
		Table table_Jms = tableViewer_Jms.getTable();
		table_Jms.setBounds(10, 224, 647, 126);
		table_Jms.setLinesVisible(true);
		table_Jms.setHeaderVisible(true);
		
		TableViewerColumn tableViewerColumn = new TableViewerColumn(tableViewer_Jms, SWT.NONE);
		tableViewerColumn.setLabelProvider(new CellIdLabelProvider());
		tableViewerColumn.setEditingSupport(new CellJmsIdEditingSupport(tableViewerColumn.getViewer()));
		TableColumn tblclmnName = tableViewerColumn.getColumn();
		tblclmnName.setWidth(75);
		tblclmnName.setText("Id");
		
		TableViewerColumn tblclmnPath_Jms = new TableViewerColumn(tableViewer_Jms, SWT.NONE);
		tblclmnPath_Jms.setLabelProvider(new CellPathLabelProvider());
		tblclmnPath_Jms.setEditingSupport(new CellPathEditingSupport(tblclmnPath_Jms.getViewer()));
		TableColumn tblclmnPath = tblclmnPath_Jms.getColumn();
		tblclmnPath.setWidth(560);
		tblclmnPath.setText("File");
		
		tableViewer_Mule = new TableViewer(group, SWT.BORDER | SWT.FULL_SELECTION);
		Table table_Mule = tableViewer_Mule.getTable();
		table_Mule.setBounds(10, 60, 647, 138);
		table_Mule.setLinesVisible(true);
		table_Mule.setHeaderVisible(true);
		
		TableViewerColumn tblclmnName_Mule = new TableViewerColumn(tableViewer_Mule, SWT.NONE);
		tblclmnName_Mule.setLabelProvider(new CellIdLabelProvider());
		tblclmnName_Mule.setEditingSupport(new CellMuleIdEditingSupport(tblclmnName_Mule.getViewer()));
		TableColumn tblclmnName_1 = tblclmnName_Mule.getColumn();
		tblclmnName_1.setWidth(75);
		tblclmnName_1.setText("Id");
		
		TableViewerColumn tblclmnPath_Mule = new TableViewerColumn(tableViewer_Mule, SWT.NONE);
		tblclmnPath_Mule.setLabelProvider(new CellPathLabelProvider());
		tblclmnPath_Mule.setEditingSupport(new CellPathEditingSupport(tblclmnPath_Mule.getViewer()));
		TableColumn tblclmnPath_1 = tblclmnPath_Mule.getColumn();
		tblclmnPath_1.setWidth(560);
		tblclmnPath_1.setText("File");
		
		Button btnDeleteFile = new Button(group, SWT.NONE);
		btnDeleteFile.setBounds(663, 257, 117, 28);
		btnDeleteFile.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				deleteJmsFile();
			}
		});
		btnDeleteFile.setText("Delete Entry");
		
		Button btnDeleteFile_1 = new Button(group, SWT.NONE);
		btnDeleteFile_1.setBounds(663, 93, 117, 28);
		btnDeleteFile_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				deleteMuleFile();
			}
		});
		btnDeleteFile_1.setText("Delete Entry");
		
		configFilePath = new Text(group, SWT.BORDER);
		configFilePath.setBounds(11, 15, 523, 19);
		
		Button btnLoadConfig = new Button(group, SWT.NONE);
		btnLoadConfig.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				loadConfigDialog();
			}
		});
		btnLoadConfig.setBounds(540, 10, 117, 28);
		btnLoadConfig.setText("Load Config");
		
		Button btnSaveConfig = new Button(group, SWT.NONE);
		btnSaveConfig.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				saveOutputFileConfig();
			}
		});
		btnSaveConfig.setBounds(663, 10, 117, 28);
		btnSaveConfig.setText("Save Config");
		
		tableViewer_Mule.setContentProvider(new ArrayContentProvider());
		tableViewer_Mule.setInput(filesInMemoryStore.getMuleFileList());
		tableViewer_Jms.setContentProvider(new ArrayContentProvider());		
		tableViewer_Jms.setInput(filesInMemoryStore.getJmsFileList());
	
		
	}

	private void addMuleFile() {		
		filesInMemoryStore.newMuleFile();
		tableViewer_Mule.refresh(true);
	}
	
	private void addJmsFile() {
		filesInMemoryStore.newJmsFile();
		tableViewer_Jms.refresh(true);
	}
	
 	private void deleteJmsFile() { 		
 		OutputFileType outputFile = (OutputFileType) ((IStructuredSelection) tableViewer_Jms.getSelection()).getFirstElement(); 		
 		filesInMemoryStore.getJmsFileList().remove(outputFile);
 		
 		tableViewer_Jms.refresh(true);		
	}
 	
 	private void deleteMuleFile() { 		
 		OutputFileType outputFile = (OutputFileType) ((IStructuredSelection) tableViewer_Mule.getSelection()).getFirstElement(); 		
 		filesInMemoryStore.getMuleFileList().remove(outputFile); 		
 		tableViewer_Mule.refresh(true);		
	}
 
 	private void loadConfigDialog() {
 		FileDialog dialog = new FileDialog(this.getShell(), SWT.OPEN); 
 		String configPath = dialog.open();
 		
 		
 		configFilePath.setText(configPath);
 		
 		 try {
 			 
 			File file = new File(configPath);
 			JAXBContext jaxbContext = JAXBContext.newInstance(OutputFileConfigType.class);
 	 
 			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
 			OutputFileConfigType outputFileConfigType = (OutputFileConfigType) jaxbUnmarshaller.unmarshal(file);

 			filesInMemoryStore.getJmsFileList().clear();
 			filesInMemoryStore.getMuleFileList().clear(); 			
 			filesInMemoryStore.getJmsFileList().addAll(outputFileConfigType.getJmsList().getOutputFile());
 			filesInMemoryStore.getMuleFileList().addAll(outputFileConfigType.getMuleList().getOutputFile());
 			
 			tableViewer_Mule.refresh();
 			tableViewer_Jms.refresh();
 	 
 		  } catch (JAXBException e) {
 			e.printStackTrace();
 		  }
 	 
 		
 	}
 	
	private void saveOutputFileConfig() {
 		FileDialog dialog = new FileDialog(this.getShell(), SWT.SAVE);
 		
 		if(!configFilePath.equals("")) {
 			File file = new File(configFilePath.getText()); 			
 			dialog.setFilterPath(file.getAbsolutePath());
 			dialog.setFileName(file.getName());
 		}
 		String configPath = dialog.open();
 		configFilePath.setText(configPath);
 		
 		OutputFileConfigType output = new OutputFileConfigType();
 		JmsListType jmsListType = new JmsListType();
		jmsListType.getOutputFile().addAll(filesInMemoryStore.getJmsFileList());
 		output.setJmsList(jmsListType);
 		
 		MuleListType muleListType = new MuleListType();
 		muleListType.getOutputFile().addAll(filesInMemoryStore.getMuleFileList());
 		output.setMuleList(muleListType);
 		
		try {

			File file = new File(configPath);
			JAXBContext jaxbContext = JAXBContext.newInstance(OutputFileConfigType.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			jaxbMarshaller.marshal(output, file);

		} catch (JAXBException e) {
			e.printStackTrace();
		}

	}
	
}


