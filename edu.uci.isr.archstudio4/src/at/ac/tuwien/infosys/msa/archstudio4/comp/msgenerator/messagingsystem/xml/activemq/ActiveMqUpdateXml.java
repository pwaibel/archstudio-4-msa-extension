package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem.xml.activemq;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.PersistenceAdditionalConfig;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.XadlConnectorConfiguration;

public class ActiveMqUpdateXml {

	private Document doc;
	private ActiveMqNewXml activeMqNewXml;
	
	private List<String> usedPersistenceArchStudioIds = new ArrayList<String>();
	private List<String> usedTransportArchStudioIds = new ArrayList<String>();

	public ActiveMqUpdateXml(Document doc) {
		this.doc = doc;
		activeMqNewXml = new ActiveMqNewXml(doc);

	}

	public Element findUpdateRootElement() {

		NodeList rootElements = doc.getElementsByTagName("beans");
		
		Element rootElement;
		if(rootElements.getLength() == 1) {
			rootElement = (Element) rootElements.item(0);
			rootElement = updateBeansElement(rootElement);
		} else {
			rootElement = activeMqNewXml.generateBeansElement();
		}

		return rootElement;
	}

	public Element findUpdateBrokerElement(Element rootElement) {
		
		NodeList brokerElements = doc.getElementsByTagName("broker");
		
		Element brokerElement;
		if (brokerElements.getLength() == 1) {
			brokerElement = (Element) brokerElements.item(0);
			brokerElement = updateBrokerElement(brokerElement);
		} else {
			brokerElement = activeMqNewXml.generateBeansElement();
		}

		return brokerElement;
	}

	/**
	 * <transportConnectors> 
	 * 			<transportConnector uri="tcp://localhost:61616"/>
	 * </transportConnectors>
	 * 
	 * @param doc
	 * @param brokerElement
	 * @param connectorConfigList
	 * @return
	 */
	public Element findUpdateTransportConnectorElements(Element brokerElement, List<XadlConnectorConfiguration> connectorConfigList) {

		NodeList transportNodes = brokerElement.getElementsByTagName("transportConnectors");
		Element transportElement;
		if (transportNodes.getLength() == 1) {
			transportElement = (Element) transportNodes.item(0);
		} else {
			transportElement = doc.createElement("transportConnectors");
			brokerElement.appendChild(transportElement);
		}

		return updateTransportConnectorElements(transportElement, connectorConfigList);
	}

	private Element updateTransportConnectorElements(Element transportElement, List<XadlConnectorConfiguration> connectorConfigList) {

		NodeList transportConnectors = transportElement.getElementsByTagName("transportConnector");

		List<XadlConnectorConfiguration> alreadyWritten = new ArrayList<XadlConnectorConfiguration>();
		for (XadlConnectorConfiguration connectorConfig : connectorConfigList) {
			if (!alreadyWritten.contains(connectorConfig)) {
				Element currnetTransportConnector = null;
				boolean found = false;
				for (int i = 0; i < transportConnectors.getLength(); i++) {
					currnetTransportConnector = (Element) transportConnectors.item(i);
					found = checkForArchIdComment(currnetTransportConnector, connectorConfig.getArchStudioId());
					if (found) {
						break;
					}
				}

				if (found) {
					currnetTransportConnector.setAttribute("uri", connectorConfig.getJmsConnector().getBrokerUrl());
				} else {
					Element transportConnector = doc.createElement("transportConnector");
					transportConnector.setAttribute("uri", connectorConfig.getJmsConnector().getBrokerUrl());
					activeMqNewXml.generateArchStudioIdComment(transportConnector, connectorConfig.getArchStudioId());
					transportElement.appendChild(transportConnector);
					alreadyWritten.add(connectorConfig);
				}
				
				usedTransportArchStudioIds.add(connectorConfig.getArchStudioId());
			}
		}

		return transportElement;
	}

	/**
	 * <persistenceAdapter>
     *    <kahaDB directory="${activemq.data}/kahadb"/>
     * </persistenceAdapter>
	 * 
	 * @param doc
	 * @param brokerElement
	 * @param connectorConfigList
	 * @return
	 */
	public Element findUpdatePersistenceAdapterElements(Element brokerElement, List<XadlConnectorConfiguration> connectorConfigList) {

		for (XadlConnectorConfiguration connectorConfig : connectorConfigList) {
			List<PersistenceAdditionalConfig> additionalConfigs = connectorConfig.getJmsConnector().getPersistenceConfigs();

			for (PersistenceAdditionalConfig additionalConfig : additionalConfigs) {
				if (additionalConfig instanceof PersistenceAdditionalConfig) {

					NodeList persistenceAdapterNodes = brokerElement.getElementsByTagName("persistenceAdapter");

					usedPersistenceArchStudioIds.add(connectorConfig.getArchStudioId());
					
					for (int i = 0; i < persistenceAdapterNodes.getLength();) { 
						Element persistenceAdapterElement = (Element)persistenceAdapterNodes.item(i);	
						return updatePersistenceAdapterElements(persistenceAdapterElement, additionalConfig, connectorConfig.getArchStudioId() );
					}
					
					Element	persistenceAdapterElement = doc.createElement("persistenceAdapter");
					brokerElement.appendChild(persistenceAdapterElement);
					return updatePersistenceAdapterElements(persistenceAdapterElement, additionalConfig, connectorConfig.getArchStudioId() );
					
				}
			}
		}
		return brokerElement;
	}
	

	private Element updatePersistenceAdapterElements(Element persistenceAdapterElement, PersistenceAdditionalConfig additionalConfig, String archStudioId) {
				
		NodeList persistenceAdapters = persistenceAdapterElement.getElementsByTagName("*");
		
		PersistenceAdditionalConfig addConfig = (PersistenceAdditionalConfig) additionalConfig;
		
		if(persistenceAdapters.getLength() > 0) {
			Element persistenceAdapter = (Element)persistenceAdapters.item(0); 	// there could be only 1
			if(!persistenceAdapter.getTagName().equals(addConfig.getAdapterName())) {
				Element newPersistenceAdapter = doc.createElement(addConfig.getAdapterName());
				activeMqNewXml.generateArchStudioIdComment(persistenceAdapter, archStudioId);
				newPersistenceAdapter.setAttribute("directory", addConfig.getDirectory());
				activeMqNewXml.generateArchStudioIdComment(newPersistenceAdapter, archStudioId);
			}
			else {
				persistenceAdapter.setAttribute("directory", addConfig.getDirectory());
			}
		}
		else {
			Element newPersistenceAdapter = doc.createElement(addConfig.getAdapterName());
			newPersistenceAdapter.setAttribute("directory", addConfig.getDirectory());
			activeMqNewXml.generateArchStudioIdComment(newPersistenceAdapter, archStudioId);
			persistenceAdapterElement.appendChild(newPersistenceAdapter);
		}
			
		return persistenceAdapterElement;
		
	}
	
	
	/**
	 * <broker xmlns="http://activemq.apache.org/schema/core">
	 * 
	 * @param doc
	 * @param rootElement
	 * @return
	 */
	protected Element updateBrokerElement(Element brokerElement) {
		brokerElement.setAttribute("xmlns", "http://activemq.apache.org/schema/core");
		return brokerElement;
	}

	/**
	 * <beans xmlns="http://www.springframework.org/schema/beans"
	 * xmlns:amq="http://activemq.apache.org/schema/core"
	 * xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	 * xsi:schemaLocation="http://www.springframework.org/schema/beans
	 * http://www.springframework.org/schema/beans/spring-beans-2.0.xsd
	 * http://activemq.apache.org/schema/core
	 * http://activemq.apache.org/schema/core/activemq-core.xsd
	 * http://camel.apache.org/schema/spring
	 * http://camel.apache.org/schema/spring/camel-spring.xsd">
	 * 
	 * @param doc
	 * @return
	 */
	protected Element updateBeansElement(Element rootElement) {
		rootElement.setAttribute("xmlns", "http://www.springframework.org/schema/beans");
		rootElement.setAttribute("xmlns:amq", "http://activemq.apache.org/schema/core");
		rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		rootElement.setAttribute("xsi:schemaLocation", "http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.0.xsd http://activemq.apache.org/schema/core http://activemq.apache.org/schema/core/activemq-core.xsd http://camel.apache.org/schema/spring http://camel.apache.org/schema/spring/camel-spring.xsd");
		return rootElement;
	}

	private boolean checkForArchIdComment(Element element, String archStudioId) {

		NodeList nodeList = element.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++) {
			if (nodeList.item(i).getNodeType() == Element.COMMENT_NODE) {
				Comment comment = (Comment) nodeList.item(i);
				if (comment.getTextContent().contains("ArchStudioId=" + archStudioId)) {
					return true;
				}
			}
		}

		return false;
	}

	public List<String> getUsedPersistenceArchStudioIds() {
		return usedPersistenceArchStudioIds;
	}

	public List<String> getUsedTransportArchStudioIds() {
		return usedTransportArchStudioIds;
	}

	
}
