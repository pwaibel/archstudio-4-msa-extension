package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.component;

public class AdditionalConfig {
	private String name;
	private String value;
	
	public AdditionalConfig(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}
	
	public String getName() {
		return name;
	}
	
	public String getValue() {
		return value;
	}
	
}
