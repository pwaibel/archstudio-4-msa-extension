package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.transformation;

import edu.uci.isr.xarchflat.XArchFlatInterface;

public abstract class AbstractTransformation {

	protected XArchFlatInterface xarch;
	
	public AbstractTransformation(XArchFlatInterface xarch) {		
		this.xarch = xarch;
	}
	
}
