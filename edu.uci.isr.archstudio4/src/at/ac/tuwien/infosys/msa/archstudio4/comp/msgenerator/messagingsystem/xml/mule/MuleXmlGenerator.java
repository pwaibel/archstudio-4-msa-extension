package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem.xml.mule;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem.IMessagingSystem;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem.xml.AbstractXmlGenerator;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem.xml.mule.update.MuleUpdateXml;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.workflow.IEsbWorkflow;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msglauncher.FilesInMemoryStore;

public class MuleXmlGenerator extends AbstractXmlGenerator {

	
	private Map<String, List<IEsbWorkflow>> fileIdEsbWorkflowMap = new HashMap<String, List<IEsbWorkflow>>();
	
	@Override
	public void generateXmlFile(IMessagingSystem esb) {
		for(IEsbWorkflow workflow : esb.getEsbWorkflows()) {
			String currentFileId = workflow.getComponentConfig().getFileId();
			
			if(!fileIdEsbWorkflowMap.containsKey(currentFileId)) {
				fileIdEsbWorkflowMap.put(currentFileId, new ArrayList<IEsbWorkflow>());
			}
			
			fileIdEsbWorkflowMap.get(currentFileId).add(workflow);			
		}
		
		for(Entry<String, List<IEsbWorkflow>> mapEntrySet : fileIdEsbWorkflowMap.entrySet()) {
			String fileId = mapEntrySet.getKey();
			List<IEsbWorkflow> workflowList = mapEntrySet.getValue();
			
			String filePath = FilesInMemoryStore.getInstance().getMuleFilePath(fileId);
			
			generateWorkflow(filePath, workflowList, esb.getEsbVersion(), esb.getEsbJmsSystem());
		}
		
		
	}
	
	private void generateWorkflow(String filePath, List<IEsbWorkflow> workflow, String esbVersion, String esbJmsSystem) {
		
		File xmlFile = new File(filePath);
		
		if(xmlFile.exists()) {
			updateNewWorkflow(filePath, workflow, esbVersion, esbJmsSystem);			
		}
		else {
			generateNewWorkflow(filePath, workflow, esbVersion, esbJmsSystem);
		}
		
	}
	
	/**
	 * Update flows
	 */
	private void updateNewWorkflow(String filePath, List<IEsbWorkflow> workflow, String esbVersion, String esbJmsSystem) {
		try {
			
			Document doc = super.openXmlFile(filePath);
			
			MuleUpdateXml muleUpdateXml = new MuleUpdateXml(doc, esbJmsSystem);
			muleUpdateXml.updateMuleRootElement(esbVersion);
			
			muleUpdateXml.updateFlowSpecificInterfaceInformation(workflow);
			
			super.writeXmlFile(doc,filePath);
			
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}


	private void generateNewWorkflow(String filePath, List<IEsbWorkflow> workflow, String esbVersion, String esbJmsSystem) {
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			MuleNewXml muleNewXml = new MuleNewXml(doc);
			Element rootElement = muleNewXml.generateMuleElement(esbVersion);
			
			muleNewXml.generateFlowSpecificInterfaceInformation(rootElement, workflow, esbJmsSystem);
			
			super.writeXmlFile(doc,filePath);
			
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}

}
