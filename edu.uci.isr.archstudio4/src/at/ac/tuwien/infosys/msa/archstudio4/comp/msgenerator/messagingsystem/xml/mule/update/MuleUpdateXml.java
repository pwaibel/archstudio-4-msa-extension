package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem.xml.mule.update;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.XadlConnectorConfiguration;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.workflow.ConnectionEndpoint;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.workflow.IEsbWorkflow;

public class MuleUpdateXml extends AbstractMuleUpdate {

	private List<XadlConnectorConfiguration> generatedGlobalConnectors = new ArrayList<XadlConnectorConfiguration>();
	private NodeList globalConnectorNodes;
	
	private String esbJmsSystem;
	private Element rootElement;
	
	private Map<String, List<String>> usedFlowArchStudioIds = new HashMap<String, List<String>>();
	private Map<String, List<String>> usedConnectorArchStudioIds = new HashMap<String, List<String>>();
	
	public MuleUpdateXml(Document doc, String esbJmsSystem) {
		super(doc);
		this.esbJmsSystem = esbJmsSystem;
	}

	public Element updateMuleRootElement(String muleVersion) {

		rootElement = (Element)doc.getFirstChild();

		if (rootElement.getTagName().equals("mule")) {

		} else {
			rootElement = generateHelper.generateMuleElement(doc, muleVersion);
		}

		return rootElement;
	}


	public void updateFlowSpecificInterfaceInformation(List<IEsbWorkflow> workflows) {

		globalConnectorNodes = rootElement.getElementsByTagName("jms:" + esbJmsSystem + "-connector");
		
		for (IEsbWorkflow workflow : workflows) {

			Element flowElement = findFlow(workflow);
			
			if(flowElement != null) {
				generateHelper.updateStartFlow(flowElement, doc, rootElement, workflow.getFlowName(), workflow.getComponentConfig());
			}
			else {
				flowElement = generateHelper.generateStartFlow(doc, rootElement, workflow.getFlowName(), workflow.getComponentConfig());
			}
			
			usedFlowArchStudioIds.put(workflow.getComponentConfig().getArchStudioId(), new ArrayList<String>());
			
			boolean startEndpoint = true;
			MuleUpdateEndpoints endpointsWorker = new MuleUpdateEndpoints(doc);
			for(ConnectionEndpoint conDirDomInterface : workflow.getOrderedInterfaces()) {
				
				updateGlobalConnector(conDirDomInterface);
	
				endpointsWorker.updateEndpoints(flowElement, workflow, conDirDomInterface, startEndpoint, usedFlowArchStudioIds);
				startEndpoint = false;
			}
			
			keepEndpointOrder(workflow.getOrderedInterfaces(), flowElement);
			
		}
		
		MuleUpdateDeleteUnusedElements postWork = new MuleUpdateDeleteUnusedElements(doc);
		postWork.deleteUnusedElements(usedFlowArchStudioIds, usedConnectorArchStudioIds);
		
		
	}


	private Element findFlow(IEsbWorkflow workflow) {
		NodeList flowNodes = rootElement.getElementsByTagName("flow");
		String archStudioId = workflow.getComponentConfig().getArchStudioId();
		
		for(int i = 0; i < flowNodes.getLength(); i++) {
			Element currentFlow = (Element)flowNodes.item(i);
			if(checkForArchIdComment(currentFlow, archStudioId)) {
				return currentFlow;
			}
		}
		
		return null;
	}

	private void updateGlobalConnector(ConnectionEndpoint connectionEndpoint) {
		XadlConnectorConfiguration connectorConfig = connectionEndpoint.getConnectorConfig();
		if (!generatedGlobalConnectors.contains(connectorConfig)) {

			for (int i = 0; i < globalConnectorNodes.getLength(); i++) {
				Element globalConnector = (Element) globalConnectorNodes.item(i);
				if (checkForArchIdComment(globalConnector, connectorConfig.getArchStudioId())) {
					generateHelper.updateConnectorElement(globalConnector, connectorConfig);										
					generatedGlobalConnectors.add(connectorConfig);
					saveInGlobalConnectorMap(connectionEndpoint);
					
					return;
				}
			}

			generateHelper.generateConnectorElement(doc, rootElement, esbJmsSystem, connectionEndpoint);
			generatedGlobalConnectors.add(connectorConfig);
			
			saveInGlobalConnectorMap(connectionEndpoint);
		}
	}
	
	private void saveInGlobalConnectorMap(ConnectionEndpoint connectionEndpoint) {
		XadlConnectorConfiguration connectorConfig = connectionEndpoint.getConnectorConfig();
		
		if(!usedConnectorArchStudioIds.containsKey(connectorConfig.getArchStudioId())) {
			usedConnectorArchStudioIds.put(connectorConfig.getArchStudioId(), new ArrayList<String>());
		}
		List<String> usedConnection = usedConnectorArchStudioIds.get(connectorConfig.getArchStudioId());
		usedConnection.add(connectionEndpoint.getInterfaceConfig().getArchStudioId());
		
	}

	
	public void keepEndpointOrder(List<ConnectionEndpoint> orderedList, Element workflowElement) {
		
		List<String> documentEndpointOrder = new ArrayList<String>();
		Map<String, Node> endpointCommentElements = new HashMap<String, Node>();
		
		List<Node> allComments = findAllArchStudioIdComments(workflowElement);
		
		for(Node commentElement : allComments) {
			Comment comment= (Comment) commentElement;
			String commentString = comment.getTextContent();

			if (commentString.contains("ArchStudioId=") && isEndpointComment(commentElement)) {
				String id = commentString.split("ArchStudioId=")[1];
				id = id.split(" ")[0].trim();
				endpointCommentElements.put(id, commentElement);
				documentEndpointOrder.add(id);
			}
		}
		
		int orderCounter = 0;
		for(ConnectionEndpoint currentEndpoint : orderedList) {
			String archStudioId = currentEndpoint.getInterfaceConfig().getArchStudioId();
			if(!documentEndpointOrder.get(orderCounter).equals(archStudioId)) {
									
				String archStudioIdWrongOrder = documentEndpointOrder.get(orderCounter);
				
				Node correctEndpointClone = endpointCommentElements.get(archStudioId).getParentNode().cloneNode(true);
				Node incorrectEndpointClone = endpointCommentElements.get(archStudioIdWrongOrder).getParentNode().cloneNode(true);
				
				workflowElement.replaceChild(correctEndpointClone, endpointCommentElements.get(archStudioIdWrongOrder).getParentNode());
				workflowElement.replaceChild(incorrectEndpointClone, endpointCommentElements.get(archStudioId).getParentNode());
				
				int firstArchStudioIdIndex = documentEndpointOrder.indexOf(archStudioIdWrongOrder);
				int secondArchStudioIdIndex = documentEndpointOrder.indexOf(archStudioId);
				
				documentEndpointOrder.set(firstArchStudioIdIndex, archStudioId);							
				documentEndpointOrder.set(secondArchStudioIdIndex, archStudioIdWrongOrder);
				
			}
			orderCounter++;
		}
		
	}
}






