package at.ac.tuwien.infosys.msa.archstudio4.comp.msacc;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.IMSGenerator;
import edu.uci.isr.myx.fw.AbstractMyxSimpleBrick;
import edu.uci.isr.myx.fw.IMyxName;
import edu.uci.isr.myx.fw.MyxRegistry;
import edu.uci.isr.myx.fw.MyxUtils;
import edu.uci.isr.xarchflat.XArchFlatInterface;

public class MSACCMyxComponent extends AbstractMyxSimpleBrick{

	public static final IMyxName INTERFACE_NAME_IN_MSACC = MyxUtils.createName("msacc");
	public static final IMyxName INTERFACE_NAME_OUT_XARCH = MyxUtils.createName("xarch");
	public static final IMyxName INTERFACE_NAME_OUT_MSGENERATOR = MyxUtils.createName("msgenerator");
	
	protected MSACCImpl msacc;
	protected IMSGenerator msgenerator = null;
	protected XArchFlatInterface xarch = null;
	
	private MyxRegistry er = MyxRegistry.getSharedInstance();
	
	public MSACCMyxComponent(){
	}
	
	public void init(){	
		
		xarch = (XArchFlatInterface)MyxUtils.getFirstRequiredServiceObject(this, INTERFACE_NAME_OUT_XARCH);
		msgenerator = (IMSGenerator)MyxUtils.getFirstRequiredServiceObject(this, INTERFACE_NAME_OUT_MSGENERATOR);
		msacc = new MSACCImpl(xarch, msgenerator);
		
		er.register(this);
	}
	
	public void end(){
		er.unregister(this);
	}

	public Object getServiceObject(IMyxName interfaceName){
		if(interfaceName.equals(INTERFACE_NAME_IN_MSACC)){
			return msacc;
		}
		return null;
	}
	
	public IMSGenerator getMSGenerator() {
		return msgenerator;
	}

}

