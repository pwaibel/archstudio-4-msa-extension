package at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.checks;

import edu.uci.isr.xarchflat.ObjRef;
import edu.uci.isr.xarchflat.XArchFlatInterface;

public abstract class AbstractConsistencyCheck implements IConsistencyCeck  {

	protected XArchFlatInterface xarch;
	
	public AbstractConsistencyCheck(XArchFlatInterface xarch) {
		this.xarch = xarch;
	}
	
	public String getDirectionAsString(XArchFlatInterface xarch, ObjRef interfaceRef) {
		ObjRef directionRef = (ObjRef) xarch.get(interfaceRef, "direction");
		return (String) xarch.get(directionRef, "value");
	}
}
