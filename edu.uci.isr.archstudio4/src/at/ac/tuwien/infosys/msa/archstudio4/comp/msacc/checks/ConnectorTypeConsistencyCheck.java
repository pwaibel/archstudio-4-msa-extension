package at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.checks;

import java.util.Set;

import at.ac.tuwien.infosys.msa.archstudio4.comp.GeneralHelper;
import edu.uci.isr.xadlutils.XadlUtils;
import edu.uci.isr.xarchflat.ObjRef;
import edu.uci.isr.xarchflat.XArchFlatInterface;

/**
 * ConnectorType Consistency Checks:
 * Errors:
 * <ul>
 * 	<li>Check if transport information is set</li>
 * </ul>
 * 
 * Warnings:
 * <ul>
 * 	<li>Check if the Connector has an implementation</li>
 * </ul>
 * @author philippwaibel
 *
 */
public class ConnectorTypeConsistencyCheck extends AbstractConsistencyCheck {
	
	public ConnectorTypeConsistencyCheck(XArchFlatInterface xarch) {
		super(xarch);
	}

	@Override
	public boolean check(Set<ObjRef> componentRefs, Set<ObjRef> connectorRefs, ObjRef structureRef) throws ArchitectureConsistencyException {

		for (ObjRef connectorRef : connectorRefs) {
			ObjRef connectorTypeRef = XadlUtils.resolveXLink(xarch, connectorRef, "type");

			ObjRef jmsImplementationRef = GeneralHelper.getJmsImplementationRef(xarch, connectorTypeRef);
			if (jmsImplementationRef != null) {

				String brokerUrl = "";
				for (ObjRef transportConfig : xarch.getAll(jmsImplementationRef, "Transport_Configuration")) {
					brokerUrl = (String) xarch.get(transportConfig, "transportConnector");	
				}
				if (brokerUrl == null || brokerUrl.isEmpty()) {
					throw new ArchitectureConsistencyException("The connector type \"" + XadlUtils.getDescription(xarch, connectorTypeRef) + "\" doesn't define the property Transport_Configuration.");
				}
			}
			
			else {
				String message = "The connector type \"" + XadlUtils.getDescription(xarch, connectorTypeRef) + "\" doesn't have an implementation.";
				ArchitectureWarning architectureWarning = new ArchitectureWarning();
				boolean shouldCancle = architectureWarning.showArchitectureWarning(message);
				if (shouldCancle) {
					throw new ArchitectureConsistencyException(message);
				}
			}
			
		}

		return true;
	}

}
