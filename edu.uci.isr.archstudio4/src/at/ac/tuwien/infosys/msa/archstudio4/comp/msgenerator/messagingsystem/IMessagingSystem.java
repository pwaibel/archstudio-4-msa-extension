package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem;

import java.util.List;
import java.util.Map;

import edu.uci.isr.xarchflat.ObjRef;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.XadlConnectorConfiguration;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.workflow.IEsbWorkflow;

public interface IMessagingSystem {

	public List<IEsbWorkflow> getEsbWorkflows() ;
	public void setEsbWorkflows(List<IEsbWorkflow> esbWorkflows);	
	public void addEsbWorkflows(IEsbWorkflow esbWorkflow);
	
	public Map<ObjRef, XadlConnectorConfiguration> getConnectorConnectorConfigMap() ;
	public void setConnectorConnectorConfigMap(Map<ObjRef, XadlConnectorConfiguration> connectorConnectorConfigMap) ;
	
	public String getEsbJmsSystem();
	public String getEsbVersion();
	public boolean generateXmlFile();
	
}
