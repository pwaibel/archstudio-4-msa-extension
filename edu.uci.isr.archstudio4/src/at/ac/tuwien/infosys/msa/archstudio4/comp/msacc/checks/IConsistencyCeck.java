package at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.checks;

import java.util.Set;


import edu.uci.isr.xarchflat.ObjRef;

public interface IConsistencyCeck {

	/**
	 * Start Consistency Check
	 * @param componentRefs	
	 * @param connectorRefs
	 * @param structureRef
	 * @return
	 * @throws ArchitectureConsistencyException
	 */
	public boolean check(Set<ObjRef> componentRefs, Set<ObjRef> connectorRefs, ObjRef structureRef) throws ArchitectureConsistencyException;
	
}