package at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.checks;

import java.util.Set;

import at.ac.tuwien.infosys.msa.archstudio4.comp.GeneralHelper;
import edu.uci.isr.xadlutils.XadlUtils;
import edu.uci.isr.xarchflat.ObjRef;
import edu.uci.isr.xarchflat.XArchFlatInterface;

/**
 * Connector Consistency Checks:
 * Errors:
 * <ul>
 * 	<li>Check if Topic_Configuration or Queue_Configuration is set.</li>
 *  <li>Check if only Topic_Configuration or Queue_Configuration is set.</li>
 * </ul>
 * 
 * Warnings:
 * <ul>
 * 	<li>Check if the Connector has an implementation.</li>
 * </ul>
 * @author philippwaibel
 *
 */
public class ConnectorConsistencyCheck extends AbstractConsistencyCheck {

	
	public ConnectorConsistencyCheck(XArchFlatInterface xarch) {
		super(xarch);
	}
	
	@Override
	public boolean check(Set<ObjRef> componentRefs, Set<ObjRef> connectorRefs, ObjRef structureRef) throws ArchitectureConsistencyException {
		
		for (ObjRef connectorRef : connectorRefs) {

			ObjRef jmsImplementationRef = GeneralHelper.getChannelImplementationRef(xarch, connectorRef);
			if (jmsImplementationRef != null) {

				if (xarch.get(jmsImplementationRef, "Queue_Configuration") == null && xarch.get(jmsImplementationRef, "Topic_Configuration") == null) {
					throw new ArchitectureConsistencyException("The connector \"" + XadlUtils.getDescription(xarch, connectorRef) + "\" has a ChannelImplementation but either Topic_Configuration nor Queue_Configuration is set.");
				}
				if (xarch.get(jmsImplementationRef, "Queue_Configuration") != null && xarch.get(jmsImplementationRef, "Topic_Configuration") != null) {
					throw new ArchitectureConsistencyException("The connector \"" + XadlUtils.getDescription(xarch, connectorRef) + "\" has the properties Topic_Configuration and Queue_Configuration set, but only one of them is allowed at the same connector.");
				}
			}
			else {
				String message = "The Connector \"" + XadlUtils.getDescription(xarch, connectorRef) + "\" doesn't have an implementation!";
				ArchitectureWarning architectureWarning = new ArchitectureWarning();
				boolean shouldCancle = architectureWarning.showArchitectureWarning(message);
				if (shouldCancle) {
					throw new ArchitectureConsistencyException(message);
				}
			}
			
		}

		return true;
	}

}
