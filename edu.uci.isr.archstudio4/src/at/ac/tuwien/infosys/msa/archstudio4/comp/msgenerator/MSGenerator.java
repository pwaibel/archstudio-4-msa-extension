package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import at.ac.tuwien.infosys.msa.archstudio4.comp.MessaginSystemGenerationException;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.MSAInterfaceDirection;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem.IMessagingSystem;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem.MuleActiveMqSystem;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.component.XadlInterfaceConfiguration;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.workflow.ConnectionEndpoint;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.workflow.IEsbWorkflow;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.workflow.MuleWorkflow;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.transformation.ComponentData;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.transformation.ComponentTransformation;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.transformation.ConnectorTransformation;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.transformation.ConnectorTypeTransformation;
import edu.uci.isr.xarchflat.ObjRef;
import edu.uci.isr.xarchflat.XArchFlatInterface;

public class MSGenerator implements IMSGenerator {

	private XArchFlatInterface xarch;

	private ComponentTransformation componentTransformation;
	private ConnectorTypeTransformation connectorTypeTransformation;
	private ConnectorTransformation connectorTransformation;

	public MSGenerator(XArchFlatInterface xarch) {
		this.xarch = xarch;
	}

	@Override
	public void generateMessagingSystem(String name, ObjRef xArchRef, ObjRef structureRef) throws MessaginSystemGenerationException {

		componentTransformation = new ComponentTransformation(xarch);
		connectorTypeTransformation = new ConnectorTypeTransformation(xarch);
		connectorTransformation = new ConnectorTransformation(xarch);
		
		// 0. Transform Connector 
		connectorTransformation.transformConnector(structureRef);
		
		// 1. Generate for each ConnectorType a connector configuration (queue/topic name, broker url)
		connectorTypeTransformation.transformConnectors(structureRef);

		// 2. Generate map from Component to Connectors, include also direction from Component to Connector (Read,Write,ReadWrite)
		componentTransformation.transformComponents(structureRef, connectorTypeTransformation.getConnectorMap(), connectorTransformation.getConnectorToChannelMap(), connectorTransformation.getComponentInterfaceToChannelMap());

		// 3. Generate EsbWorkflow for each Component
		List<IEsbWorkflow> workflows = new ArrayList<IEsbWorkflow>();
		for (Map.Entry<ObjRef, ComponentData> compObjComponentEntry : componentTransformation.getComponentMap().entrySet()) {
			List<ConnectionEndpoint> interfaceList = compObjComponentEntry.getValue().getConnectionEndpoints();			
			List<ConnectionEndpoint> orderedInterface;
			if (compObjComponentEntry.getValue().getConnectionEndpointsOrder().size() > 0) {
				orderedInterface = orderInterfaceManual(interfaceList, compObjComponentEntry.getValue().getConnectionEndpointsOrder());
			} else {
				orderedInterface = orderInterfaces(interfaceList);
			}
			workflows.add(new MuleWorkflow(orderedInterface, compObjComponentEntry.getValue().getXadlComponentConfig()));
		}
		
		IMessagingSystem messagingSystem = new MuleActiveMqSystem();
		messagingSystem.setEsbWorkflows(workflows);
		messagingSystem.setConnectorConnectorConfigMap(connectorTypeTransformation.getConnectorMap());
		messagingSystem.generateXmlFile();

	}

	private List<ConnectionEndpoint> orderInterfaces(List<ConnectionEndpoint> interfaceList) {

		List<ConnectionEndpoint> orderedList = new ArrayList<ConnectionEndpoint>();

		// 1. Find IN Interface
		boolean found = false;
		for (ConnectionEndpoint currentInterface : interfaceList) {
			if(!currentInterface.getInterfaceConfig().getMuleInterfaceConfiguration().isRequestReplyEndpoint()) {
				if (currentInterface.getInterfaceConfig().getMuleInterfaceConfiguration().getDirection() == MSAInterfaceDirection.IN) {
					orderedList.add(currentInterface);
					found = true;
				}
			}
		}

		if (!found) {
			// 2. search for INOUT
			for (ConnectionEndpoint currentInterface : interfaceList) {
				if (currentInterface.getInterfaceConfig().getMuleInterfaceConfiguration().getDirection() == MSAInterfaceDirection.INOUT) {
					orderedList.add(currentInterface);
				}
			}
		}

		// 3. find OUT (if there exists one)
		ConnectionEndpoint outInterface = null;
		for (ConnectionEndpoint currentInterface : interfaceList) {
			if (!currentInterface.getInterfaceConfig().getMuleInterfaceConfiguration().isRequestReplyEndpoint()) {
				if (currentInterface.getInterfaceConfig().getMuleInterfaceConfiguration().getDirection() == MSAInterfaceDirection.OUT) {
					outInterface = currentInterface;
				}
			}
		}

		// 4. insert remaining interfaces
		for (ConnectionEndpoint currentInterface : interfaceList) {
			if (!orderedList.contains(currentInterface)) {
				if (outInterface == null || (outInterface != null && currentInterface != outInterface)) {
					
					orderedList.add(currentInterface);
					
					if(currentInterface.getInterfaceConfig().getMuleInterfaceConfiguration().isRequestReplyEndpoint()) {
						
						for (ConnectionEndpoint searchSecondRequstReply : interfaceList) {
							if(currentInterface != searchSecondRequstReply && searchSecondRequstReply.getInterfaceConfig().getMuleInterfaceConfiguration().isRequestReplyEndpoint()) {
								if(searchSecondRequstReply.getInterfaceConfig().getMuleInterfaceConfiguration().getRequestReplyEndpointId().equals(currentInterface.getInterfaceConfig().getMuleInterfaceConfiguration().getRequestReplyEndpointId())) {
									orderedList.add(searchSecondRequstReply);
								}
							}
						}
					}
					
				}
			}
		}

		// 5. add OUT interface at the end
		if (outInterface != null) {
			orderedList.add(outInterface);
		}

		return orderedList;
	}
	
	

	private List<ConnectionEndpoint> orderInterfaceManual(List<ConnectionEndpoint> endpointList, Map<ConnectionEndpoint, Integer> manuallyOrdered) {

		List<ConnectionEndpoint> orderedInterface = new ArrayList<ConnectionEndpoint>();
		
		int numberOfEndpoints = endpointList.size();
		
		// if it is a request-reply endpoint it should be counted as one endpoint
		int numberOfRequestReplyEndpoints = 0;
		for(ConnectionEndpoint currentEndpoint : endpointList) {
			if(currentEndpoint.getInterfaceConfig().getMuleInterfaceConfiguration().isRequestReplyEndpoint()) {
				numberOfRequestReplyEndpoints++;
			}
		}
		numberOfEndpoints = numberOfEndpoints - numberOfRequestReplyEndpoints/2;
		
		if (numberOfEndpoints == manuallyOrdered.size()) {

			List<Entry<ConnectionEndpoint, Integer>> list = new LinkedList<Entry<ConnectionEndpoint, Integer>>(manuallyOrdered.entrySet());

			Collections.sort(list, new Comparator<Entry<ConnectionEndpoint, Integer>>() {
				public int compare(Entry<ConnectionEndpoint, Integer> value1, Entry<ConnectionEndpoint, Integer> value2) {
					return ((Comparable<Integer>) value1.getValue()).compareTo(value2.getValue());
				}
			});

			for(Entry<ConnectionEndpoint, Integer> entry : list) {
				ConnectionEndpoint currentEndpoint = entry.getKey();
				orderedInterface.add(currentEndpoint);
				
				if(currentEndpoint.getInterfaceConfig().getMuleInterfaceConfiguration().isRequestReplyEndpoint()) {
					for(ConnectionEndpoint requestReplyEndpoint : endpointList) {
						XadlInterfaceConfiguration interfaceConfig = requestReplyEndpoint.getInterfaceConfig();
						if(requestReplyEndpoint != currentEndpoint && interfaceConfig.getMuleInterfaceConfiguration().isRequestReplyEndpoint() &&
								interfaceConfig.getMuleInterfaceConfiguration().getRequestReplyEndpointId().equals(currentEndpoint.getInterfaceConfig().getMuleInterfaceConfiguration().getRequestReplyEndpointId())) {
							orderedInterface.add(requestReplyEndpoint);
						}
					}
				}
				
			}
		} else {
			orderedInterface = orderInterfaces(endpointList);
		}

		return orderedInterface;
	}

}
