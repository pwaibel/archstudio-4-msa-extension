package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.component;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.MSAInterfaceDirection;

public class MuleInterfaceConfiguration implements IMuleInterfaceConfiguration {

	private MSAInterfaceDirection direction;
	private String durableName = "";
	private String replyChannelName = "";
	private String requestReplyEndpointId = "";		// Used to map request interface with reply interface
	private boolean requestReplyEndpoint = false;	// is true if the interface is part of a request-reply pattern
	private boolean generateInteface = true;		// the interface won't be generated if set to false
	
	@Override
	public MSAInterfaceDirection getDirection() {
		return direction;
	}

	public void setDirection(MSAInterfaceDirection direction) {
		this.direction = direction;
	}

	@Override
	public String getReplyChannelName() {
		return replyChannelName;
	}

	public void setReplyChannelName(String replyChannelName) {
		this.replyChannelName = replyChannelName;
	}

	@Override
	public String getDurableName() {
		return durableName;
	}

	public void setDurableName(String durableName) {
		this.durableName = durableName;
	}

	@Override
	public boolean isGenerateInteface() {
		return generateInteface;
	}

	public void setGenerateInteface(boolean generateInteface) {
		this.generateInteface = generateInteface;
	}

	@Override
	public String getRequestReplyEndpointId() {
		return requestReplyEndpointId;
	}

	public void setRequestReplyEndpointId(String replyToEndpointId) {
		if(replyToEndpointId == null || replyToEndpointId.isEmpty()) {
			this.requestReplyEndpoint = false;
		}
		else {
			this.requestReplyEndpoint = true;
		}
		this.requestReplyEndpointId = replyToEndpointId;
	}

	@Override
	public boolean isRequestReplyEndpoint() {
		return requestReplyEndpoint;
	}

}
