package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.channel;

public abstract class AbstractJmsChannel implements IJmsChannel {

	private String name;
	
	public AbstractJmsChannel(String name) {
		super();
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	
}
