package at.ac.tuwien.infosys.msa.archstudio4.comp.msglauncher;

import org.eclipse.swt.graphics.Image;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.IMSACC;
import edu.uci.isr.archstudio4.editors.AbstractArchstudioEditorMyxComponent;
import edu.uci.isr.archstudio4.launcher.ILaunchData;
import edu.uci.isr.archstudio4.launcher.LaunchData;
import edu.uci.isr.myx.fw.IMyxName;
import edu.uci.isr.myx.fw.MyxUtils;

public class MSGLauncherMyxComponent extends AbstractArchstudioEditorMyxComponent{

	public static final String EDITOR_NAME = "MSG Launcher";
	public static final String ECLIPSE_EDITOR_ID = "at.tuwien.msa.archstudio4.comp.msglauncher.MSGLauncherEditor.MSGLauncherEditor";

	public static final String IMAGE_MSAGENERATOR_ICON = "msagenerator:icon";
	public static final String URL_MSAGENERATOR_ICON = "res/msagenerator-icon-32.gif";
	public static final String IMAGE_MSAGENERATOR_GO_ICON = "msagenerator:go";
	public static final String URL_MSAGENERATOR_GO_ICON = "res/icon-go.gif";
	public static final String IMAGE_MSAGENERATOR_STOP_ICON = "msagenerator:stop";
	public static final String URL_MSAGENERATOR_STOP_ICON = "res/icon-stop.gif";

	public static final IMyxName INTERFACE_NAME_OUT_MSACC = MyxUtils.createName("msacc");
	
	protected IMSACC msacc = null;
	
	public MSGLauncherMyxComponent(){
		super(EDITOR_NAME, ECLIPSE_EDITOR_ID, true);
	}
	
	public void begin(){
		super.begin();
		msacc = (IMSACC)MyxUtils.getFirstRequiredServiceObject(this, INTERFACE_NAME_OUT_MSACC);
		resources.createImage(IMAGE_MSAGENERATOR_ICON, MSGLauncherMyxComponent.class.getResourceAsStream(URL_MSAGENERATOR_ICON));
		resources.createImage(IMAGE_MSAGENERATOR_GO_ICON, MSGLauncherMyxComponent.class.getResourceAsStream(URL_MSAGENERATOR_GO_ICON));
		resources.createImage(IMAGE_MSAGENERATOR_STOP_ICON, MSGLauncherMyxComponent.class.getResourceAsStream(URL_MSAGENERATOR_STOP_ICON));
	}
	
	public ILaunchData getLaunchData(){
		return new LaunchData(ECLIPSE_EDITOR_ID, EDITOR_NAME, 
			"Messaging Systems Generator", getIcon(), 
			ILaunchData.EDITOR);
	}
	
	public Image getIcon(){
		return resources.getImage(IMAGE_MSAGENERATOR_ICON);
	}

	public IMSACC getMSACC(){
		return msacc;
	}
}
