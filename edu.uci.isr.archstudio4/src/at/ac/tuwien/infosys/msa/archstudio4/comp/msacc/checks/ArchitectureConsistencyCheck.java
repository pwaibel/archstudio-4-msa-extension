package at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.checks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import at.ac.tuwien.infosys.msa.archstudio4.comp.GeneralHelper;
import edu.uci.isr.xadlutils.XadlUtils;
import edu.uci.isr.xarchflat.ObjRef;
import edu.uci.isr.xarchflat.XArchFlatInterface;

/**
 * Architecture Consistency Checks: 
 * Errors:
 * <ul>
 * <li>Check if a Connector is directly connected to another Connector.</li>
 * <li>Check if a Component is directly connected to another Component.</li>
 * <li>Check if each link is connected to another element.</li>
 * </ul>
 * 
 * Warnings:
 * <ul>
 * <li>Check if 2 Connectors have the same channel name.</li>
 * <li>Check a topic has an INOUT-Interface.</li>
 * <li>Check if a OUT-Interface in a Queue has several links.</li>
 * <li>Check if a Component has more than one link at an interface.</li>
 * <li>Check if an OUT-Interface, with Reply_To_Queue property, is connected to an IN-interface, with Reply_To_Queue property.</li>
 * <li>Check if a topic is used for request-reply and has several links at the out interface.</li>
 * </ul>
 * 
 * @author philippwaibel
 * 
 */
public class ArchitectureConsistencyCheck extends AbstractConsistencyCheck {

	private Map<ObjRef, List<ObjRef>> interfaceRef_link_Map = new HashMap<ObjRef, List<ObjRef>>();

	/**
	 * To avoid multiple warnings save all references into HashSets and print
	 * the warnings at the end
	 */
	private Set<ObjRef> containsTopicInout = new HashSet<ObjRef>();
	private Set<ObjRef> containsQueueWithMoreOut = new HashSet<ObjRef>();
	private Set<ObjRef> containsComponentWithMoreOut = new HashSet<ObjRef>();
	private Set<ObjRef> containsTopicWithMoreOut = new HashSet<ObjRef>();
	private Map<String, String> containsSameChannelName = new HashMap<String, String>();

	private Map<String, String> channelName_ConnectorName_Map = new HashMap<String, String>();
	private Map<ObjRef, ObjRef> connectorRef_interfaceRef_Map = new HashMap<ObjRef, ObjRef>();
	
	private ObjRef structureRef;

	public ArchitectureConsistencyCheck(XArchFlatInterface xarch) {
		super(xarch);
	}

	@Override
	public boolean check(Set<ObjRef> componentRefs, Set<ObjRef> connectorRefs, ObjRef structureRef) throws ArchitectureConsistencyException {

		this.structureRef = structureRef;
		
		for (ObjRef linkRef : xarch.getAll(structureRef, "link")) {
			ObjRef[] pointRefs = xarch.getAll(linkRef, "point");

			boolean isRequestingComp = false;
			boolean isComponent = false;
			boolean isConnector = false;
			ObjRef connectorRef = null;
			ObjRef componentRef = null;
			ObjRef replyToQueueInterfaceRefSaver = null;

			for (int i = 0; i < pointRefs.length; i++) {

				ObjRef interfaceRef = XadlUtils.resolveXLink(xarch, pointRefs[i], "anchorOnInterface");

				if (!interfaceRef_link_Map.containsKey(interfaceRef)) {
					interfaceRef_link_Map.put(interfaceRef, new ArrayList<ObjRef>());
				}
				interfaceRef_link_Map.get(interfaceRef).add(linkRef);

				ObjRef parentBrickRef = xarch.getParent(interfaceRef);
				if (xarch.isInstanceOf(parentBrickRef, "types#Connector")) {

					if (isConnector) {
						throw new ArchitectureConsistencyException("The connector \"" + XadlUtils.getDescription(xarch, parentBrickRef) + "\" is connected direkt to another connector.");
					}
					isConnector = true;

					connectorRef = parentBrickRef;
					if (replyToQueueInterfaceRefSaver != null) {
						connectorRef_interfaceRef_Map.put(parentBrickRef, replyToQueueInterfaceRefSaver);
					}

					String direction = getDirectionAsString(xarch, interfaceRef);

					if (direction.equals("inout")) {
						inoutCheck(parentBrickRef);
					}

					checkChannelNameMultipleUsed(parentBrickRef);

					if (!componentRefs.contains(parentBrickRef) && !connectorRefs.contains(parentBrickRef)) {
						throw new ArchitectureConsistencyException("The link \"" + XadlUtils.getDescription(xarch, linkRef) + "\" points to a brick that is not in the structure \"" + XadlUtils.getDescription(xarch, structureRef) + "\".");
					}
				} else if (xarch.isInstanceOf(parentBrickRef, "types#Component")) {
					if (isComponent) {
						throw new ArchitectureConsistencyException("The component \"" + XadlUtils.getDescription(xarch, parentBrickRef) + "\" has a direct link to another component.");
					}
					isComponent = true;
					componentRef = parentBrickRef;

					isRequestingComp = checksForReplyToQueue(interfaceRef, connectorRef);
					if (connectorRef == null && isRequestingComp) {
						replyToQueueInterfaceRefSaver = interfaceRef;
					}
					

				}
			}
			
			if(isRequestingComp && !isRequestReplyUsedCorrect(connectorRef)) {
				throw new ArchitectureConsistencyException("The component \"" + XadlUtils.getDescription(xarch, componentRef) + "\" has an OUT-interface, with Reply_To_Queue property, that isn't connected to an IN-interface, which has the Reply_To_Queue property set.");
			}

		}

		for (Entry<String, String> entrySet : containsSameChannelName.entrySet()) {
			throw new ArchitectureConsistencyException("The connector \"" + entrySet.getKey() + "\" and the connector \"" + entrySet.getValue() + "\" have the same channel name.");
		}
		
		postCheck();
		
		for (ObjRef queueObj : containsComponentWithMoreOut) {
			throw new ArchitectureConsistencyException("The component \"" + XadlUtils.getDescription(xarch, queueObj) + "\" has an interface with several connected links.");
		}
		
		printWarnings();

		return true;
	}
	
	/**
	 * Check if an interface, with direction out and Reply_To_Queue property, is connected over a connector to an interface with direction IN and Reply_To_Queue property.  
	 * @param connectorRef
	 * @return
	 */
	private boolean isRequestReplyUsedCorrect(ObjRef connectorRef) {
		boolean interfaceImplementationWrong = false;
		boolean noInterfaceImplementation = true;
		boolean replyToQueueFound = false;
		
		ObjRef[] connInterfaceRefs = xarch.getAll(connectorRef, "interface");
		for (ObjRef connInterfaceRef : connInterfaceRefs) {
			String connInterfaceDirectionString = XadlUtils.getDirection(xarch, connInterfaceRef);
			if (connInterfaceDirectionString.equals("out")) {
				ObjRef[] linksRef = findConnectedLinks(connInterfaceRef);

				for (ObjRef linkRef : linksRef) {
					ObjRef[] pointRefs = xarch.getAll(linkRef, "point");

					for (int i = 0; i < pointRefs.length; i++) {

						ObjRef compInterfaceRef = XadlUtils.resolveXLink(xarch, pointRefs[i], "anchorOnInterface");

						ObjRef parentBrickRef = xarch.getParent(compInterfaceRef);
						if (xarch.isInstanceOf(parentBrickRef, "types#Component")) {
							
							if (xarch.isInstanceOf(compInterfaceRef, "endpointimplementation#InterfaceImpl")) {
								noInterfaceImplementation = false;
								
								ObjRef receiveImplementationRef = GeneralHelper.getEndpointImplementationRef(xarch, compInterfaceRef);
								if (receiveImplementationRef != null) {
									ObjRef queueRef = (ObjRef) XadlUtils.resolveXLink(xarch, receiveImplementationRef, "Connection_To_Request_Endpoint");
									if (queueRef != null) {
										interfaceImplementationWrong = true;
									}
									queueRef = (ObjRef) XadlUtils.resolveXLink(xarch, receiveImplementationRef, "Reply_To_Queue");
									if (queueRef != null) {
										replyToQueueFound = true;
									}
								}
							}	
							else {
								noInterfaceImplementation = true;
							}
						}
					}
				}
			}
		}
		if(interfaceImplementationWrong || noInterfaceImplementation) {
			return false;
		}
		
		return replyToQueueFound;
	}

	private ObjRef[] findConnectedLinks(ObjRef connInterfaceRef) {
		List<ObjRef> returnList = new ArrayList<ObjRef>();
		for (ObjRef linkRef : xarch.getAll(structureRef, "link")) {
			ObjRef[] pointRefs = xarch.getAll(linkRef, "point");

			for (int i = 0; i < pointRefs.length; i++) {
				ObjRef compInterfaceRef = XadlUtils.resolveXLink(xarch, pointRefs[i], "anchorOnInterface");
				if(connInterfaceRef == compInterfaceRef) {
					returnList.add(linkRef);
				}
			}
		}
		
		return  returnList.toArray(new ObjRef[returnList.size()]);
	}

	private boolean checksForReplyToQueue(ObjRef interfaceRef, ObjRef connectorRef) {

		if (xarch.isInstanceOf(interfaceRef, "endpointimplementation#InterfaceImpl")) {

			ObjRef endpointImplementationRef = GeneralHelper.getEndpointImplementationRef(xarch, interfaceRef);
			if (endpointImplementationRef != null) {
				ObjRef referredInterfaceRef = (ObjRef) XadlUtils.resolveXLink(xarch, endpointImplementationRef, "Reply_To_Queue");
				
				ObjRef directionRef = (ObjRef) xarch.get(interfaceRef, "direction");
				String directionString = (String) xarch.get(directionRef, "value");
				if (referredInterfaceRef != null && directionString.equals("out")) {
					if (connectorRef != null) {
						connectorRef_interfaceRef_Map.put(connectorRef, interfaceRef);
					}
					return true;
				}
			}
		}
		return false;
	}

	private void printWarnings() throws ArchitectureConsistencyException {
		for (ObjRef topicWithInout : containsTopicInout) {
			String message = "The topic, defined by the connector \"" + XadlUtils.getDescription(xarch, topicWithInout) + "\", has an INOUT-interface.";
			showArchitectureWarning(message);
		}

		for (ObjRef queueObj : containsQueueWithMoreOut) {
			String message = "The connector \"" + XadlUtils.getDescription(xarch, queueObj) + "\" has an OUT-interface with several connected links.";
			showArchitectureWarning(message);
		}

		for (ObjRef queueObj : containsTopicWithMoreOut) {
			String message = "The connector \"" + XadlUtils.getDescription(xarch, queueObj) + "\" is a topic and is used for Request-Reply. This could lead to several response messages because each subscribed component could response to it.";
			showArchitectureWarning(message);
		}
	}

	private void showArchitectureWarning(String message) throws ArchitectureConsistencyException {
		ArchitectureWarning architectureWarning = new ArchitectureWarning();
		boolean shouldCancel = architectureWarning.showArchitectureWarning(message);
		if (shouldCancel) {
			throw new ArchitectureConsistencyException(message, false);
		}
	}

	/**
	 * - Check if a queue has several links at one interface 
	 * - Check if a topic is used for request-reply and has several links at the out interface 
	 * - Check if a component has more than one link at an interface
	 */
	private void postCheck() {

		for (Entry<ObjRef, List<ObjRef>> entrySet : interfaceRef_link_Map.entrySet()) {

			ObjRef interfaceRef = entrySet.getKey();
			ObjRef parentBrickRef = xarch.getParent(interfaceRef);
			if (xarch.isInstanceOf(parentBrickRef, "types#Connector")) {
				ObjRef channelImplementationRef = GeneralHelper.getChannelImplementationRef(xarch, parentBrickRef);
				if (channelImplementationRef != null) {
					ObjRef queueConfig = (ObjRef) xarch.get(channelImplementationRef, "Queue_Configuration");
					if (queueConfig != null) {
						if (entrySet.getValue().size() > 1) {
							String direction = getDirectionAsString(xarch, interfaceRef);
							if (direction.equals("out")) {
								containsQueueWithMoreOut.add(parentBrickRef);
							}
						}
					} else {
						ObjRef topicConfig = (ObjRef) xarch.get(channelImplementationRef, "Topic_Configuration");
						if (topicConfig != null) {
							if (connectorRef_interfaceRef_Map.containsKey(parentBrickRef)) {
								containsTopicWithMoreOut.add(parentBrickRef);
							}
						}
					}
				}
			} else if (xarch.isInstanceOf(parentBrickRef, "types#Component")) {
				if (entrySet.getValue().size() > 1) {
					containsComponentWithMoreOut.add(parentBrickRef);
				}
			}
		}

	}

	/**
	 * Check if there is a topic with INOUT and save the reference for the print
	 * warning method.
	 * 
	 * @param parentBrickRef
	 */
	private void inoutCheck(ObjRef parentBrickRef) {
		ObjRef channelImplementationRef = GeneralHelper.getChannelImplementationRef(xarch, parentBrickRef);
		if (channelImplementationRef != null) {
			ObjRef topicConfig = (ObjRef) xarch.get(channelImplementationRef, "Topic_Configuration");
			if (topicConfig != null) {
				containsTopicInout.add(parentBrickRef);
			}
		}
	}

	/**
	 * Check if the channel names are used multiple times and save the
	 * references
	 * 
	 * @param parentBrickRef
	 */
	private void checkChannelNameMultipleUsed(ObjRef parentBrickRef) {
		ObjRef channelImplementationRef = GeneralHelper.getChannelImplementationRef(xarch, parentBrickRef);
		if (channelImplementationRef != null) {

			String channelName = "";

			ObjRef topicConfig = (ObjRef) xarch.get(channelImplementationRef, "Topic_Configuration");
			if (topicConfig != null) {
				channelName = (String) xarch.get(topicConfig, "name");
			} else {
				ObjRef queueConfig = (ObjRef) xarch.get(channelImplementationRef, "Queue_Configuration");
				if (queueConfig != null) {
					channelName = (String) xarch.get(queueConfig, "name");
				}
			}

			if (!channelName.isEmpty()) {
				ObjRef descriptionRef = (ObjRef) xarch.get(parentBrickRef, "description");
				String name = (String) xarch.get(descriptionRef, "value");
				if (channelName_ConnectorName_Map.containsKey(channelName) && !name.equals(channelName_ConnectorName_Map.get(channelName))) {
					containsSameChannelName.put(channelName_ConnectorName_Map.get(channelName), name);
				} 
				channelName_ConnectorName_Map.put(channelName, name);
			}
		}
	}

}
