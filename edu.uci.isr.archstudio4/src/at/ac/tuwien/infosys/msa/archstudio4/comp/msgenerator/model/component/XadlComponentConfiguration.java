package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.component;

import edu.uci.isr.xarchflat.ObjRef;

/**
 * This class represent the common component informations from the XADL file
 * @author philippwaibel
 *
 */
public class XadlComponentConfiguration {

	private IComponentConfig componentConfiguration;
	private ObjRef componentRefList;
	private String archStudioId;
	private String fileId = "";
	
	public XadlComponentConfiguration() {
		super();
	}
	public XadlComponentConfiguration(ObjRef componentRefList, IComponentConfig componentConfiguration, String archStudioId, String fileId) {
		super();
		this.componentConfiguration = componentConfiguration;
		this.componentRefList = componentRefList;
		this.archStudioId = archStudioId;
		this.fileId = fileId;
	}
	
	public IComponentConfig getComponentConfiguration() {
		return componentConfiguration;
	}
	public void setComponentConfiguration(IComponentConfig componentConfiguration) {
		this.componentConfiguration = componentConfiguration;
	}
	public ObjRef getComponentRefList() {
		return componentRefList;
	}
	public void setComponentRefList(ObjRef componentRefList) {
		this.componentRefList = componentRefList;
	}
	public String getArchStudioId() {
		return archStudioId;
	}
	public void setArchStudioId(String archStudioId) {
		this.archStudioId = archStudioId;
	}
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	
	
	
}
