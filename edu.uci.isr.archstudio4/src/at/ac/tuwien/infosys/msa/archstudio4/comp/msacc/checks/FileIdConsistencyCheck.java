package at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.checks;

import java.util.List;
import java.util.Set;

import at.ac.tuwien.infosys.msa.archstudio4.comp.GeneralHelper;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msglauncher.FilesInMemoryStore;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msglauncher.config.model.OutputFileType;
import edu.uci.isr.xadlutils.XadlUtils;
import edu.uci.isr.xarchflat.ObjRef;
import edu.uci.isr.xarchflat.XArchFlatInterface;

/**
 * File Id Consistency Checks:
 * Errors:
 * <ul>
 * 	<li>Check if all Connector and Component file ids are set.</li>
 *  <li>Check if the file id that is set in the Connector or Component is available.</li>
 *  <li>Check if the file id has a file path.</li>
 * </ul>
 * 
 * Warnings:

 * @author philippwaibel
 *
 */
public class FileIdConsistencyCheck extends AbstractConsistencyCheck {


	public FileIdConsistencyCheck(XArchFlatInterface xarch) {
		super(xarch);
	}

	@Override
	public boolean check(Set<ObjRef> componentRefs, Set<ObjRef> connectorRefs, ObjRef structureRef) throws ArchitectureConsistencyException {

		FilesInMemoryStore filesInMemory = FilesInMemoryStore.getInstance();

		List<OutputFileType> muleFileList = filesInMemory.getMuleFileList();
		List<OutputFileType> jmsFileList = filesInMemory.getJmsFileList();

		for (ObjRef connectorBrickRef : connectorRefs) {
			ObjRef connectorTypeRef = XadlUtils.resolveXLink(xarch, connectorBrickRef, "type");

			ObjRef jmsImplementationRef = GeneralHelper.getJmsImplementationRef(xarch, connectorTypeRef);
			if (jmsImplementationRef != null) {
				String fileId = (String) xarch.get(jmsImplementationRef, "file_id");

				if (fileId == null || fileId.isEmpty()) {
					throw new ArchitectureConsistencyException("The File-ID of the connector \"" + XadlUtils.getDescription(xarch, connectorBrickRef) + "\" is empty!");
				}
				searchFileIdInConfig(jmsFileList, fileId);

			}
		}

		for (ObjRef componentBrickRef : componentRefs) {
			ObjRef muleImplementationRef = GeneralHelper.getMuleImplementationRef(xarch, componentBrickRef);
			if (muleImplementationRef != null) {

				String fileId = (String) xarch.get(muleImplementationRef, "file_id");

				if (fileId == null || fileId.isEmpty()) {
					throw new ArchitectureConsistencyException("The File-ID of the component \"" + XadlUtils.getDescription(xarch, componentBrickRef) + "\" is empty!");
				}

				searchFileIdInConfig(muleFileList, fileId);

			}
		}

		return true;
	}

	private void searchFileIdInConfig(List<OutputFileType> fileList, String fileId) throws ArchitectureConsistencyException {

		boolean found = false;
		for (OutputFileType tempFile : fileList) {
			if (fileId.equals(tempFile.getId())) {
				if (tempFile.getFilepath().isEmpty()) {
					throw new ArchitectureConsistencyException("There is no Filepath for the File-ID \"" + tempFile.getId() + "\" defined!");
				}
				found = true;
			}
		}

		if (!found) {
			throw new ArchitectureConsistencyException("The File-ID \"" + fileId + "\" could not be found in the output configuration file!");
		}
	}

}
