package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.transformation;

import java.util.Map;

import at.ac.tuwien.infosys.msa.archstudio4.comp.GeneralHelper;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.MSAInterfaceDirection;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.channel.IJmsChannel;
import edu.uci.isr.xadlutils.XadlUtils;
import edu.uci.isr.xarchflat.ObjRef;
import edu.uci.isr.xarchflat.XArchFlatInterface;

public class InterfaceTransformation extends AbstractTransformation {

	private ObjRef requestEndpoint = null;
	private Integer positionNo = null;
	private String replyChannelName = "";
	private String durableName = "";
	private ObjRef interfaceRef = null;
	private MSAInterfaceDirection direction = null;
	private boolean generateInterface = false;

	public InterfaceTransformation(XArchFlatInterface xarch) {
		super(xarch);
	}

	public void process(ObjRef interfaceRef, Map<ObjRef, IJmsChannel> componentInterface_to_ChannelMap) {
		this.interfaceRef = interfaceRef;
		
		if (xarch.isInstanceOf(interfaceRef, "endpointimplementation#InterfaceImpl")) {
			ObjRef endpointImplementationRef = GeneralHelper.getEndpointImplementationRef(xarch, interfaceRef);
			if (endpointImplementationRef != null) {
		
				generateInterface = true;
				
				setDirection(interfaceRef);
				
				setReplyTo(endpointImplementationRef, interfaceRef, componentInterface_to_ChannelMap);

				setPositionNo(endpointImplementationRef);

				setDurableName(endpointImplementationRef);

			}
		}
	}

	private void setDirection(ObjRef interfaceRef) {
		ObjRef directionRef = (ObjRef) xarch.get(interfaceRef, "direction");
		String directionString = (String) xarch.get(directionRef, "value");

		if (directionString.equals("in")) {
			direction = MSAInterfaceDirection.IN;
		} else if (directionString.equals("out")) {
			direction = MSAInterfaceDirection.OUT;
		} else if (directionString.equals("inout")) {
			direction = MSAInterfaceDirection.INOUT;
		}
	}

	private void setReplyTo(ObjRef endpointImplementationRef, ObjRef interfaceRef, Map<ObjRef, IJmsChannel> componentInterface_to_ChannelMap) {

		ObjRef replyInterfaceRef = (ObjRef) XadlUtils.resolveXLink(xarch, endpointImplementationRef, "Reply_To_Queue");

		if (replyInterfaceRef != null) {
			if (direction == MSAInterfaceDirection.IN) {
				direction = MSAInterfaceDirection.INOUT;
			} else if (direction == MSAInterfaceDirection.OUT) {
				String name = componentInterface_to_ChannelMap.get(replyInterfaceRef).getName();
				replyChannelName = name;
			}
		}

		requestEndpoint = (ObjRef) XadlUtils.resolveXLink(xarch, endpointImplementationRef, "Connection_To_Request_Endpoint");
		
		if (requestEndpoint != null && direction == MSAInterfaceDirection.OUT) {
			generateInterface = false;
			requestEndpoint = null;
		}
	}

	private void setPositionNo(ObjRef endpointImplementationRef) {
		ObjRef endpointRef = (ObjRef) xarch.get(endpointImplementationRef, "Endpoint_Position_No");
		if (endpointRef != null) {
			String positionString = (String) xarch.get(endpointRef, "value");
			if (positionString != null) {
				positionNo = new Integer(positionString);
			}
		}
	}

	private void setDurableName(ObjRef endpointImplementationRef) {
		ObjRef durableRef = (ObjRef) xarch.get(endpointImplementationRef, "Durable_Name");
		if (durableRef != null) {
			String durableName = (String) xarch.get(durableRef, "name");
			if (durableName != null) {
				this.durableName = durableName;
			}
		}
	}

	public ObjRef getRequestEndpoint() {
		return requestEndpoint;
	}

	public Integer getOrderNo() {
		return positionNo;
	}

	public String getReplyChannelName() {
		return replyChannelName;
	}
	
	public boolean hasReplyChannelName() {
		return replyChannelName != null && !replyChannelName.isEmpty();
	}

	public String getDurableName() {
		return durableName;
	}

	public ObjRef getInterfaceRef() {
		return interfaceRef;
	}

	public MSAInterfaceDirection getDirection() {
		return direction;
	}

	public boolean isGenerateInterface() {
		return generateInterface;
	}

	
}
