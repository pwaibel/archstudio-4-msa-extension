package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector;

import java.util.HashMap;
import java.util.Map;

public class PersistenceAdditionalConfig {

	private String directory;
	private String adapterName;
	private Map<String, String> additionalProperties = new HashMap<String, String>();
	
	public PersistenceAdditionalConfig(String directory, String adapterName) {
		this.directory = directory;
		this.adapterName = adapterName;
	}

	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	public String getAdapterName() {
		return adapterName;
	}

	public void setAdapterName(String adapterName) {
		this.adapterName = adapterName;
	}

	public Map<String, String> getAdditionalProperties() {
		return additionalProperties;
	}

	public void setAdditionalProperties(Map<String, String> persistenceProperties) {
		this.additionalProperties = persistenceProperties;
	}
	
	public void addAdditionalProperties(String key, String value) {
		this.additionalProperties.put(key, value);
	}
	
}
