package at.ac.tuwien.infosys.msa.archstudio4.comp.msglauncher.gui;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msglauncher.config.model.OutputFileType;

public abstract class CellIdEditingSupport extends EditingSupport {

	protected final ColumnViewer viewer;
	private TextCellEditor cellEditor;

	public CellIdEditingSupport(ColumnViewer viewer) {
		super(viewer);
		this.viewer = viewer;
		cellEditor = new TextCellEditor(((TableViewer) viewer).getTable());
	}

	@Override
	protected CellEditor getCellEditor(Object element) {
		return cellEditor;
	}

	@Override
	protected boolean canEdit(Object element) {
		return true;
	}
	
	@Override
	protected Object getValue(Object element) {
		return ((OutputFileType) element).getId();
	}

}