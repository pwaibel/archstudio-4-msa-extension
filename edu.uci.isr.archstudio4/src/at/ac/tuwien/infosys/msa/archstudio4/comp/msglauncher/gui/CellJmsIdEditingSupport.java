package at.ac.tuwien.infosys.msa.archstudio4.comp.msglauncher.gui;

import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.widgets.Shell;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msglauncher.FilesInMemoryStore;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msglauncher.config.model.OutputFileType;

public class CellJmsIdEditingSupport extends CellIdEditingSupport {

	public CellJmsIdEditingSupport(ColumnViewer viewer) {
		super(viewer);
	}

	@Override
	protected void setValue(Object element, Object userInputValue) {
		
		String id = String.valueOf(userInputValue);
		String oldId = ((OutputFileType) element).getId();
		List<OutputFileType> outputFiles = FilesInMemoryStore.getInstance().getJmsFileList();
		if (!id.equals(oldId)) {
			if (FilesInMemoryStore.getInstance().isIdUniq(id, outputFiles)) {
				((OutputFileType) element).setId(id);
				viewer.update(element, null);
			} else {
				Shell shell = ((TableViewer) viewer).getTable().getShell();

				MessageDialog dialog = new MessageDialog(shell, "Unique Id", null, "The File ID has to be unique!", MessageDialog.ERROR, new String[] { "Ok" }, 0);
				dialog.open();
			}
		}

	}

}