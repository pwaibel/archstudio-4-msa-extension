package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.component;

import java.util.ArrayList;
import java.util.List;


public class MuleComponentConfig implements IComponentConfig {

	private String flowname = "";

	private List<AdditionalConfig> additionalConfigs = new ArrayList<AdditionalConfig>();
	
	
	public MuleComponentConfig(String flowname) {
		super();
		this.flowname = flowname;
	}

	public List<AdditionalConfig> getAdditionalConfigs() {
		return additionalConfigs;
	}

	public void setAdditionalConfigs(List<AdditionalConfig> additionalConfigs) {
		this.additionalConfigs = additionalConfigs;
	}
	
	public void addAdditionalConfigs(AdditionalConfig additionalConfig) {
		if(this.additionalConfigs == null) {
			this.additionalConfigs = new ArrayList<AdditionalConfig>();
		}
		this.additionalConfigs.add(additionalConfig);
	}

	public String getFlowname() {
		return flowname;
	}
	
}