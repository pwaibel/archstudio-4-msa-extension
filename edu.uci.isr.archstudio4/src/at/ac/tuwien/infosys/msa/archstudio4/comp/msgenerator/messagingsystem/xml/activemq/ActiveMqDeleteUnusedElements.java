package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem.xml.activemq;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem.xml.mule.update.MuleUpdateRemoveQuestion;

public class ActiveMqDeleteUnusedElements {
	private Map<String, Node> unusedPersistenceElements = new HashMap<String, Node>();
	private Map<String, Node> unusedTransportElements = new HashMap<String, Node>();

	private Document doc;
	
	public ActiveMqDeleteUnusedElements(Document doc) {
		this.doc = doc;
	}
	
	
	public void deleteUnusedElements(List<String> usedPersistenceArchStudioIds, List<String> usedTransportArchStudioIds) {
		
		List<Node> allComments = findAllArchStudioIdComments(doc.getDocumentElement());
		

		
		for(Node commentElement : allComments) {
			Comment comment= (Comment) commentElement;
			String commentString = comment.getTextContent();
			
			if(commentString.contains("ArchStudioId=")) {
				String id = commentString.split("ArchStudioId=")[1];
				id = id.split(" ")[0].trim();
				
				if(isPersistenceComment(commentElement) && !usedPersistenceArchStudioIds.contains(id)) {					
					unusedPersistenceElements.put(id, commentElement);
					
				}
				else if(isTransportComment(commentElement) && !usedTransportArchStudioIds.contains(id)) {
					unusedTransportElements.put(id, commentElement);
				}
				
			}
		}
		
		deleteTransportElements();
		
		deletePersistenceElements();
		
	}
	
	private boolean isPersistenceComment(Node commentElement) {
		Node element = commentElement.getParentNode().getParentNode();
		return element.getNodeName().equals("persistenceAdapter");
	}
	
	private boolean isTransportComment(Node commentElement) {
		Node element = commentElement.getParentNode();
		return element.getNodeName().contains("transportConnector");
	}
	

	private void deleteTransportElements() {
		if(unusedTransportElements.size() > 0)
		{
			MuleUpdateRemoveQuestion removeQuestion = new MuleUpdateRemoveQuestion();
			for(Entry<String, Node> unusedElement : unusedTransportElements.entrySet()) {
				String message = "It seems that the TransportConnector with the ArchStudio Id " + unusedElement.getKey() + " was used previously but now it isn't. Should the Mule Flow be removed or only the referenc to ArchStudio?";
				removeQuestion.showRemoveQuestion(message);
				
				if(removeQuestion.shouldDeleteElement()) {
					Node brokerElement = unusedElement.getValue().getParentNode().getParentNode();
					if(brokerElement.getNodeName().equals("transportConnectors")) {
						brokerElement.removeChild(unusedElement.getValue().getParentNode());
					}				
				}
				else if(removeQuestion.shouldDeleteReference()) {
					Node transportElement = unusedElement.getValue().getParentNode();
					if(transportElement.getNodeName().equals("transportConnector")) {
						transportElement.removeChild(unusedElement.getValue());
					}
				}
			}
		}
	}

	

	private void deletePersistenceElements() {
		if(unusedPersistenceElements.size() > 0)
		{
			MuleUpdateRemoveQuestion removeQuestion = new MuleUpdateRemoveQuestion();
			for(Entry<String, Node> unusedElement : unusedPersistenceElements.entrySet()) {
				String message = "It seems that the PersistenceAdapter with the ArchStudio Id " + unusedElement.getKey() + " was used previously but now it isn't. Should the Mule Flow be removed or only the referenc to ArchStudio?";
				removeQuestion.showRemoveQuestion(message);
				
				if(removeQuestion.shouldDeleteElement()) {
					Node persistenceAdapterElement = unusedElement.getValue().getParentNode().getParentNode();
					if(persistenceAdapterElement.getNodeName().equals("persistenceAdapter")) {
						persistenceAdapterElement.removeChild(unusedElement.getValue().getParentNode());
					}				
				}
				else if(removeQuestion.shouldDeleteReference()) {
					Node adapterNameElement = unusedElement.getValue().getParentNode();
					Node persistenceAdapterElement = adapterNameElement.getParentNode();
					if(persistenceAdapterElement.getNodeName().equals("persistenceAdapter")) {
						adapterNameElement.removeChild(unusedElement.getValue());
					}
				}
			}
		}
	}


	protected List<Node> findAllArchStudioIdComments(Node startPoint) {

		List<Node> allComments = new ArrayList<Node>();
		
		findAllArchStudioIdCommentsRecursive(startPoint, allComments);
	
		return allComments;
	}
	
	
	protected List<Node> findAllArchStudioIdCommentsRecursive(Node node, List<Node> allComments) {

	    NodeList nodeList = node.getChildNodes();
	    for (int i = 0; i < nodeList.getLength(); i++) {
	        Node currentNode = nodeList.item(i);
	        if (currentNode.getNodeType() == Node.COMMENT_NODE) {
	        	allComments.add(currentNode);
	        }
	        findAllArchStudioIdCommentsRecursive(currentNode, allComments);
	    }
	    
	    return allComments;
	}
	
}
