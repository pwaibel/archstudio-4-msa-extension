package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator;

import at.ac.tuwien.infosys.msa.archstudio4.comp.MessaginSystemGenerationException;
import edu.uci.isr.xarchflat.ObjRef;

public interface IMSGenerator {

	public void generateMessagingSystem(String name, ObjRef xArchRef, ObjRef structureRef) throws MessaginSystemGenerationException;
	
}
