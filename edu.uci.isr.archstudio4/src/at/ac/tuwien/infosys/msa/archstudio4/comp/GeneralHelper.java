package at.ac.tuwien.infosys.msa.archstudio4.comp;

import edu.uci.isr.xarchflat.InvalidOperationException;
import edu.uci.isr.xarchflat.ObjRef;
import edu.uci.isr.xarchflat.XArchFlatInterface;

public class GeneralHelper {

	public static ObjRef getJmsImplementationRef(XArchFlatInterface xarch, ObjRef brickRef) {
		try {
			for (ObjRef jmsImplementationRef : xarch.getAll(brickRef, "implementation")) {
				if (xarch.isInstanceOf(jmsImplementationRef, "jmsimplementation#JmsImplementation")) {
					return jmsImplementationRef;
				}
			}
		} catch (InvalidOperationException ex) {
			return null;
		}
		return null;
	}

	public static ObjRef getMuleImplementationRef(XArchFlatInterface xarch, ObjRef brickRef) {
		try {
			for (ObjRef muleImplementationRef : xarch.getAll(brickRef, "implementation")) {
				if (xarch.isInstanceOf(muleImplementationRef, "muleimplementation#MuleImplementation")) {
					return muleImplementationRef;
				}
			}
		} catch (InvalidOperationException ex) {
			return null;
		}
		return null;
	}

	public static ObjRef getEndpointImplementationRef(XArchFlatInterface xarch, ObjRef brickRef) {
		try {
			for (ObjRef endpointImplementationRef : xarch.getAll(brickRef, "implementation")) {
				if (xarch.isInstanceOf(endpointImplementationRef, "endpointimplementation#EndpointImplementation")) {
					return endpointImplementationRef;
				}
			}
		} catch (InvalidOperationException ex) {
			return null;
		}
		return null;
	}
	
	public static ObjRef getChannelImplementationRef(XArchFlatInterface xarch, ObjRef brickRef) {
		try {
			for (ObjRef channelImplementationRef : xarch.getAll(brickRef, "implementation")) {
				if (xarch.isInstanceOf(channelImplementationRef, "channelimplementation#ChannelImplementation")) {
					return channelImplementationRef;
				}
			}
		} catch (InvalidOperationException ex) {
			return null;
		}
		return null;
	}
}
