package at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.checks;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;

public class ArchitectureExceptionDialog implements Runnable {

	private boolean cancel;
	private String message;
	
	public boolean showArchitectureException(String message) {
		this.message = message;

		Display.getDefault().syncExec(this);

		return shouldCancel();

	}
	
	@Override
	public void run() {
		MessageDialog dialog = new MessageDialog(Display.getDefault().getActiveShell(), "Architecture Exception", null, message, MessageDialog.ERROR, new String[] { "OK"}, 0);
		int result = dialog.open();
		
		if(result == 1) {
			cancel = true;
		}
		
	}
	
	public boolean shouldCancel() {
		return cancel;
	}

}
