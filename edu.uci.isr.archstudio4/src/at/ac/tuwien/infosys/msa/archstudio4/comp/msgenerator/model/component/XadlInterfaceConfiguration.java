package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.component;


/**
 * This class represent the interface informations from the XADL file  
 * @author philippwaibel
 *
 */
public class XadlInterfaceConfiguration {
	private String archStudioId;
	private IMuleInterfaceConfiguration muleInterfaecConfiguration;
	
	public String getArchStudioId() {
		return archStudioId;
	}

	public void setArchStudioId(String archStudioId) {
		this.archStudioId = archStudioId;
	}

	public IMuleInterfaceConfiguration getMuleInterfaceConfiguration() {
		return muleInterfaecConfiguration;
	}

	public void setMuleInterfaceConfiguration(IMuleInterfaceConfiguration muleInterfaecConfiguration) {
		this.muleInterfaecConfiguration = muleInterfaecConfiguration;
	}

}