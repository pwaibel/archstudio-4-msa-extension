package at.ac.tuwien.infosys.msa.archstudio4.comp.msacc;

public enum MSAInterfaceDirection {
	IN, OUT, INOUT
}
