package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.transformation;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import at.ac.tuwien.infosys.msa.archstudio4.comp.GeneralHelper;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.channel.IJmsChannel;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.channel.QueueJmsChannel;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.channel.TopicJmsChannel;
import edu.uci.isr.xadlutils.XadlUtils;
import edu.uci.isr.xarchflat.ObjRef;
import edu.uci.isr.xarchflat.XArchFlatInterface;

public class ConnectorTransformation extends AbstractTransformation {

	private final static Logger LOGGER = Logger.getLogger(ConnectorTransformation.class.getName());
	
	private Map<ObjRef, IJmsChannel> connector_To_Channel_Map = new HashMap<ObjRef, IJmsChannel>();
	
	private Map<ObjRef, IJmsChannel> componentInterface_To_Channel_Map = new HashMap<ObjRef, IJmsChannel>();
	
	public ConnectorTransformation(XArchFlatInterface xarch) {		
		super(xarch);
	}
	
	public void transformConnector(ObjRef structureRef) {
		
		for (ObjRef linkRef : xarch.getAll(structureRef, "link")) {
			
			IJmsChannel jmsChannel = null;
			ObjRef componentInterfaceRef = null;
			
			for (ObjRef pointRef : xarch.getAll(linkRef, "point")) {
				ObjRef interfaceRef = XadlUtils.resolveXLink(xarch, pointRef, "anchorOnInterface");
				ObjRef parentBrickRef = xarch.getParent(interfaceRef);

				if (xarch.isInstanceOf(parentBrickRef, "types#Connector")) {
					jmsChannel = generateJmsChannel(parentBrickRef);
					connector_To_Channel_Map.put(parentBrickRef, jmsChannel);
				}
				
				if(xarch.isInstanceOf(parentBrickRef, "types#Component")) {
					componentInterfaceRef = interfaceRef;
				}
			}		
			if(jmsChannel != null && componentInterfaceRef != null) {
				componentInterface_To_Channel_Map.put(componentInterfaceRef, jmsChannel);
			}
	
		}
	}
	
	private IJmsChannel generateJmsChannel(ObjRef connectorBrickRef) {
		IJmsChannel jmsChannel = null;
		
		ObjRef connectorImplementationRef = GeneralHelper.getChannelImplementationRef(xarch, connectorBrickRef);
		if (connectorImplementationRef != null) {
			ObjRef queueConfig = (ObjRef) xarch.get(connectorImplementationRef, "Queue_Configuration");
			if (queueConfig != null) {
				jmsChannel = generateQueueJmsChannel(queueConfig);
			} else {
				ObjRef topicConfig = (ObjRef) xarch.get(connectorImplementationRef, "Topic_Configuration");
				if (topicConfig != null) {
					jmsChannel = generateTopicJmsChannel(topicConfig);
				}
			}
		}
		return jmsChannel;
	}

	private IJmsChannel generateQueueJmsChannel(ObjRef queueConfig) {
		String queueName = (String) xarch.get(queueConfig, "name");
		IJmsChannel jmsChannel = new QueueJmsChannel(queueName);
		LOGGER.fine("Queue Name: " + queueName);
		return jmsChannel;
	}

	private IJmsChannel generateTopicJmsChannel(ObjRef topicConfig) {
		String topicName = (String) xarch.get(topicConfig, "name");
		IJmsChannel jmsChannel = new TopicJmsChannel(topicName);
		LOGGER.info("Topic Name: " + topicName);
		return jmsChannel;
	}
	
	public Map<ObjRef, IJmsChannel> getConnectorToChannelMap() {
		return connector_To_Channel_Map;
	}

	public Map<ObjRef, IJmsChannel> getComponentInterfaceToChannelMap() {
		return componentInterface_To_Channel_Map;
	}
	
}
