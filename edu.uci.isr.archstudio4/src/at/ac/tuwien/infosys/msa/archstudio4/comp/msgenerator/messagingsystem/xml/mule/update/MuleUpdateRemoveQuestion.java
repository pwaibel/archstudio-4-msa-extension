package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem.xml.mule.update;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;

public class MuleUpdateRemoveQuestion implements Runnable {

	private boolean deleteNothing = false;
	private boolean deleteElement = false;
	private boolean deleteReference = false;
	private String message;
	
	public void showRemoveQuestion(String message) {
		this.message = message;
		
		Display.getDefault().syncExec(this);
	}

	@Override
	public void run() {
		MessageDialog dialog = new MessageDialog(Display.getDefault().getActiveShell(), "Architecture Warning", null, message, MessageDialog.QUESTION, new String[] { "Delete Element", "Delete nothing", "Delete Reference", }, 0);
		int result = dialog.open();
		
		if(result == 0) {
			deleteElement = true;
		}
		if(result == 1) {
			deleteNothing = true;
		}
		if(result == 2) {
			deleteReference = true;
		}

	}
	
	public boolean shouldDeleteNothing() {
		return deleteNothing;
	}
	
	public boolean shouldDeleteElement() {
		return deleteElement;
	}
	
	public boolean shouldDeleteReference() {
		return deleteReference;
	}

}
