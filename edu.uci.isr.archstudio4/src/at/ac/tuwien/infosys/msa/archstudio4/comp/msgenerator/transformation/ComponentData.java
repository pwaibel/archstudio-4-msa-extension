package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.transformation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.component.XadlComponentConfiguration;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.workflow.ConnectionEndpoint;

public class ComponentData {

	private List<ConnectionEndpoint> connectionEndpoints = new ArrayList<ConnectionEndpoint>();
	private Map<ConnectionEndpoint, Integer> connectionEndpointsOrder = new HashMap<ConnectionEndpoint, Integer>();
	private XadlComponentConfiguration xadlComponentConfig = new XadlComponentConfiguration();
	
	
	public List<ConnectionEndpoint> getConnectionEndpoints() {
		return connectionEndpoints;
	}
	public void setConnectionEndpoints(List<ConnectionEndpoint> connectionEndpoints) {
		this.connectionEndpoints = connectionEndpoints;
	}
	public Map<ConnectionEndpoint, Integer> getConnectionEndpointsOrder() {
		return connectionEndpointsOrder;
	}
	public void setConnectionEndpointsOrder(Map<ConnectionEndpoint, Integer> connectionEndpointsOrder) {
		this.connectionEndpointsOrder = connectionEndpointsOrder;
	}
	public XadlComponentConfiguration getXadlComponentConfig() {
		return xadlComponentConfig;
	}
	public void setXadlComponentConfig(XadlComponentConfiguration xadlComponentConfig) {
		this.xadlComponentConfig = xadlComponentConfig;
	}

	
}

