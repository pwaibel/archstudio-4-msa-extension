package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem.xml.mule.update;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Node;


public class MuleUpdateDeleteUnusedElements extends AbstractMuleUpdate {

	private Map<String, Node> unusedFlowElements = new HashMap<String, Node>();
	private Map<String, Node> unusedEndpointElements = new HashMap<String, Node>();
	private Map<String, Node> unusedConnectionElements = new HashMap<String, Node>();

	private Map<String, List<String>> usedConnectorArchStudioIds = new HashMap<String, List<String>>();
	
	public MuleUpdateDeleteUnusedElements(Document doc) {
		super(doc);
	}
	
	public void deleteUnusedElements(Map<String, List<String>> usedFlowArchStudioIds, Map<String, List<String>> usedConnectorArchStudioIds) {
		
		List<Node> allComments = findAllArchStudioIdComments(doc.getDocumentElement());

		List<String> usedEndpointArchStudioIds = new ArrayList<String>(); 
		
		for(Entry<String, List<String>> entrySet : usedFlowArchStudioIds.entrySet()) {
			usedEndpointArchStudioIds.addAll(entrySet.getValue());
		}
		
		
		for(Node commentElement : allComments) {
			Comment comment= (Comment) commentElement;
			String commentString = comment.getTextContent();
			
			if(commentString.contains("ArchStudioId=")) {
				String id = commentString.split("ArchStudioId=")[1];
				id = id.split(" ")[0].trim();
				
				if(isFlowComment(commentElement) && !usedFlowArchStudioIds.containsKey(id)) {					
					unusedFlowElements.put(id, commentElement);
					
				}
				else if(isConnectorComment(commentElement) && !usedConnectorArchStudioIds.containsKey(id)) {
					unusedConnectionElements.put(id, commentElement);
					
				}
				else if (isEndpointComment(commentElement) && !usedEndpointArchStudioIds.contains(id)) {
					unusedEndpointElements.put(id, commentElement);											
				}
			}
		}
		
		deleteFlowElements();
		
		deleteEndpointElements();
		
		deleteConnectorElements();
	}
	

	
	private void deleteFlowElements() {
		if(unusedFlowElements.size() > 0)
		{
			MuleUpdateRemoveQuestion removeQuestion = new MuleUpdateRemoveQuestion();
			for(Entry<String, Node> unusedElement : unusedFlowElements.entrySet()) {
				String message = "It seems that the Flow with the ArchStudio Id " + unusedElement.getKey() + " was used previously but now it isn't. Should the Mule Flow be removed or only the referenc to ArchStudio?";
				removeQuestion.showRemoveQuestion(message);
				
				if(removeQuestion.shouldDeleteElement()) {
					
					Node flowElement = unusedElement.getValue().getParentNode();
					
					removeAllEndpointsFromList(flowElement);
					
					Node muleElement = flowElement.getParentNode();
					if(muleElement.getNodeName().equals("mule")) {
						muleElement.removeChild(unusedElement.getValue().getParentNode());
					}				
				}
				else if(removeQuestion.shouldDeleteReference()) {
					Node flowElement = unusedElement.getValue().getParentNode();
					if(flowElement.getNodeName().equals("flow")) {
						flowElement.removeChild(unusedElement.getValue());
					}
				}
			}
		}
	}

	private void removeAllEndpointsFromList(Node flowElement) {

		List<Node> allEndpointComments = findAllArchStudioIdComments(flowElement);

		for (Node commentElement : allEndpointComments) {
			Comment comment = (Comment) commentElement;
			String commentString = comment.getTextContent();

			if (commentString.contains("ArchStudioId=")) {
				String id = commentString.split("ArchStudioId=")[1];
				id = id.split(" ")[0].trim();
				if (isEndpointComment(commentElement) && unusedEndpointElements.containsKey(id)) {
					unusedEndpointElements.remove(id);
				}
			}
		}
	}

	private void deleteEndpointElements() {

		if(unusedEndpointElements.size() > 0)
		{
			MuleUpdateRemoveQuestion removeQuestion = new MuleUpdateRemoveQuestion();
			for(Entry<String, Node> unusedElement : unusedEndpointElements.entrySet()) {
				String message = "It seems that the Endpoint with the ArchStudio Id " + unusedElement.getKey() + " was used previously but now it isn't. Should the Mule Endpoint be removed or only the referenc to ArchStudio?";
				removeQuestion.showRemoveQuestion(message);
				
				if(removeQuestion.shouldDeleteElement()) {
					Node muleElement = unusedElement.getValue().getParentNode().getParentNode();
					if(muleElement.getNodeName().equals("flow")) {
						muleElement.removeChild(unusedElement.getValue().getParentNode());
					}
					for(Entry<String, List<String>> entrySet: usedConnectorArchStudioIds.entrySet()) {
						if(entrySet.getValue().contains(unusedElement.getKey())) {
							entrySet.getValue().remove(unusedElement.getKey());
						}
					}					
				}
				else if(removeQuestion.shouldDeleteReference()) {
					Node flowElement = unusedElement.getValue().getParentNode();
					if(flowElement.getNodeName().contains("-endpoint")) {
						flowElement.removeChild(unusedElement.getValue());
					}
				}
			}
		}
	}
	
	private void deleteConnectorElements() {
		
		for(Entry<String, List<String>> entrySet: usedConnectorArchStudioIds.entrySet()) {
			if(!entrySet.getValue().isEmpty()) {
				unusedConnectionElements.remove(entrySet.getKey());
			}
		}
		
		if(unusedConnectionElements.size() > 0)
		{
			MuleUpdateRemoveQuestion removeQuestion = new MuleUpdateRemoveQuestion();
			for(Entry<String, Node> unusedElement : unusedConnectionElements.entrySet()) {
				String message = "It seems that the Global Connector with the ArchStudio Id " + unusedElement.getKey() + " was used previously but now it isn't. Should the Mule Endpoint be removed or only the referenc to ArchStudio?";
				removeQuestion.showRemoveQuestion(message);
				
				if(removeQuestion.shouldDeleteElement()) {
					Node muleElement = unusedElement.getValue().getParentNode().getParentNode();
					if(muleElement.getNodeName().equals("mule")) {
						muleElement.removeChild(unusedElement.getValue().getParentNode());
					}
				}
				else if(removeQuestion.shouldDeleteReference()) {
					Node flowElement = unusedElement.getValue().getParentNode();
					if(flowElement.getNodeName().contains("-connector")) {
						flowElement.removeChild(unusedElement.getValue());
					}
				}
			}
		}
	}
	
	
}
