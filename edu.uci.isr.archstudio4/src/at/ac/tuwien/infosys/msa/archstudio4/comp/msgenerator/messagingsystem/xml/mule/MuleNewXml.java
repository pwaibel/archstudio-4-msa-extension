package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.messagingsystem.xml.mule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import at.ac.tuwien.infosys.msa.archstudio4.comp.msacc.MSAInterfaceDirection;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.XadlConnectorConfiguration;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.channel.QueueJmsChannel;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.channel.TopicJmsChannel;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.workflow.ConnectionEndpoint;
import at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.workflow.IEsbWorkflow;

class MuleNewXml {

	private List<XadlConnectorConfiguration> alreadyWritten = new ArrayList<XadlConnectorConfiguration>();
	private MuleGenerateXmlHelper generateHelper = new MuleGenerateXmlHelper();
	private Document doc;

	private Map<String, Element> requestReplyMap = new HashMap<String, Element>();

	public MuleNewXml(Document doc) {
		this.doc = doc;

	}

	public void generateFlowSpecificInterfaceInformation(Element rootElement, List<IEsbWorkflow> workflows, String esbJmsSystem) {

		for (IEsbWorkflow workflow : workflows) {

			Element flowElement = generateHelper.generateStartFlow(doc, rootElement, workflow.getFlowName(), workflow.getComponentConfig());

			List<ConnectionEndpoint> orderedList = workflow.getOrderedInterfaces();

			boolean startEndpoint = true;
			for (ConnectionEndpoint conDirDomInterface : orderedList) {
				if (conDirDomInterface.getConnectorConfig() != null) {
					if (conDirDomInterface.getInterfaceConfig().getMuleInterfaceConfiguration().isRequestReplyEndpoint()) {
						generateRequestReplyEndpoints(rootElement, flowElement, workflow, conDirDomInterface, esbJmsSystem);
					} else {
						generateEndpoints(rootElement, flowElement, workflow, conDirDomInterface, esbJmsSystem, startEndpoint);
						startEndpoint = false;
					}
				}
			}

		}
	}

	private void generateRequestReplyEndpoints(Element rootElement, Element flowElement, IEsbWorkflow workflow, ConnectionEndpoint conDirDomInterface, String esbJmsSystem) {

		Element requestReplyElement;
		if (!requestReplyMap.containsKey(conDirDomInterface.getInterfaceConfig().getMuleInterfaceConfiguration().getRequestReplyEndpointId())) {
			requestReplyElement = generateHelper.generateRequestReplyElementForFlowElement(doc, flowElement);
			requestReplyMap.put(conDirDomInterface.getInterfaceConfig().getMuleInterfaceConfiguration().getRequestReplyEndpointId(), requestReplyElement);
		} else {
			requestReplyElement = requestReplyMap.get(conDirDomInterface.getInterfaceConfig().getMuleInterfaceConfiguration().getRequestReplyEndpointId());
		}

		generateEndpoints(rootElement, requestReplyElement, workflow, conDirDomInterface, esbJmsSystem, false);
	}

	private void generateEndpoints(Element rootElement, Element flowElement, IEsbWorkflow workflow, ConnectionEndpoint connectionEndpoint, String esbJmsSystem, boolean startEndpoint) {
		MSAInterfaceDirection direction = connectionEndpoint.getInterfaceConfig().getMuleInterfaceConfiguration().getDirection();
		XadlConnectorConfiguration connectorConfig = connectionEndpoint.getConnectorConfig();

		if (!alreadyWritten.contains(connectorConfig) && connectionEndpoint.getConnectorConfig() != null) {
			generateHelper.generateConnectorElement(doc, rootElement, esbJmsSystem, connectionEndpoint);
			alreadyWritten.add(connectorConfig);
		}

		if (direction.equals(MSAInterfaceDirection.IN)) {
			generateInboundEndpoint(flowElement, connectionEndpoint);
		} else if (direction.equals(MSAInterfaceDirection.OUT)) {
			generateOutboundEndpoint(flowElement, connectionEndpoint);
		} else if (direction.equals(MSAInterfaceDirection.INOUT)) {
			generateInOutboundEndpoint(flowElement, connectionEndpoint, startEndpoint);
		}

		if (!connectionEndpoint.getInterfaceConfig().getMuleInterfaceConfiguration().getReplyChannelName().isEmpty()) {
			generateHelper.generateMessagePropertiesElement(doc, requestReplyMap.get(connectionEndpoint.getInterfaceConfig().getMuleInterfaceConfiguration().getRequestReplyEndpointId()), connectionEndpoint);
		}

	}

	private Element generateInboundEndpoint(Element flowElement, ConnectionEndpoint connectionEndpoint) {
		Element endpoint = null;

		if (connectionEndpoint.getJmsChannel() instanceof TopicJmsChannel) {
			endpoint = generateHelper.generateTopicInboundEndpointElement(doc, flowElement, connectionEndpoint);
		} else if (connectionEndpoint.getJmsChannel() instanceof QueueJmsChannel) {
			endpoint = generateHelper.generateQueueInboundEndpointElement(doc, flowElement, connectionEndpoint);
		}

		return endpoint;
	}

	private Element generateOutboundEndpoint(Element flowElement, ConnectionEndpoint connectionEndpoint) {
		Element endpoint = null;

		if (connectionEndpoint.getJmsChannel() instanceof TopicJmsChannel) {
			endpoint = generateHelper.generateTopicOutboundEndpointElement(doc, flowElement, connectionEndpoint);
		} else if (connectionEndpoint.getJmsChannel() instanceof QueueJmsChannel) {
			endpoint = generateHelper.generateQueueOutboundEndpointElement(doc, flowElement, connectionEndpoint);
		}
		return endpoint;
	}

	private Element generateInOutboundEndpoint(Element flowElement, ConnectionEndpoint connectionEndpoint, boolean startEndpoint) {
		Element endpoint = null;

		if (connectionEndpoint.getJmsChannel() instanceof TopicJmsChannel) {
			endpoint = generateHelper.generateTopicInOutboundEndpointElement(doc, flowElement, connectionEndpoint, startEndpoint);
		} else if (connectionEndpoint.getJmsChannel() instanceof QueueJmsChannel) {
			endpoint = generateHelper.generateQueueInOutboundEndpointElement(doc, flowElement, connectionEndpoint, startEndpoint);
		}

		return endpoint;
	}

	public Element generateMuleElement(String muleVersion) {
		return generateHelper.generateMuleElement(doc, muleVersion);
	}

}
