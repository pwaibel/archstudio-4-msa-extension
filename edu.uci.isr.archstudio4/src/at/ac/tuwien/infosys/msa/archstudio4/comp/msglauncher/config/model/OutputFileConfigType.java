//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.01.30 at 09:23:36 PM CET 
//


package at.ac.tuwien.infosys.msa.archstudio4.comp.msglauncher.config.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for output_file_configType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="output_file_configType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="jms_list" type="{}jms_listType"/>
 *         &lt;element name="mule_list" type="{}mule_listType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "output_file_configType", propOrder = {
    "jmsList",
    "muleList"
})
@XmlRootElement(name = "output_file_config")
public class OutputFileConfigType {

    @XmlElement(name = "jms_list", required = true)
    protected JmsListType jmsList;
    @XmlElement(name = "mule_list", required = true)
    protected MuleListType muleList;

    /**
     * Gets the value of the jmsList property.
     * 
     * @return
     *     possible object is
     *     {@link JmsListType }
     *     
     */
    public JmsListType getJmsList() {
        return jmsList;
    }

    /**
     * Sets the value of the jmsList property.
     * 
     * @param value
     *     allowed object is
     *     {@link JmsListType }
     *     
     */
    public void setJmsList(JmsListType value) {
        this.jmsList = value;
    }

    /**
     * Gets the value of the muleList property.
     * 
     * @return
     *     possible object is
     *     {@link MuleListType }
     *     
     */
    public MuleListType getMuleList() {
        return muleList;
    }

    /**
     * Sets the value of the muleList property.
     * 
     * @param value
     *     allowed object is
     *     {@link MuleListType }
     *     
     */
    public void setMuleList(MuleListType value) {
        this.muleList = value;
    }

}
