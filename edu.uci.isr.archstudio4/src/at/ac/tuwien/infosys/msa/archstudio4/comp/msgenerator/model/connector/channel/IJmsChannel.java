package at.ac.tuwien.infosys.msa.archstudio4.comp.msgenerator.model.connector.channel;

public interface IJmsChannel {

	public String getName();
	
}
