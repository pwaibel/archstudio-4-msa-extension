Messaging System Archlight Tests
=============================================================================
Usage:


To perform consistency checks with Archlight, following steps has to be done:
1. Open the xADL document with Archlight.
2. After opening the document, the "Outline" view shows all available tests. 
   The corresponding messaging system consistency checks are listed under the path "xADL 2.0/Frameworks/MSCC/". This directory
   contains all messaging system relevant consistency checks, without link specific checks. The link specific checks can be found 
   in the directory "xADL 2.0/Structure and Types/Links".
3. To perform a check, it has to be marked. To do this, right-click on the desired test (or on a directory to mark all tests in that directory).
   This opens a context menu with which a test can be marked by selecting "Make Test Applied/Enabled".
4. By selecting the Button "Run Tests" all marked checks are performed, on the current xADL document.
5. After all tests are executed, the test results can be found in the "Archlight Issues" view. 
6. By clicking on a test result, a separate window, showing detailed information, is opened.
   By right-clicking on a test result, a context menu is shown. By using the sub-elements of this menu it can be directly navigated to the inconsistent component.  
 

=============================================================================
Available Tests:   


xADL 2.0/Frameworks/MSCC/Structure/Architecture:
Link between Connector and Connector: Checks if a connector is directly connected to another connector.
Link between Component and Component: Checks if a component is directly connected to another component.
Duplicated channel names: Checks if two connectors have the same channel name.
Topic has an INOUT-Interface: Checks if a topic has an INOUT-Interface.
OUT-Interface, of a Queue, has several Links: Checks if an OUT-interface, of a queue, has several connected links.
Interfaces, of a Component, has several Links: Checks if an OUT-interface on a component has several links.
Correct connected Request-Reply: Checks if an OUT-interface with Reply_To_Queue property is connected (over a connector) to an IN-interface with Reply_To_Queue property.
Request-Reply uses a topic: Checks if a topic is used for Request-Reply.

xADL 2.0/Frameworks/MSCC/Structure/Components:
MuleImplementation set: Checks if all components have a MuleImplementation.
Maximal one OUT-Interface set: Checks if not more than one IN-interface and one OUT-interface is set.
IN- and OUT-Interface present: Check if a Request-Reply composition has an IN- and an OUT-interface.
Request-Reply valid: Checks the right usage of the Request-Reply properties (one interface can only have the property Reply_To_Queue or Connection_To_Request_Endpoint).
Endpoint_Position_No required: Checks if the interfaces have to be organized manually. This has to be done if more than one INOUT-interface or a Request-Reply group exists.
Endpoint_Position_No usage: Checks if all interfaces have the attribute Endpoint_Position_No, if at least one interface has it.

xADL 2.0/Frameworks/MSCC/Types/ConnectorType:
JmsImplementation set: Checks if all connector types have a JmsImplementation.
Transport_Connector defined: Checks if all JmsImplementations have the Transport_Connector property set.

xADL 2.0/Frameworks/MSCC/Structure/Connector:
ChannelImplementation set: Checks if all connectors have a ChannelImplementation.
Topic or Queue defined: Checks if all ChannelImplementations have a Topic_Configuration or Queue_Configuration set.
Only Topic or Queue set: Checks if only Topic_Configuration or Queue_Configuration is set.

xADL 2.0/Frameworks/MSCC/Types/ConnectorType:
File_id value set: Checks if all connector types, with a JmsImplementation, have the file_id value set.
File_id value set: Checks if all Components, with a MuleImplementation, have the file_id value set.
